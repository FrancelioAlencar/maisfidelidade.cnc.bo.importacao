using Amazon.Lambda.Core;
using Amazon.Lambda.RuntimeSupport;
using Amazon.Lambda.SNSEvents;
using maisfidelidade.cnc.imp.core.Common.AWS;
using maisfidelidade.cnc.imp.core.Common.DependencyInjection;
using maisfidelidade.cnc.imp.core.Common.Helpers;
using maisfidelidade.cnc.imp.core.Common.Logging;
using maisfidelidade.cnc.imp.core.Domain.Model.Arquivos;
using maisfidelidade.cnc.imp.core.Domain.Model.OrquestracaoCadastro;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
namespace maisfidelidade.cnc.imp.orq.cadastro
{
	public class Function : FunctionBase
	{
		private readonly IArquivoMongoDBRepository arquivoMongoDBRepository;

		public Function()
			: base(LogContextEnum.OrquestracaoCadastro)
		{
			arquivoMongoDBRepository = DependencyResolver.GetService<IArquivoMongoDBRepository>();
		}

        private static async Task Main(string[] args)
        {
			Func<string, ILambdaContext, string> func = null;// FunctionHandler;
			using (var handlerWrapper = HandlerWrapper.GetHandlerWrapper(func, new Amazon.Lambda.Serialization.Json.JsonSerializer()))
			using (var bootstrap = new LambdaBootstrap(handlerWrapper))
			{
				await bootstrap.RunAsync();
			}
		}

		public bool FunctionHandler(SNSEvent snsEvent, ILambdaContext lambdaContext)
        {
			object arquivoImportadoId = null;

			try
			{
				BeginLogInfo(lambdaContext, arquivoImportadoId, "A solicitação de processamento da orquestração do cadastro foi iniciada.");


				if (IsSnsMessageInvalid(snsEvent, out object arquivoImportadId, out CadastroEnum etapaCadastroAtual))
				{
					throw new Exception("");
				}

				var arquivoImportado = arquivoMongoDBRepository.BuscarPor(arquivoImportadId);

				if (!arquivoImportado.EtapaEmpresaConcluida || !arquivoImportado.EtapaGerenteConcluida)
				{
					switch (etapaCadastroAtual)
					{
						case CadastroEnum.Empresas:
							arquivoImportado.EtapaEmpresaConcluida = true;
							break;
						case CadastroEnum.Gerentes:
							arquivoImportado.EtapaGerenteConcluida = true;
							break;
						default:
							break;
					}

					arquivoMongoDBRepository.Salvar(arquivoImportado);

					bool sendSNS = arquivoImportado.EtapaEmpresaConcluida && arquivoImportado.EtapaGerenteConcluida;

					if (sendSNS)
					{
						AWSHelper.EnviarSNS(arquivoImportadId.ToString(), "sns-apuracao");
					}
				}

				EndLogInfo(lambdaContext, arquivoImportadoId, "A solicitação de processamento da orquestração do cadastro foi concluída com sucesso.");

				return true;
			}
			catch (Exception exception)
			{
				LogError(lambdaContext, arquivoImportadoId, $"Falha na solicitação de processamento da orquestração do cadastro - {exception.Message}", exception.StackTrace, exception);

				return false;
			}
		}
		private bool IsSnsMessageInvalid(SNSEvent snsEvent, out object arquivoId, out CadastroEnum etapaCadastroAtual)
		{
			var result = false;

			arquivoId = null;
			etapaCadastroAtual = CadastroEnum.Default;

			var snsMessage = snsEvent.Records?[0].Sns.Message;

			if (string.IsNullOrEmpty(snsMessage))
			{
				result = true;
			}

			var snsOrquestracaoMessage = JsonConvert.DeserializeObject<SnsOrquestracaoCadastro>(snsMessage, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Include });

			if (snsOrquestracaoMessage.ObjectId != string.Empty)
				arquivoId = snsOrquestracaoMessage.ObjectId;

			if (Enum.IsDefined(typeof(CadastroEnum), snsOrquestracaoMessage.EtapaCadastro))
				etapaCadastroAtual = snsOrquestracaoMessage.EtapaCadastro;

			return result;
		}
	}
}
