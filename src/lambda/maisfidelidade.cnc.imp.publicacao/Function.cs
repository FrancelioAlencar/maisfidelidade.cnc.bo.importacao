using Amazon;
using Amazon.Lambda.Core;
using Amazon.Lambda.SNSEvents;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using maisfidelidade.cnc.imp.core.Common.AWS;
using maisfidelidade.cnc.imp.core.Common.Configuration;
using maisfidelidade.cnc.imp.core.Common.Logging;
using maisfidelidade.cnc.imp.core.Domain.Model.Arquivos;
using maisfidelidade.cnc.imp.core.Domain.Model.Enums;
using maisfidelidade.cnc.imp.core.Infrastructure.Publication;
using MongoDB.Bson;
using System;
using System.Threading.Tasks;

[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace maisfidelidade.cnc.imp.publicacao
{
    public class Function : FunctionBase
    {
        public Function()
            : base(LogContextEnum.Publicacao)
        {

        }

        public bool FunctionHandler(SNSEvent snsEvent, ILambdaContext lambdaContext)
        {
            object arquivoImportadoId = null;

            try
            {
                BeginLogInfo(lambdaContext, arquivoImportadoId, "A solicitação de processamento da publicação foi iniciada.");

                lambdaContext.SetEnvironment();

                if (IsSnsMessageInvalid(snsEvent, out arquivoImportadoId))
                {
                    throw new Exception("");
                }

                LogInfo(lambdaContext, arquivoImportadoId, "Processando publicação");
                
                ProcessarPublicacao(arquivoImportadoId);

                LogInfo(lambdaContext, arquivoImportadoId, "Publicação processada com sucesso");

                EndLogInfo(lambdaContext, arquivoImportadoId, "A solicitação de processamento da publicação foi concluída com sucesso.");

                return true;
            }
            catch (Exception exception)
            {
                LogError(lambdaContext, arquivoImportadoId, $"Falha na solicitação de processamento da publicação - {exception.Message}", exception.StackTrace, exception);

                return false;
            }
        }

        #region Métodos Privados

        private bool IsSnsMessageInvalid(SNSEvent snsEvent, out object arquivoId)
        {
            var result = false;
            
            arquivoId = null;

            var snsMessage = snsEvent.Records?[0].Sns.Message;

            if (string.IsNullOrEmpty(snsMessage))
            {              
                result = true;
            }

            if (!ObjectId.TryParse(snsMessage, out ObjectId objectId))
            {
                result = true;
            }
            
            if (objectId != ObjectId.Empty)
            {
                arquivoId = objectId;
            }

            return result;
        }

        private void AtualizarStatusParaPublicado(ArquivoImportado arquivoImportado)
        {
            try
            {
                arquivoImportado.StatusArquivo = StatusArquivoEnum.Publicando;

                //arquivoMongoDBRepositorio.Salvar(arquivoImportado);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        private void MoverArquivoFisicoParaPastaPublicando(string arquivoNome)
        {
            try
            {

            }
            catch (Exception exception)
            {

            }
        }

        private void MoverArquivoFisicoParaPastaPublicado(string arquivoNome)
        {
            try
            {

            }
            catch (Exception exception)
            {

            }
        }

        private void ProcessarPublicacao(object arquivoId)
        {
            Publisher.Publish(arquivoId);
        }

        #endregion

        





        private async Task MoverArquivoAsync(string sourceBucketName, string sourceObjectKey, string destinationObjectKey)
        {
            using (var client = new AmazonS3Client(RegionEndpoint.SAEast1))
            {
                CopyObjectRequest copyRequest = new CopyObjectRequest
                {
                    SourceBucket = sourceBucketName,
                    SourceKey = sourceObjectKey,
                    DestinationBucket = sourceBucketName,
                    DestinationKey = destinationObjectKey
                };

                await client.CopyObjectAsync(copyRequest);

                var deleteRequest = new DeleteObjectRequest
                {
                    BucketName = sourceBucketName,
                    Key = sourceObjectKey
                };

                await client.DeleteObjectAsync(deleteRequest);
            }
        }

    }
}
