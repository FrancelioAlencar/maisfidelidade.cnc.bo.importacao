using Amazon.Lambda.Core;
using Amazon.Lambda.RuntimeSupport;
using Amazon.Lambda.Serialization.Json;
using Amazon.Lambda.SNSEvents;
using maisfidelidade.cnc.imp.core.Common.AWS;
using maisfidelidade.cnc.imp.core.Common.Helpers;
using maisfidelidade.cnc.imp.core.Common.DependencyInjection;
using maisfidelidade.cnc.imp.core.Common.Logging;
using maisfidelidade.cnc.imp.core.Domain.Model.Arquivos;
using maisfidelidade.cnc.imp.core.Domain.Model.Enums;
using maisfidelidade.cnc.imp.core.Domain.Model.PerformanceIndicadores;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace maisfidelidade.cnc.imp.indicador.performance
{
    public class Function : FunctionBase
    {
        private readonly IArquivoMongoDBRepository arquivoMongoDBRepository;
        private readonly IPerformanceIndicadorMongoDBRepository performanceIndicadorMongoDBRepository;
        private readonly IPerformanceIndicadorMySqlRepository performanceIndicadorMySqlRepository;

        private IList<PerformanceIndicador> performanceIndicadoresCadastrados;
        private IList<PerformanceIndicadorApuracaoImportado> performanceIndicadorApuracoes;

        public Function()
            : base(LogContextEnum.ApuracaoPerformanceIndicador)
        {
            arquivoMongoDBRepository = DependencyResolver.GetService<IArquivoMongoDBRepository>();
            performanceIndicadorMongoDBRepository = DependencyResolver.GetService<IPerformanceIndicadorMongoDBRepository>();
            performanceIndicadorMySqlRepository = DependencyResolver.GetService<IPerformanceIndicadorMySqlRepository>();
        }

        /// <summary>
        /// The main entry point for the custom runtime.
        /// </summary>
        /// <param name="args"></param>
        private static async Task Main(string[] args)
        {
            Func<SNSEvent, ILambdaContext, string> func = null;// FunctionHandler;
            using (var handlerWrapper = HandlerWrapper.GetHandlerWrapper(func, new JsonSerializer()))
            using(var bootstrap = new LambdaBootstrap(handlerWrapper))
            {
                await bootstrap.RunAsync();
            }
        }

        /// <summary>
        /// A simple function that takes a string and does a ToUpper
        ///
        /// To use this handler to respond to an AWS event, reference the appropriate package from 
        /// https://github.com/aws/aws-lambda-dotnet#events
        /// and change the string input parameter to the desired event type.
        /// </summary>
        /// <param name="input"></param>
        /// <param name="lambdaContext"></param>
        /// <returns></returns>
        public bool FunctionHandler(SNSEvent snsEvent, ILambdaContext lambdaContext)
        {
            object arquivoImportadoId = null;

            try
            {
                BeginLogInfo(lambdaContext, arquivoImportadoId, "A solicitação de processamento da apuração do indicador de performance foi iniciada.");

                if (IsSnsMessageInvalid(snsEvent, out arquivoImportadoId))
                {
                    throw new Exception("");
                }

                performanceIndicadoresCadastrados = performanceIndicadorMySqlRepository.ObterPerformanceIndicadores();

                var arquivoImportado = arquivoMongoDBRepository.BuscarPor(arquivoImportadoId);

                var quantidadeLinhas = 5;
                
                var lotes = arquivoImportado.ObterQuantidadeLotes(quantidadeLinhas);
                var colunas = arquivoMongoDBRepository.ObterColunas(GrupoColunaEnum.PerformanceIndicador);

                performanceIndicadorApuracoes = new List<PerformanceIndicadorApuracaoImportado>();

                for (int lote = 0; lote < lotes; lote++)
                {
                    var skip = lote * quantidadeLinhas;

                    var arquivoImportadoLinhas = arquivoMongoDBRepository.BuscarArquivoImportadoLinhas(arquivoImportado.Id, skip, quantidadeLinhas);

                    foreach (var arquivoImportadoLinha in arquivoImportadoLinhas)
                    {
                        var performanceIndicadorApuracao = new PerformanceIndicadorApuracaoImportado
                        {
                            Guid = Guid.Parse(arquivoImportadoLinha.Celunas[ArquivoColunaIndexes.GUID].ToString()),
                            Cnpj = arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.NO_CNPJ].Nome].ToString()
                        };

                        foreach (var performanceIndicador in performanceIndicadoresCadastrados)
                        {
                            switch (performanceIndicador.Tipo)
                            {
                                case PerformanceIndicadorEnum.Ambos:
                                    performanceIndicadorApuracao.PerformanceIndicadorApurados.Add(ObterAmbosPerformanceIndicadorApurado(Convert.ToInt32(performanceIndicador.Id), arquivoImportadoLinha, colunas));
                                    break;
                                case PerformanceIndicadorEnum.SeguroAuto:
                                    performanceIndicadorApuracao.PerformanceIndicadorApurados.Add(ObterSeguroAutoPerformanceIndicadorApurado(Convert.ToInt32(performanceIndicador.Id), arquivoImportadoLinha, colunas));
                                    break;
                                case PerformanceIndicadorEnum.SeguroPrestamista:
                                    performanceIndicadorApuracao.PerformanceIndicadorApurados.Add(ObterSeguroPrestamistaPerformanceIndicadorApurado(Convert.ToInt32(performanceIndicador.Id), arquivoImportadoLinha, colunas));
                                    break;
                            }
                        }

                        performanceIndicadorApuracoes.Add(performanceIndicadorApuracao);
                    }
                }

                SalvarPerformanceIndicadorApuracoes(arquivoImportadoId);

                AWSHelper.EnviarSNS(arquivoImportadoId.ToString(), "sns-orquestracao-apuracao");

                EndLogInfo(lambdaContext, arquivoImportadoId, "A solicitação de processamento da apuração do indicador de performance foi concluída com sucesso.");

                return true;
            }
            catch (Exception exception)
            {
                LogError(lambdaContext, arquivoImportadoId, $"Falha na solicitação de processamento da apuração do indicador de performance - {exception.Message}", exception.StackTrace, exception);

                return false;
            }
        }

        #region Métodos Privados

        private bool IsSnsMessageInvalid(SNSEvent snsEvent, out object arquivoId)
        {
            var result = false;

            arquivoId = null;

            var snsMessage = snsEvent.Records?[0].Sns.Message;

            if (string.IsNullOrEmpty(snsMessage))
            {
                result = true;
            }

            if (!ObjectId.TryParse(snsMessage, out ObjectId objectId))
            {
                result = true;
            }

            if (objectId != ObjectId.Empty)
            {
                arquivoId = objectId;
            }

            return result;
        }

        private PerformanceIndicadorApurado ObterAmbosPerformanceIndicadorApurado(int performanceIndicadorId, ArquivoImportadoLinha arquivoImportadoLinha, IDictionary<int, ArquivoColuna> colunas)
        {
            return new AmbosPerformanceIndicadorApurado
            {
                PerformanceIndicadorId = performanceIndicadorId,
                QuantidadeContratos = Convert.ToInt32(arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.AMBOS_CONTRATOS].Nome])
            };
        }

        private PerformanceIndicadorApurado ObterSeguroAutoPerformanceIndicadorApurado(int performanceIndicadorId, ArquivoImportadoLinha arquivoImportadoLinha, IDictionary<int, ArquivoColuna> colunas)
        {
            return new SeguroAutoPerformanceIndicadorApurado
            {
                PerformanceIndicadorId = performanceIndicadorId,
                Porcentagem = Convert.ToDouble(arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.SEGURO_AUTO_PC].Nome]),
                ContratosElegiveis = Convert.ToInt32(arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.SEGURO_AUTO_CONTRATOS_ELEGIVEIS].Nome]),
                ContratosConvertidos = Convert.ToInt32(arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.SEGURO_AUTO_CONTRATOS_CONVERTIDOS].Nome])
            };
        }

        private PerformanceIndicadorApurado ObterSeguroPrestamistaPerformanceIndicadorApurado(int performanceIndicadorId, ArquivoImportadoLinha arquivoImportadoLinha, IDictionary<int, ArquivoColuna> colunas)
        {
            return new SeguroPrestamistaPerformanceIndicadorApurado
            {
                PerformanceIndicadorId = performanceIndicadorId,
                Porcentagem = Convert.ToDouble(arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.SEGURO_PRESTAMISTA_PC].Nome]),
                ContratosElegiveis = Convert.ToInt32(arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.SEGURO_PRESTAMISTA_CONTRATOS_ELEGIVEIS].Nome]),
                ContratosConvertidos = Convert.ToInt32(arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.SEGURO_PRESTAMISTA_CONTRATOS_CONVERTIDOS].Nome])
            };
        }

        private void SalvarPerformanceIndicadorApuracoes(object arquivoImportadoId)
        {
            performanceIndicadorMongoDBRepository.Salvar(new PerformanceIndicadorRegistro
            {
                ArquivoImportadoId = arquivoImportadoId,
                DataInclusao = DateTime.Now,
                PerformanceIndicadorApuracoes = performanceIndicadorApuracoes
            });
        }

        #endregion
    }
}
