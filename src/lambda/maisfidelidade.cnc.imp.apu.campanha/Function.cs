using Amazon.Lambda.Core;
using Amazon.Lambda.RuntimeSupport;
using Amazon.Lambda.Serialization.Json;
using maisfidelidade.cnc.imp.core.Common.Helpers;
using Amazon.Lambda.SNSEvents;
using maisfidelidade.cnc.imp.core.Common.AWS;
using maisfidelidade.cnc.imp.core.Common.Logging;
using System;
using System.Threading.Tasks;

namespace maisfidelidade.cnc.imp.apu.campanha
{
    public class Function : FunctionBase
    {
        public Function()
            : base(LogContextEnum.ApuracaoCampanha)
        {

        }


        /// <summary>
        /// The main entry point for the custom runtime.
        /// </summary>
        /// <param name="args"></param>
        private static async Task Main(string[] args)
        {
            Func<string, ILambdaContext, string> func = null;// FunctionHandler;
            using(var handlerWrapper = HandlerWrapper.GetHandlerWrapper(func, new JsonSerializer()))
            using(var bootstrap = new LambdaBootstrap(handlerWrapper))
            {
                await bootstrap.RunAsync();
            }
        }

        /// <summary>
        /// A simple function that takes a string and does a ToUpper
        ///
        /// To use this handler to respond to an AWS event, reference the appropriate package from 
        /// https://github.com/aws/aws-lambda-dotnet#events
        /// and change the string input parameter to the desired event type.
        /// </summary>
        /// <param name="input"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public bool FunctionHandler(SNSEvent snsEvent, ILambdaContext lambdaContext)
        {
            object arquivoImportadoId = null;

            try
            {
                BeginLogInfo(lambdaContext, arquivoImportadoId, "A solicita��o de processamento da apura��o da campanha foi iniciada.");

                AWSHelper.EnviarSNS(arquivoImportadoId.ToString(), "sns-orquestracao-apuracao");

                EndLogInfo(lambdaContext, arquivoImportadoId, "A solicita��o de processamento da apura��o da campanha foi conclu�da com sucesso.");

                return true;
            }
            catch (Exception exception)
            {
                LogError(lambdaContext, arquivoImportadoId, $"Falha na solicita��o de processamento da apura��o da campanha - {exception.Message}", exception.StackTrace, exception);

                return true;
            }
        }
    }
}
