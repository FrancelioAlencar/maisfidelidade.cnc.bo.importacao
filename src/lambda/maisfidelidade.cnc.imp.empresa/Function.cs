using Amazon.Lambda.Core;
using Amazon.Lambda.RuntimeSupport;
using Amazon.Lambda.Serialization.Json;
using Amazon.Lambda.SNSEvents;
using maisfidelidade.cnc.imp.core.Common.AWS;
using maisfidelidade.cnc.imp.core.Common.DependencyInjection;
using maisfidelidade.cnc.imp.core.Common.Logging;
using maisfidelidade.cnc.imp.core.Domain.Model;
using maisfidelidade.cnc.imp.core.Domain.Model.Arquivos;
using maisfidelidade.cnc.imp.core.Domain.Model.Empresas;
using maisfidelidade.cnc.imp.core.Domain.Model.Enums;
using maisfidelidade.cnc.imp.core.Common.Helpers;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using maisfidelidade.cnc.imp.core.Domain.Model.Segmentos;

namespace maisfidelidade.cnc.imp.empresa
{
    public class Function : FunctionBase
    {
        private readonly IArquivoMongoDBRepository arquivoMongoDBRepository;
        private readonly IEmpresaMongoDBRepository empresaMongoDBRepository;
        private readonly IEmpresaMySqlRepository empresaMySqlRepository;
        private readonly ISegmentoMySqlRepository segmentoMySqlRepository;

        private IList<Empresa> empresasCadastradas;
        private IList<EmpresaImportada> empresasImportadas;

        private List<Linha<EmpresaImportada>> empresaLinhasValidas;
        private List<Linha<EmpresaImportada>> empresaLinhasInvalidas;

        public Function()
            : base(LogContextEnum.Empresa)
        {
            arquivoMongoDBRepository = DependencyResolver.GetService<IArquivoMongoDBRepository>();
            empresaMongoDBRepository = DependencyResolver.GetService<IEmpresaMongoDBRepository>();
            empresaMySqlRepository = DependencyResolver.GetService<IEmpresaMySqlRepository>();
            segmentoMySqlRepository = DependencyResolver.GetService<ISegmentoMySqlRepository>();
        }


        /// <summary>
        /// The main entry point for the custom runtime.
        /// </summary>
        /// <param name="args"></param>
        private static async Task Main(string[] args)
        {
            Func<string, ILambdaContext, string> func = null;// FunctionHandler;
            using(var handlerWrapper = HandlerWrapper.GetHandlerWrapper(func, new JsonSerializer()))
            using(var bootstrap = new LambdaBootstrap(handlerWrapper))
            {
                await bootstrap.RunAsync();
            }
        }

        /// <summary>
        /// A simple function that takes a string and does a ToUpper
        ///
        /// To use this handler to respond to an AWS event, reference the appropriate package from 
        /// https://github.com/aws/aws-lambda-dotnet#events
        /// and change the string input parameter to the desired event type.
        /// </summary>
        /// <param name="input"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public bool FunctionHandler(SNSEvent snsEvent, ILambdaContext lambdaContext)
        {
            object arquivoImportadoId = null;

            try
            {
                BeginLogInfo(lambdaContext, arquivoImportadoId, "A solicitação de processamento da importação da empresa foi iniciada.");

                if (IsSnsMessageInvalid(snsEvent, out object arquivoId))
                {
                    throw new Exception("");
                }

                empresasCadastradas = empresaMySqlRepository.ObterEmpresas();

                var arquivoImportado = arquivoMongoDBRepository.BuscarPor(arquivoId);

                var quantidadeLinhas = 5;

                var lotes = arquivoImportado.ObterQuantidadeLotes(quantidadeLinhas);

                var colunas = arquivoMongoDBRepository.ObterColunas(GrupoColunaEnum.Empresa);
                var segmentos = segmentoMySqlRepository.ObterSegmentos();

                empresaLinhasValidas = new List<Linha<EmpresaImportada>>();
                empresaLinhasInvalidas = new List<Linha<EmpresaImportada>>();

                empresasImportadas = new List<EmpresaImportada>();

                for (int lote = 0; lote < lotes; lote++)
                {
                    var skip = lote * quantidadeLinhas;

                    var arquivoImportadoLinhas = arquivoMongoDBRepository.BuscarArquivoImportadoLinhas(arquivoImportado.Id, skip, quantidadeLinhas);

                    foreach (var arquivoImportadoLinha in arquivoImportadoLinhas)
                    {
                        var empresaImportada = new EmpresaImportada
                        {
                            Cnpj = arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.NO_CNPJ].Nome].ToString(),
                            NumeroTab = Convert.ToInt32(arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.NO_TAB].Nome]),
                            NomeFantasia = arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.NM_FANTASIA].Nome].ToString(),
                            NomeGrupo = arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.NM_GRUPO].Nome].ToString(),
                            SegmentoId = ObterSegmentoId(arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.NM_SEGMENTO].Nome].ToString(), segmentos),
                            Email = arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.DS_EMAIL].Nome].ToString(),
                            MatriculaGR = Convert.ToInt32(arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.MATRICULA_GR].Nome]),
                            MatriculaGC = Convert.ToInt32(arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.MATRICULA_GC].Nome])
                        };

                        ValidarEmpresaImportada(empresaImportada);
                    }
                }

                //if (empresaLinhasInvalidas.Any())
                //{
                //    return true;
                //}

                SalvarEmpresasImportadas(arquivoId);

                AWSHelper.EnviarSNS(arquivoId.ToString(), "sns-orq-cadastros");

                EndLogInfo(lambdaContext, arquivoImportadoId, "A solicitação de processamento da importação da empresa foi concluída com sucesso.");

                return true;
            }
            catch (Exception exception)
            {
                LogError(lambdaContext, arquivoImportadoId, $"Falha na solicitação de processamento da importação da empresa - {exception.Message}", exception.StackTrace, exception);

                return false;
            }
        }

        #region Métodos Privados

        private bool IsSnsMessageInvalid(SNSEvent snsEvent, out object arquivoId)
        {
            var result = false;

            arquivoId = null;

            var snsMessage = snsEvent.Records?[0].Sns.Message;

            if (string.IsNullOrEmpty(snsMessage))
            {
                result = true;
            }

            if (!ObjectId.TryParse(snsMessage, out ObjectId objectId))
            {
                result = true;
            }

            if (objectId != ObjectId.Empty)
            {
                arquivoId = objectId;
            }

            return result;
        }

        public bool NaoExisteNenhumaEmpresaCadastradaComEsteCnpj(string cnpj)
        {
            return !empresasCadastradas.Any(e => e.Cnpj.ToUpper().Equals(cnpj.ToUpper()));
        }

        private void ValidarEmpresaImportada(EmpresaImportada empresaImportada)
        {
            if (NaoExisteNenhumaEmpresaCadastradaComEsteCnpj(empresaImportada.Cnpj))
            {
                empresasImportadas.Add(empresaImportada);
            }
        }

        private void SalvarEmpresasImportadas(object arquivoId)
        {
            empresaMongoDBRepository.Salvar(new EmpresaRegistro
            {
                ArquivoImportadoId = arquivoId,
                DataInclusao = DateTime.Now,
                EmpresasImportadas = empresasImportadas
            });
        }

        private int ObterSegmentoId(string nomeSegmento, IList<Segmento> segmentos)
        {
            try
            {
                return Convert.ToInt32(segmentos.FirstOrDefault(s => s.Nome.ToUpper().Equals(nomeSegmento.ToUpper())).Id);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion
    }
}
