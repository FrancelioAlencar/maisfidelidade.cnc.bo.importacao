using Amazon.Lambda.Core;
using Amazon.Lambda.RuntimeSupport;
using Amazon.Lambda.Serialization.Json;
using Amazon.Lambda.SNSEvents;
using maisfidelidade.cnc.imp.core.Common.AWS;
using maisfidelidade.cnc.imp.core.Common.Helpers;
using maisfidelidade.cnc.imp.core.Common.DependencyInjection;
using maisfidelidade.cnc.imp.core.Common.Logging;
using maisfidelidade.cnc.imp.core.Domain.Model;
using maisfidelidade.cnc.imp.core.Domain.Model.Arquivos;
using maisfidelidade.cnc.imp.core.Domain.Model.Enums;
using maisfidelidade.cnc.imp.core.Domain.Model.Gerentes;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace maisfidelidade.cnc.imp.gerente
{
    public class Function : FunctionBase
    {        
        private readonly IArquivoMongoDBRepository arquivoMongoDBRepository;
        private readonly IGRMongoDBRepository grMongoDBRepository;
        private readonly IGCMongoDBRepository gcMongoDBRepository;
        private readonly IGerenteMySqlRepository gerenteMySqlRepository;

        private IList<Gerente> gerentesCadastrados;
        private IList<GRImportado> gerentesDeRelacionamentoImportados;
        private IList<GCImportado> gerentesComercialImportados;

        private List<Linha<GRImportado>> grLinhasValidas;
        private List<Linha<GRImportado>> grLinhasInvalidas;
        private List<Linha<GCImportado>> gcLinhasValidas;
        private List<Linha<GCImportado>> gcLinhasInvalidas;


        public Function()
            : base(LogContextEnum.Gerente)
        {
            arquivoMongoDBRepository = DependencyResolver.GetService<IArquivoMongoDBRepository>();
            grMongoDBRepository = DependencyResolver.GetService<IGRMongoDBRepository>();
            gcMongoDBRepository = DependencyResolver.GetService<IGCMongoDBRepository>();
            gerenteMySqlRepository = DependencyResolver.GetService<IGerenteMySqlRepository>();

        }


        /// <summary>
        /// The main entry point for the custom runtime.
        /// </summary>
        /// <param name="args"></param>
        private static async Task Main(string[] args)
        {
            Func<SNSEvent, ILambdaContext, string> func = null;// FunctionHandler;
            using (var handlerWrapper = HandlerWrapper.GetHandlerWrapper(func, new JsonSerializer()))
            using (var bootstrap = new LambdaBootstrap(handlerWrapper))
            {
                await bootstrap.RunAsync();
            }
        }

        /// <summary>
        /// A simple function that takes a string and does a ToUpper
        ///
        /// To use this handler to respond to an AWS event, reference the appropriate package from 
        /// https://github.com/aws/aws-lambda-dotnet#events
        /// and change the string input parameter to the desired event type.
        /// </summary>
        /// <param name="input"></param>
        /// <param name="lambdaContext"></param>
        /// <returns></returns>
        public bool FunctionHandler(SNSEvent snsEvent, ILambdaContext lambdaContext)
        {
            object arquivoImportadoId = null;

            try
            {
                BeginLogInfo(lambdaContext, arquivoImportadoId, "A solicitação de processamento da importação do gerente foi iniciada.");

                if (IsSnsMessageInvalid(snsEvent, out object arquivoId))
                {
                    throw new Exception("");
                }

                gerentesCadastrados = gerenteMySqlRepository.ObterTodosGerentes();

                var arquivoImportado = arquivoMongoDBRepository.BuscarPor(arquivoId);

                var quantidadeLinhas = 5;

                var lotes = arquivoImportado.ObterQuantidadeLotes(quantidadeLinhas);

                var colunas = arquivoMongoDBRepository.ObterColunas(GrupoColunaEnum.Gerente);

                grLinhasValidas = new List<Linha<GRImportado>>();
                grLinhasInvalidas = new List<Linha<GRImportado>>();
                gcLinhasValidas = new List<Linha<GCImportado>>();
                gcLinhasInvalidas = new List<Linha<GCImportado>>();

                gerentesDeRelacionamentoImportados = new List<GRImportado>();
                gerentesComercialImportados = new List<GCImportado>();

                for (int lote = 0; lote < lotes; lote++)
                {
                    var skip = lote * quantidadeLinhas;

                    var arquivoImportadoLinhas = arquivoMongoDBRepository.BuscarArquivoImportadoLinhas(arquivoImportado.Id, skip, quantidadeLinhas);

                    foreach (var arquivoImportadoLinha in arquivoImportadoLinhas)
                    {
                        var cnpj = arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.NO_CNPJ].Nome].ToString();

                        var gerenteDeRelacionamentoImportado = new GRImportado
                        {
                            Matricula = Convert.ToInt32(arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.MATRICULA_GR].Nome]),
                            Nome = arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.NOME_GR].Nome].ToString(),
                            Email = arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.EMAIL_GR].Nome].ToString(),
                            Telefone = ObterTelefoneSemMascara(arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.TELEFONE_GR].Nome].ToString())
                        };

                        var gerenteComercialImportado = new GCImportado
                        {
                            Matricula = Convert.ToInt32(arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.MATRICULA_GC].Nome]),
                            Nome = arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.NOME_GC].Nome].ToString(),
                            Email = arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.EMAIL_GC].Nome].ToString(),
                            Telefone = ObterTelefoneSemMascara(arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.TELEFONE_GC].Nome].ToString())
                        };

                        ValidarGRImportado(cnpj, gerenteDeRelacionamentoImportado);
                        ValidarGCImporatado(cnpj, gerenteComercialImportado);
                    }
                }

                if (grLinhasInvalidas.Any() || gcLinhasInvalidas.Any())
                {
                    return true;
                }

                SalvarGRImportados(arquivoId);
                SalvarGCImportados(arquivoId);

                AWSHelper.EnviarSNS(arquivoId.ToString(), "sns-orq-cadastros");

                EndLogInfo(lambdaContext, arquivoImportadoId, "A solicitação de processamento da importação do gerente foi concluída com sucesso.");

                return true;
            }
            catch (Exception exception)
            {
                LogError(lambdaContext, arquivoImportadoId, $"Falha na solicitação de processamento da importação do gerente - {exception.Message}", exception.StackTrace, exception);

                return false;
            }
        }

        #region Métodos Privados

        private bool IsSnsMessageInvalid(SNSEvent snsEvent, out object arquivoId)
        {
            var result = false;

            arquivoId = null;

            var snsMessage = snsEvent.Records?[0].Sns.Message;

            if (string.IsNullOrEmpty(snsMessage))
            {
                result = true;
            }

            if (!ObjectId.TryParse(snsMessage, out ObjectId objectId))
            {
                result = true;
            }

            if (objectId != ObjectId.Empty)
            {
                arquivoId = objectId;
            }

            return result;
        }

        private bool ExisteAlgumGRCadastradoNoBancoComEstaMatricula(int matricula)
        {
            return gerentesCadastrados.Any(gr => gr.Matricula == matricula && gr.Tipo == TipoGerenteEnum.GR);
        }

        private bool ExisteAlgumGCCadastradoNoBancoComEstaMatricula(int matricula)
        {
            return gerentesCadastrados.Any(gr => gr.Matricula == matricula && gr.Tipo == TipoGerenteEnum.GC);
        }

        private bool ExisteAlgumGRCadastradoNoBancoComEstaMatriculaEEmail(int matricula, string email)
        {
            return gerentesCadastrados.Any(gr => gr.Matricula == matricula && gr.Email.ToLower() == email.ToLower() && gr.Tipo == TipoGerenteEnum.GR);
        }

        private bool ExisteAlgumGCCadastradoNoBancoComEstaMatriculaEEmail(int matricula, string email)
        {
            return gerentesCadastrados.Any(gr => gr.Matricula == matricula && gr.Email.ToLower() == email.ToLower() && gr.Tipo == TipoGerenteEnum.GC);
        }

        private bool ExisteAlgumGerenteCadatradoNoBancoComEsteEmail(string email)
        {
            return gerentesCadastrados.Any(gr => gr.Email.ToLower() == email.ToLower());
        }

        private bool ExisteAlgumGRAdicionadoNaListaGerentesDeRelacionamentoImportadosComEstaMatriculaEEmail(int matricula, string email)
        {
            return gerentesDeRelacionamentoImportados.Any(gr => gr.Matricula == matricula && gr.Email.ToLower() == email.ToLower());
        }

        private bool ExisteAlgumGCAdicionadoNaListaGerentesComercialImportadosComEstaMatriculaEEmail(int matricula, string email)
        {
            return gerentesComercialImportados.Any(gr => gr.Matricula == matricula && gr.Email.ToLower() == email.ToLower());
        }

        private bool ExisteAlgumGRAdicionadoNaListaGerentesDeRelacionamentoImportadosComEsteEmail(string email)
        {
            return gerentesDeRelacionamentoImportados.Any(gr => gr.Email.ToLower() == email.ToLower());
        }

        private bool ExisteAlgumGCAdicionadoNaListaGerentesComercialImportadosComEsteEmail(string email)
        {
            return gerentesComercialImportados.Any(gr => gr.Email.ToLower() == email.ToLower());
        }

        private bool ExisteAlgumaLinhaInvalidaComEsteEmail(string email)
        {
            return grLinhasInvalidas.Any(l => l.Entity.Email.ToLower() == email.ToLower());
        } 

        public void AdicionarGRLinhaValida(string cnpj, GRImportado gerenteDeRelacionamentoImportado)
        {
            grLinhasValidas.Add(new Linha<GRImportado>
            {
                Cnpj = cnpj,
                Entity = gerenteDeRelacionamentoImportado
            });
        }

        public void AdicionarGCLinhaValida(string cnpj, GCImportado gerenteComercialImportado)
        {
            gcLinhasValidas.Add(new Linha<GCImportado>
            {
                Cnpj = cnpj,
                Entity = gerenteComercialImportado
            });
        }

        public void AdicionarGRLinhaInvalida(string cnpj, GRImportado gerenteDeRelacionamentoImportado)
        {
            grLinhasInvalidas.Add(new Linha<GRImportado>
            {
                Cnpj = cnpj,
                Entity = gerenteDeRelacionamentoImportado
            });
        }

        public void AdicionarGCLinhaInvalida(string cnpj, GCImportado gerenteComercialImportado)
        {
            gcLinhasInvalidas.Add(new Linha<GCImportado>
            {
                Cnpj = cnpj,
                Entity = gerenteComercialImportado
            });
        }

        public void AtualizarGRLinhasValidasEInvalidas(GRImportado gerenteDeRelacionamentoImportado)
        {
            grLinhasInvalidas.AddRange(grLinhasValidas.Where(l => l.Entity.Email.ToLower() == gerenteDeRelacionamentoImportado.Email.ToLower()));
            grLinhasValidas = new List<Linha<GRImportado>>(grLinhasValidas.Where(l => l.Entity.Email.ToLower() != gerenteDeRelacionamentoImportado.Email.ToLower()));
        }

        public void AtualizarGCLinhasValidasEInvalidas(GCImportado gerenteComercialImportado)
        {
            gcLinhasInvalidas.AddRange(gcLinhasValidas.Where(l => l.Entity.Email.ToLower() == gerenteComercialImportado.Email.ToLower()));
            gcLinhasValidas = new List<Linha<GCImportado>>(gcLinhasValidas.Where(l => l.Entity.Email.ToLower() != gerenteComercialImportado.Email.ToLower()));
        }

        public void AdicionarGRImportadoNaListaGerentesDeRelacionamentoImportados(GRImportado gerenteDeRelacionamentoImportado)
        {
            gerentesDeRelacionamentoImportados.Add(gerenteDeRelacionamentoImportado);
        }

        public void AdicionarGCImportadoNaListaGerentesComercialImportados(GCImportado gerenteComercialImportado)
        {
            gerentesComercialImportados.Add(gerenteComercialImportado);
        }

        public void RemoverGRImportadoDaListaGerentesDeRelacionamentoImportados(GRImportado gerenteDeRelacionamentoImportado)
        {
            gerentesDeRelacionamentoImportados = new List<GRImportado>(gerentesDeRelacionamentoImportados.Where(g => g.Email.ToLower() != gerenteDeRelacionamentoImportado.Email.ToLower()));
        }

        public void RemoverGCImportadoDaListaGerentesComercialImportados(GCImportado gerenteComercialImportado)
        {
            gerentesComercialImportados = new List<GCImportado>(gerentesComercialImportados.Where(g => g.Email.ToLower() != gerenteComercialImportado.Email.ToLower()));
        }

        private void ValidarGRImportado(string cnpj, GRImportado gerenteDeRelacionamentoImportado) 
        {
            if (ExisteAlgumGRCadastradoNoBancoComEstaMatricula(gerenteDeRelacionamentoImportado.Matricula))
            {
                if (ExisteAlgumGRCadastradoNoBancoComEstaMatriculaEEmail(gerenteDeRelacionamentoImportado.Matricula, gerenteDeRelacionamentoImportado.Email))
                {
                    AdicionarGRLinhaValida(cnpj, gerenteDeRelacionamentoImportado);
                }
                else
                {
                    AdicionarGRLinhaInvalida(cnpj, gerenteDeRelacionamentoImportado);
                }
            }
            else
            {
                if (ExisteAlgumGerenteCadatradoNoBancoComEsteEmail(gerenteDeRelacionamentoImportado.Email))
                {
                    AdicionarGRLinhaInvalida(cnpj, gerenteDeRelacionamentoImportado);
                }
                else
                {
                    if (ExisteAlgumGRAdicionadoNaListaGerentesDeRelacionamentoImportadosComEstaMatriculaEEmail(gerenteDeRelacionamentoImportado.Matricula, gerenteDeRelacionamentoImportado.Email))
                    {
                        AdicionarGRLinhaValida(cnpj, gerenteDeRelacionamentoImportado);
                    }
                    else
                    {
                        if (ExisteAlgumGRAdicionadoNaListaGerentesDeRelacionamentoImportadosComEsteEmail(gerenteDeRelacionamentoImportado.Email)
                            || ExisteAlgumaLinhaInvalidaComEsteEmail(gerenteDeRelacionamentoImportado.Email))
                        {
                            RemoverGRImportadoDaListaGerentesDeRelacionamentoImportados(gerenteDeRelacionamentoImportado);
                            AtualizarGRLinhasValidasEInvalidas(gerenteDeRelacionamentoImportado);
                            AdicionarGRLinhaInvalida(cnpj, gerenteDeRelacionamentoImportado);
                        }
                        else
                        {
                            AdicionarGRLinhaValida(cnpj, gerenteDeRelacionamentoImportado);
                            AdicionarGRImportadoNaListaGerentesDeRelacionamentoImportados(gerenteDeRelacionamentoImportado);                            
                        }
                    }
                }
            }
        }

        private void ValidarGCImporatado(string cnpj, GCImportado gerenteComercialImportado)
        {
            if (ExisteAlgumGCCadastradoNoBancoComEstaMatricula(gerenteComercialImportado.Matricula))
            {
                if (ExisteAlgumGCCadastradoNoBancoComEstaMatriculaEEmail(gerenteComercialImportado.Matricula, gerenteComercialImportado.Email))
                {
                    AdicionarGCLinhaValida(cnpj, gerenteComercialImportado);
                }
                else
                {
                    AdicionarGCLinhaInvalida(cnpj, gerenteComercialImportado);
                }
            }
            else
            {
                if (ExisteAlgumGerenteCadatradoNoBancoComEsteEmail(gerenteComercialImportado.Email))
                {
                    AdicionarGCLinhaInvalida(cnpj, gerenteComercialImportado);
                }
                else
                {
                    if (ExisteAlgumGCAdicionadoNaListaGerentesComercialImportadosComEstaMatriculaEEmail(gerenteComercialImportado.Matricula, gerenteComercialImportado.Email))
                    {
                        AdicionarGCLinhaValida(cnpj, gerenteComercialImportado);
                    }
                    else
                    {
                        if (ExisteAlgumGCAdicionadoNaListaGerentesComercialImportadosComEsteEmail(gerenteComercialImportado.Email)
                            || ExisteAlgumaLinhaInvalidaComEsteEmail(gerenteComercialImportado.Email))
                        {
                            RemoverGCImportadoDaListaGerentesComercialImportados(gerenteComercialImportado);
                            AtualizarGCLinhasValidasEInvalidas(gerenteComercialImportado);
                            AdicionarGCLinhaInvalida(cnpj, gerenteComercialImportado);
                        }
                        else
                        {
                            AdicionarGCLinhaValida(cnpj, gerenteComercialImportado);
                            AdicionarGCImportadoNaListaGerentesComercialImportados(gerenteComercialImportado);
                        }
                    }
                }
            }
        }

        private void SalvarGRImportados(object arquivoId)
        {
            grMongoDBRepository.Salvar(new GRRegistro
            {
                ArquivoImportadoId = arquivoId,
                DataInclusao = DateTime.Now,
                GRImportados = gerentesDeRelacionamentoImportados
            });
        }

        private void SalvarGCImportados(object arquivoId)
        {
            gcMongoDBRepository.Salvar(new GCRegistro
            {
                ArquivoImportadoId = arquivoId,
                DataInclusao = DateTime.Now,
                GCImportados = gerentesComercialImportados
            });
        }

        private string ObterTelefoneSemMascara(string telefone)
        {
            return Regex.Replace(telefone, "[^0-9]", "");
        }

        #endregion
    }
}
