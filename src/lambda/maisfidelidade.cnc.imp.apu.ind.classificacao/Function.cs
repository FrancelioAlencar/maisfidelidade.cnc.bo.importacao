using Amazon.Lambda.Core;
using Amazon.Lambda.RuntimeSupport;
using Amazon.Lambda.Serialization.Json;
using Amazon.Lambda.SNSEvents;
using maisfidelidade.cnc.imp.core.Common.AWS;
using maisfidelidade.cnc.imp.core.Common.Helpers;
using maisfidelidade.cnc.imp.core.Common.DependencyInjection;
using maisfidelidade.cnc.imp.core.Common.Logging;
using maisfidelidade.cnc.imp.core.Domain.Model.Arquivos;
using maisfidelidade.cnc.imp.core.Domain.Model.ClassificacaoIndicadores;
using maisfidelidade.cnc.imp.core.Domain.Model.Enums;
using MongoDB.Bson;
using MoreLinq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace maisfidelidade.cnc.imp.apu.ind.classificacao
{
	public class Function : FunctionBase
	{
		private readonly IArquivoMongoDBRepository arquivoMongoDBRepository;
		private readonly IClassificacaoIndicadorMongoDBRepository classificacaoIndicadorMongoDBRepository;
		private readonly IClassificacaoIndicadorMySqlRepository classificacaoIndicadorMySqlRepository;
		private IList<ClassificacaoIndicadorApuracaoImportado> classificacaoIndicadorApuracoes = new List<ClassificacaoIndicadorApuracaoImportado>();

		public Function()
			: base(LogContextEnum.ApuracaoClassificacaoIndicador)
		{
			arquivoMongoDBRepository = DependencyResolver.GetService<IArquivoMongoDBRepository>();
			classificacaoIndicadorMongoDBRepository = DependencyResolver.GetService<IClassificacaoIndicadorMongoDBRepository>();
			classificacaoIndicadorMySqlRepository = DependencyResolver.GetService<IClassificacaoIndicadorMySqlRepository>();
		}
		private static async Task Main(string[] args)
		{
			Func<string, ILambdaContext, string> func = null; //FunctionHandler;
			using (var handlerWrapper = HandlerWrapper.GetHandlerWrapper(func, new JsonSerializer()))
			using (var bootstrap = new LambdaBootstrap(handlerWrapper))
			{
				await bootstrap.RunAsync();
			}
		}

		public bool FunctionHandler(SNSEvent snsEvent, ILambdaContext lambdaContext)
		{
			object arquivoImportadoId = null;

			try
			{
				BeginLogInfo(lambdaContext, arquivoImportadoId, "A solicitação de processamento da apuração do indicador de classificação foi iniciada.");

				if (IsSnsMessageInvalid(snsEvent, out object arquivoImportadId))
				{
					throw new Exception("");;
				}

				var classificacaoIndicadoresCadastrados = classificacaoIndicadorMySqlRepository.ObterClassificacaoIndicadores();

				var arquivoImportado = arquivoMongoDBRepository.BuscarPor(arquivoImportadId);

				var quantidadeLinhas = 5;
				
				var lotes = arquivoImportado.ObterQuantidadeLotes(quantidadeLinhas);
				var colunas = arquivoMongoDBRepository.ObterColunas(GrupoColunaEnum.ClassificacaoIndicador);

				for (int lote = 0; lote < lotes; lote++)
				{
					var skip = lote * quantidadeLinhas;

					var arquivoImportadoLinhas = arquivoMongoDBRepository.BuscarArquivoImportadoLinhas(arquivoImportado.Id, skip, quantidadeLinhas);

					foreach (var arquivoImportadoLinha in arquivoImportadoLinhas)
					{
						var classificacaoIndicadorApuracao = new ClassificacaoIndicadorApuracaoImportado
						{
							Guid = Guid.Parse(arquivoImportadoLinha.Celunas[ArquivoColunaIndexes.GUID].ToString()),
							Cnpj = arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.NO_CNPJ].Nome].ToString()
						};

						classificacaoIndicadoresCadastrados.ForEach(classificacaoIndicador =>
						{
							switch (classificacaoIndicador.Tipo)
							{
								case ClassificacaoIndicadorEnum.IPSeguros:
									classificacaoIndicadorApuracao.ClassificacaoIndicadorApurados.Add(ObterClassificacaoIPSegurosIndicadorApurado(arquivoImportadoLinha, colunas, classificacaoIndicador.Id));
									break;
								case ClassificacaoIndicadorEnum.MarketShare:
									classificacaoIndicadorApuracao.ClassificacaoIndicadorApurados.Add(ObterClassificacaoMarketShareIndicadorApurado(arquivoImportadoLinha, colunas, classificacaoIndicador.Id));
									break;
							}
						});				
						
						classificacaoIndicadorApuracoes.Add(classificacaoIndicadorApuracao);
					}
				}

				SalvarClassificacaoIndicadorApuracoes(arquivoImportadId);

				AWSHelper.EnviarSNS(arquivoImportadoId.ToString(), "sns-orquestracao-apuracao");

				EndLogInfo(lambdaContext, arquivoImportadoId, "A solicitação de processamento da apuração do indicador de classificação foi concluída com sucesso.");

				return true;
			}
			catch (Exception exception)
			{
				LogError(lambdaContext, arquivoImportadoId, $"Falha na solicitação de processamento da apuração do indicador de classificação - {exception.Message}", exception.StackTrace, exception);

				return false;
			}
		}

		private ClassificacaoIndicadorApurado ObterClassificacaoIPSegurosIndicadorApurado(ArquivoImportadoLinha arquivoImportadoLinha, IDictionary<int, ArquivoColuna> colunas, object idClassificacao)
		{
			return new IPSegurosClassificacaoIndicadorApurado
			{
				ClassificacaoIndicadorId = Convert.ToInt32(idClassificacao),
				Porcentagem = Convert.ToInt32(arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.IPS_PC].Nome])
			};
		}

		private ClassificacaoIndicadorApurado ObterClassificacaoMarketShareIndicadorApurado(ArquivoImportadoLinha arquivoImportadoLinha, IDictionary<int, ArquivoColuna> colunas, object idClassificacao)
		{
			return new MarketShareClassificacaoIndicadorApurado
			{
				ClassificacaoIndicadorId = Convert.ToInt32(idClassificacao),
				Porcentagem = Convert.ToInt32(arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.MARKET_SHARE_PC].Nome])
			};
		}
		private bool IsSnsMessageInvalid(SNSEvent snsEvent, out object arquivoId)
		{
			var result = false;

			arquivoId = null;

			var snsMessage = snsEvent.Records?[0].Sns.Message;

			if (string.IsNullOrEmpty(snsMessage))
			{
				result = true;
			}

			if (!ObjectId.TryParse(snsMessage, out ObjectId objectId))
			{
				result = true;
			}

			if (objectId != ObjectId.Empty)
			{
				arquivoId = objectId;
			}

			return result;
		}

		private void SalvarClassificacaoIndicadorApuracoes(object arquivoImportadoId)
		{
			classificacaoIndicadorMongoDBRepository.Salvar(new ClassificacaoIndicadorRegistro
			{
				ArquivoImportadoId = arquivoImportadoId,
				DataInclusao = DateTime.Now,
				ClassificacaoIndicadorApuracoes = classificacaoIndicadorApuracoes
			});
		}
	}

}





