using Amazon.Lambda.Core;
using Amazon.Lambda.RuntimeSupport;
using Amazon.Lambda.Serialization.Json;
using Amazon.Lambda.SNSEvents;
using maisfidelidade.cnc.imp.core.Common.AWS;
using maisfidelidade.cnc.imp.core.Common.Helpers;
using maisfidelidade.cnc.imp.core.Common.DependencyInjection;
using maisfidelidade.cnc.imp.core.Common.Logging;
using maisfidelidade.cnc.imp.core.Domain.Model.Apuracoes;
using maisfidelidade.cnc.imp.core.Domain.Model.Arquivos;
using maisfidelidade.cnc.imp.core.Domain.Model.Categorias;
using maisfidelidade.cnc.imp.core.Domain.Model.Enums;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace maisfidelidade.cnc.imp.apuracao
{
    public class Function : FunctionBase
    {
        private readonly IArquivoMongoDBRepository arquivoMongoDBRepository;
        private readonly IApuracaoMongoDBRepository apuracaoMongoDBRepository;
        private readonly ICategoriaMySqlRepository categoriaMySqlRepository;

        private IList<ApuracaoImportada> apuracoes;

        public Function()
            : base(LogContextEnum.Apuracao)
        {
            arquivoMongoDBRepository = DependencyResolver.GetService<IArquivoMongoDBRepository>();
            apuracaoMongoDBRepository = DependencyResolver.GetService<IApuracaoMongoDBRepository>();
            categoriaMySqlRepository = DependencyResolver.GetService<ICategoriaMySqlRepository>();
        }

        private static async Task Main(string[] args)
        {
            Func<string, ILambdaContext, string> func = null;// FunctionHandler;
            using(var handlerWrapper = HandlerWrapper.GetHandlerWrapper(func, new JsonSerializer()))
            using(var bootstrap = new LambdaBootstrap(handlerWrapper))
            {
                await bootstrap.RunAsync();
            }
        }

        public bool FunctionHandler(SNSEvent snsEvent, ILambdaContext lambdaContext)
        {
            object arquivoImportadoId = null;

            try
            {
                BeginLogInfo(lambdaContext, arquivoImportadoId, "A solicitação de processamento da apuração foi iniciada.");

                if (IsSnsMessageInvalid(snsEvent, out arquivoImportadoId))
                {
                    throw new Exception("");
                }

                var arquivoImportado = arquivoMongoDBRepository.BuscarPor(arquivoImportadoId);

                var quantidadeLinhas = 5;

                var lotes = arquivoImportado.ObterQuantidadeLotes(quantidadeLinhas);

                var colunas = arquivoMongoDBRepository.ObterColunas(GrupoColunaEnum.Apuracao);
                var categorias = categoriaMySqlRepository.ObterCategorias();

                apuracoes = new List<ApuracaoImportada>();

                for (int lote = 0; lote < lotes; lote++)
                {
                    var skip = lote * quantidadeLinhas;

                    var arquivoImportadoLinhas = arquivoMongoDBRepository.BuscarArquivoImportadoLinhas(arquivoImportado.Id, skip, quantidadeLinhas);
                    
                    foreach (var arquivoImportadoLinha in arquivoImportadoLinhas)
                    {
                        apuracoes.Add(new ApuracaoImportada
                        {
                            Guid = Guid.Parse(arquivoImportadoLinha.Celunas[ArquivoColunaIndexes.GUID].ToString()),
                            Cnpj = arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.NO_CNPJ].Nome].ToString(),
                            CategoriaId = ObterCatoriaId(arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.CATEGORIA_ATUAL].Nome].ToString(), categorias),
                            FinanciamentoSantander = Convert.ToDecimal(arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.FINANCIADO_SANTANDER].Nome]),
                            FinanciamentoLoja = Convert.ToDecimal(arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.FINANCIADO_LOJA].Nome]),
                            QuantidadeContratos = Convert.ToInt32(arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.QTD_CONTRATOS].Nome]),
                            QuantidadePrestamista = Convert.ToInt32(arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.QTD_PRESTAMISTA].Nome]),
                            QuantidadeAuto = Convert.ToInt32(arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.QTD_AUTO].Nome]),
                            QuantidadeAmbos = Convert.ToInt32(arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.QTD_AMBOS].Nome]),
                            PlusMesAnterior = Convert.ToDouble(arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.PLUS_MES_ANTERIOR].Nome]),
                            PlusMesVigente = Convert.ToDouble(arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.PLUS_MES_VIGENTE].Nome]),
                            PlusProximoMes = Convert.ToDouble(arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.PLUS_PROXIMO_MES].Nome]),
                            AutoMesAnterior = Convert.ToDouble(arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.AUTO_MES_ANTERIOR].Nome]),
                            AutoMesVigente = Convert.ToDouble(arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.AUTO_MES_VIGENTE].Nome]),
                            AutoProximoMes = Convert.ToDouble(arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.AUTO_PROXIMO_MES].Nome]),
                            PrestamistaMesAnterior = Convert.ToDouble(arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.PRESTAMISTA_MES_ANTERIOR].Nome]),
                            PrestamistaMesVigente = Convert.ToDouble(arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.PRESTAMISTA_MES_VIGENTE].Nome]),
                            PrestamistaProximoMes = Convert.ToDouble(arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.PRESTAMISTA_PROXIMO_MES].Nome])
                        });
                    }
                }

                SalvarApuracoes(arquivoImportadoId);

                AWSHelper.EnviarSNS(arquivoImportadoId.ToString(), "sns-apuracao-segmentacao");

                EndLogInfo(lambdaContext, arquivoImportadoId, "A solicitação de processamento da apuração foi concluída com sucesso.");

                return true;
            }
            catch (Exception exception)
            {
                LogError(lambdaContext, arquivoImportadoId, $"Falha na solicitação de processamento da apuração - {exception.Message}", exception.StackTrace, exception);

                return false;
            }
        }

        #region M�todos Privados

        private bool IsSnsMessageInvalid(SNSEvent snsEvent, out object arquivoId)
        {
            var result = false;

            arquivoId = null;

            var snsMessage = snsEvent.Records?[0].Sns.Message;

            if (string.IsNullOrEmpty(snsMessage))
            {
                result = true;
            }

            if (!ObjectId.TryParse(snsMessage, out ObjectId objectId))
            {
                result = true;
            }

            if (objectId != ObjectId.Empty)
            {
                arquivoId = objectId;
            }

            return result;
        }

        private void SalvarApuracoes(object arquivoImportadoId)
        {
            apuracaoMongoDBRepository.Salvar(new ApuracaoRegistro
            {
                ArquivoImportadoId = arquivoImportadoId,
                DataInclusao = DateTime.Now,
                Apuracoes = apuracoes
            });
        }

        private int ObterCatoriaId(string nomeCategoria, IList<Categoria> categorias)
        {
            try
            {
                return Convert.ToInt32(categorias.FirstOrDefault(s => s.Nome.ToUpper().Equals(nomeCategoria.ToUpper())).Id);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion
    }
}
