using Amazon.Lambda.Core;
using Amazon.Lambda.RuntimeSupport;
using Amazon.Lambda.SNSEvents;
using maisfidelidade.cnc.imp.core.Common.AWS;
using maisfidelidade.cnc.imp.core.Common.DependencyInjection;
using maisfidelidade.cnc.imp.core.Common.Helpers;
using maisfidelidade.cnc.imp.core.Common.Logging;
using maisfidelidade.cnc.imp.core.Domain.Model.Arquivos;
using maisfidelidade.cnc.imp.core.Domain.Model.OrquestracaoApuracao;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace maisfidelidade.cnc.imp.orq.apuracao
{
	public class Function : FunctionBase
	{
		private readonly IArquivoMongoDBRepository arquivoMongoDBRepository;
		public Function()
			: base(LogContextEnum.OrquestracaoApuracao)
		{
			arquivoMongoDBRepository = DependencyResolver.GetService<IArquivoMongoDBRepository>();
		}

		private static async Task Main(string[] args)
        {
			Func<string, ILambdaContext, string> func = null;// FunctionHandler;
			using (var handlerWrapper = HandlerWrapper.GetHandlerWrapper(func, new Amazon.Lambda.Serialization.Json.JsonSerializer()))
			using (var bootstrap = new LambdaBootstrap(handlerWrapper))
			{
				await bootstrap.RunAsync();
			}
		}

		public bool FunctionHandler(SNSEvent snsEvent, ILambdaContext lambdaContext)
		{
			object arquivoImportadoId = null;

			try
			{
				BeginLogInfo(lambdaContext, arquivoImportadoId, "A solicitação de processamento da orquestração da apuração foi iniciada.");

				if (IsSnsMessageInvalid(snsEvent, out object arquivoImportadId, out ApuracaoEnum etapaCadastroAtual))
				{
					throw new Exception("");
				}

				var arquivoImportado = arquivoMongoDBRepository.BuscarPor(arquivoImportadId);

				if (!arquivoImportado.EtapaBannerConcluida || !arquivoImportado.EtapaCampanhaConcluida ||
					!arquivoImportado.EtapaClassificacaoConcluida || !arquivoImportado.EtapaPerformanceConcluida ||
					!arquivoImportado.EtapaRentabilidadeConcluida)
				{
					switch (etapaCadastroAtual)
					{
						case ApuracaoEnum.Banner:
							arquivoImportado.EtapaBannerConcluida = true;
							break;
						case ApuracaoEnum.Campanha:
							arquivoImportado.EtapaCampanhaConcluida = true;
							break;
						case ApuracaoEnum.IndClassificacao:
							arquivoImportado.EtapaClassificacaoConcluida = true;
							break;
						case ApuracaoEnum.IndPerformance:
							arquivoImportado.EtapaPerformanceConcluida = true;
							break;
						case ApuracaoEnum.IndRentabilidade:
							arquivoImportado.EtapaRentabilidadeConcluida = true;
							break;
						default:
							break;
					}

					arquivoMongoDBRepository.Salvar(arquivoImportado);

					bool sendSNS = arquivoImportado.EtapaBannerConcluida && arquivoImportado.EtapaCampanhaConcluida &&
					arquivoImportado.EtapaClassificacaoConcluida && arquivoImportado.EtapaPerformanceConcluida &&
					arquivoImportado.EtapaRentabilidadeConcluida;

					if (sendSNS)
					{
						AWSHelper.EnviarSNS(arquivoImportadId.ToString(), "sns-publicacao");
					}
				}

				EndLogInfo(lambdaContext, arquivoImportadoId, "A solicitação de processamento da orquestração da apuração foi concluída com sucesso.");

				return true;
			}
			catch (Exception exception)
			{
				LogError(lambdaContext, arquivoImportadoId, $"Falha na solicitação de processamento da orquestração da apuração - {exception.Message}", exception.StackTrace, exception);

				return false;
			}
		}
		private bool IsSnsMessageInvalid(SNSEvent snsEvent, out object arquivoId, out ApuracaoEnum etapaApuracaoAtual)
		{
			var result = false;

			arquivoId = null;
			etapaApuracaoAtual = ApuracaoEnum.Default;

			var snsMessage = snsEvent.Records?[0].Sns.Message;

			if (string.IsNullOrEmpty(snsMessage))
			{
				result = true;
			}

			var snsOrquestracaoMessage = JsonConvert.DeserializeObject<SnsOrquestracaoApuracao>(snsMessage, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Include });

			if (snsOrquestracaoMessage.ObjectId != string.Empty)
				arquivoId = snsOrquestracaoMessage.ObjectId;

			if (Enum.IsDefined(typeof(ApuracaoEnum), snsOrquestracaoMessage.EtapaApuracao))
				etapaApuracaoAtual = snsOrquestracaoMessage.EtapaApuracao;

			return result;
		}
	}
}
