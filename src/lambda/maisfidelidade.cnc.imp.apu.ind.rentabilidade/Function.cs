using Amazon.Lambda.Core;
using Amazon.Lambda.RuntimeSupport;
using Amazon.Lambda.Serialization.Json;
using Amazon.Lambda.SNSEvents;
using maisfidelidade.cnc.imp.core.Common.AWS;
using maisfidelidade.cnc.imp.core.Common.Helpers;
using maisfidelidade.cnc.imp.core.Common.DependencyInjection;
using maisfidelidade.cnc.imp.core.Common.Logging;
using maisfidelidade.cnc.imp.core.Domain.Model.Arquivos;
using maisfidelidade.cnc.imp.core.Domain.Model.Enums;
using maisfidelidade.cnc.imp.core.Domain.Model.RentabilidadeIndicadores;
using MongoDB.Bson;
using MoreLinq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace maisfidelidade.cnc.imp.apu.ind.rentabilidade
{
	public class Function : FunctionBase
	{
		private readonly IArquivoMongoDBRepository arquivoMongoDBRepository;
		private readonly IRentabilidadeIndicadorMongoDBRepository rentabilidadeIndicadorMongoDBRepository;
		private readonly IRentabilidadeIndicadorMySqlRepository rentabilidadeIndicadorMySqlRepository;

		private IList<RentabilidadeIndicador> rentabilidadeIndicadoresCadastrados;
		private IList<RentabilidadeBeneficio> rentabilidadeBeneficiosCadastrados;
		private IList<RentabilidadeApuracaoImportada> rentabilidadeApuracoes = new List<RentabilidadeApuracaoImportada>();

		public Function()
			: base(LogContextEnum.ApuracaoRentabilidadeIndicador)
		{
			arquivoMongoDBRepository = DependencyResolver.GetService<IArquivoMongoDBRepository>();
			rentabilidadeIndicadorMongoDBRepository = DependencyResolver.GetService<IRentabilidadeIndicadorMongoDBRepository>();
			rentabilidadeIndicadorMySqlRepository = DependencyResolver.GetService<IRentabilidadeIndicadorMySqlRepository>();
		}
		private static async Task Main(string[] args)
		{
			Func<string, ILambdaContext, string> func = null;// FunctionHandler;
			using (var handlerWrapper = HandlerWrapper.GetHandlerWrapper(func, new JsonSerializer()))
			using (var bootstrap = new LambdaBootstrap(handlerWrapper))
			{
				await bootstrap.RunAsync();
			}
		}
		public bool FunctionHandler(SNSEvent snsEvent, ILambdaContext lambdaContext)
		{
			object arquivoImportadoId = null;

			try
			{
				BeginLogInfo(lambdaContext, arquivoImportadoId, "A solicita��o de processamento da apura��o do indicador de rentabilidade foi iniciada.");

				if (IsSnsMessageInvalid(snsEvent, out arquivoImportadoId))
				{
					throw new Exception("");
				}

				rentabilidadeIndicadoresCadastrados = rentabilidadeIndicadorMySqlRepository.ObterRentabilidadeIndicadores();
				rentabilidadeBeneficiosCadastrados = rentabilidadeIndicadorMySqlRepository.ObterRentabilidadeBeneficios();

				var arquivoImportado = arquivoMongoDBRepository.BuscarPor(arquivoImportadoId);

				var quantidadeLinhas = 5;

				var lotes = arquivoImportado.ObterQuantidadeLotes(quantidadeLinhas);
				var colunas = arquivoMongoDBRepository.ObterColunas(GrupoColunaEnum.RentabilidadeIndicador);

				for (int lote = 0; lote < lotes; lote++)
				{
					var skip = lote * quantidadeLinhas;

					var arquivoImportadoLinhas = arquivoMongoDBRepository.BuscarArquivoImportadoLinhas(arquivoImportado.Id, skip, quantidadeLinhas);

					foreach (var arquivoImportadoLinha in arquivoImportadoLinhas)
					{
						var rentabilidadeApuracao = new RentabilidadeApuracaoImportada
						{
							Guid = Guid.Parse(arquivoImportadoLinha.Celunas[ArquivoColunaIndexes.GUID].ToString()),
							Cnpj = arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.NO_CNPJ].Nome].ToString()
						};

						rentabilidadeIndicadoresCadastrados.ForEach(rentabilidadeIndicador =>
						{
							GerarItensRentabilidade(colunas, arquivoImportadoLinha, rentabilidadeApuracao, rentabilidadeIndicador);
							GerarAgrupadoresRentabilidade(colunas, arquivoImportadoLinha, rentabilidadeApuracao, rentabilidadeIndicador);
							GerarConsolidadoRentabilidade(colunas, arquivoImportadoLinha, rentabilidadeApuracao, rentabilidadeIndicador);
						});

						rentabilidadeBeneficiosCadastrados.ForEach(beneficio =>
						{
							GerarBeneficiosRentabilidade(colunas, arquivoImportadoLinha, rentabilidadeApuracao, beneficio);
						});

						rentabilidadeApuracoes.Add(rentabilidadeApuracao);
					}
				}

				SalvarRentabilidadeIndicadorApuracoes(arquivoImportadoId);

				AWSHelper.EnviarSNS(arquivoImportadoId.ToString(), "sns-orquestracao-apuracao");

				EndLogInfo(lambdaContext, arquivoImportadoId, "A solicita��o de processamento da apura��o do indicador de rentabilidade foi conclu�da com sucesso.");

				return true;
			}
			catch (Exception exception)
			{
				LogError(lambdaContext, arquivoImportadoId, $"Falha na solicita��o de processamento da apura��o do indicador de rentabilidade - {exception.Message}", exception.StackTrace, exception);

				return false;
			}
		}


		#region M�todos Privados

		private void GerarItensRentabilidade(IDictionary<int, ArquivoColuna> colunas, ArquivoImportadoLinha arquivoImportadoLinha, RentabilidadeApuracaoImportada clienteApurado, RentabilidadeIndicador rentabilidadeIndicador)
		{
			switch (rentabilidadeIndicador.Tipo)
			{
				case RentabilidadeIndicadorEnum.Auto:
					clienteApurado.RentabilidadeIndicadorApurados.Add(ObterAutoRentabilidadeIndicadorApurado(arquivoImportadoLinha, colunas, rentabilidadeIndicador.Id));
					break;
				case RentabilidadeIndicadorEnum.Plus:
					clienteApurado.RentabilidadeIndicadorApurados.Add(ObterPlusRentabilidadeIndicadorApurado(arquivoImportadoLinha, colunas, rentabilidadeIndicador.Id));
					break;
				case RentabilidadeIndicadorEnum.Prestamista:
					clienteApurado.RentabilidadeIndicadorApurados.Add(ObterPrestamistaRentabilidadeIndicadorApurado(arquivoImportadoLinha, colunas, rentabilidadeIndicador.Id));
					break;
				case RentabilidadeIndicadorEnum.Retorno:
					clienteApurado.RentabilidadeIndicadorApurados.Add(ObterRetornoRentabilidadeIndicadorApurado(arquivoImportadoLinha, colunas, rentabilidadeIndicador.Id));
					break;
			}
		}

		private void GerarAgrupadoresRentabilidade(IDictionary<int, ArquivoColuna> colunas, ArquivoImportadoLinha arquivoImportadoLinha, RentabilidadeApuracaoImportada clienteApurado, RentabilidadeIndicador rentabilidadeIndicador)
		{
			switch (rentabilidadeIndicador.Tipo)
			{
				case RentabilidadeIndicadorEnum.Marketing:
					clienteApurado.RentabilidadeIndicadorApurados.Add(ObterMarketingRentabilidadeIndicadorApurado(arquivoImportadoLinha, colunas, rentabilidadeIndicador.Id));
					break;
				case RentabilidadeIndicadorEnum.Financiamentos:
					clienteApurado.RentabilidadeIndicadorApurados.Add(ObterFinanciamentosRentabilidadeIndicadorApurado(arquivoImportadoLinha, colunas, rentabilidadeIndicador.Id));
					break;
				case RentabilidadeIndicadorEnum.Seguros:
					clienteApurado.RentabilidadeIndicadorApurados.Add(ObterSegurosRentabilidadeIndicadorApurado(arquivoImportadoLinha, colunas, rentabilidadeIndicador.Id));
					break;
			}
		}

		private void GerarBeneficiosRentabilidade(IDictionary<int, ArquivoColuna> colunas, ArquivoImportadoLinha arquivoImportadoLinha, RentabilidadeApuracaoImportada clienteApurado, RentabilidadeBeneficio Beneficio)
		{
			switch (Beneficio.Tipo)
			{
				case RentabilidadeBeneficiosEnum.Crv:
					clienteApurado.RentabilidadeBeneficioApurados.Add(ObterBeneficioCRVApurado(arquivoImportadoLinha, colunas, Beneficio.Id));
					break;
				case RentabilidadeBeneficiosEnum.Gravame:
					clienteApurado.RentabilidadeBeneficioApurados.Add(ObterBeneficioGravameApurado(arquivoImportadoLinha, colunas, Beneficio.Id));
					break;
				case RentabilidadeBeneficiosEnum.MesaCredito:
					clienteApurado.RentabilidadeBeneficioApurados.Add(ObterBeneficioMesaCreditoApurado(arquivoImportadoLinha, colunas, Beneficio.Id));
					break;
			}
		}

		private RentabilidadeBeneficioApurado ObterBeneficioMesaCreditoApurado(ArquivoImportadoLinha arquivoImportadoLinha, IDictionary<int, ArquivoColuna> colunas, object idBeneficio)
		{
			return new RentabilidadeBeneficioApuradoMesaCredito
			{
				RentabilidadeBeneficioId = Convert.ToInt32(idBeneficio),
				PossuiMesaCredito = Convert.ToBoolean(Convert.ToInt16(arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.POSSUI_MESA_CREDITO].Nome]))
			};
		}

		private RentabilidadeBeneficioApurado ObterBeneficioGravameApurado(ArquivoImportadoLinha arquivoImportadoLinha, IDictionary<int, ArquivoColuna> colunas, object idBeneficio)
		{
			return new RentabilidadeBeneficioApuradoGravame
			{
				RentabilidadeBeneficioId = Convert.ToInt32(idBeneficio),
				QuantidadeGravame = Convert.ToInt32(arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.QTD_GRAVAME].Nome])
			};
		}

		private RentabilidadeBeneficioApurado ObterBeneficioCRVApurado(ArquivoImportadoLinha arquivoImportadoLinha, IDictionary<int, ArquivoColuna> colunas, object idBeneficio)
		{
			return new RentabilidadeBeneficioApuradoCRV
			{
				RentabilidadeBeneficioId = Convert.ToInt32(idBeneficio),
				PossuiCRV = Convert.ToBoolean(Convert.ToInt16(arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.POSSUI_CRV].Nome]))
			};
		}

		private void GerarConsolidadoRentabilidade(IDictionary<int, ArquivoColuna> colunas, ArquivoImportadoLinha arquivoImportadoLinha, RentabilidadeApuracaoImportada clienteApurado, RentabilidadeIndicador rentabilidadeIndicador)
		{
			if (rentabilidadeIndicador.Tipo == RentabilidadeIndicadorEnum.Consolidado)
				clienteApurado.RentabilidadeIndicadorApurados.Add(ObterConsolidadoRentabilidadeIndicadorApurado(arquivoImportadoLinha, colunas, rentabilidadeIndicador.Id));
		}
		private RentabilidadeIndicadorApurado ObterConsolidadoRentabilidadeIndicadorApurado(ArquivoImportadoLinha arquivoImportadoLinha, IDictionary<int, ArquivoColuna> colunas, object idIndicador)
		{
			return new RentabilidadeIndicadorApuradoConsolidado
			{
				RentabilidadeIndicadorId = Convert.ToInt32(idIndicador),
				Maximo = Convert.ToDouble(arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.CONSOLIDADO_MAXIMA].Nome]),
				Realizado = Convert.ToDouble(arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.CONSOLIDADO_REALIZADO].Nome])
			};
		}

		private RentabilidadeIndicadorApurado ObterSegurosRentabilidadeIndicadorApurado(ArquivoImportadoLinha arquivoImportadoLinha, IDictionary<int, ArquivoColuna> colunas, object idIndicador)
		{
			return new RentabilidadeIndicadorApuradoSeguros
			{
				RentabilidadeIndicadorId = Convert.ToInt32(idIndicador),
				Maximo = Convert.ToDouble(arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.SEGUROS_MAXIMA].Nome]),
				Realizado = Convert.ToDouble(arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.SEGUROS_REALIZADO].Nome])
			};
		}

		private RentabilidadeIndicadorApurado ObterFinanciamentosRentabilidadeIndicadorApurado(ArquivoImportadoLinha arquivoImportadoLinha, IDictionary<int, ArquivoColuna> colunas, object idIndicador)
		{
			return new RentabilidadeIndicadorApuradoFinanciamentos
			{
				RentabilidadeIndicadorId = Convert.ToInt32(idIndicador),
				Maximo = Convert.ToDouble(arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.FINANCIAMENTOS_MAXIMA].Nome]),
				Realizado = Convert.ToDouble(arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.FINANCIAMENTOS_REALIZADO].Nome])
			};
		}

		private RentabilidadeIndicadorApurado ObterRetornoRentabilidadeIndicadorApurado(ArquivoImportadoLinha arquivoImportadoLinha, IDictionary<int, ArquivoColuna> colunas, object idIndicador)
		{
			return new RentabilidadeIndicadorApuradoRetorno
			{
				RentabilidadeIndicadorId = Convert.ToInt32(idIndicador),
				Maximo = Convert.ToDouble(arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.RETORNO_MAXIMA].Nome]),
				Realizado = Convert.ToDouble(arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.RETORNO_REALIZADO].Nome])
			};
		}

		private RentabilidadeIndicadorApurado ObterPrestamistaRentabilidadeIndicadorApurado(ArquivoImportadoLinha arquivoImportadoLinha, IDictionary<int, ArquivoColuna> colunas, object idIndicador)
		{
			return new RentabilidadeIndicadorApuradoPrestamista
			{
				RentabilidadeIndicadorId = Convert.ToInt32(idIndicador),
				Maximo = Convert.ToDouble(arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.PRESTAMISTA_MAXIMA].Nome]),
				Realizado = Convert.ToDouble(arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.PRESTAMISTA_REALIZADO].Nome])
			};
		}

		private RentabilidadeIndicadorApurado ObterPlusRentabilidadeIndicadorApurado(ArquivoImportadoLinha arquivoImportadoLinha, IDictionary<int, ArquivoColuna> colunas, object idIndicador)
		{
			return new RentabilidadeIndicadorApuradoPlus
			{
				RentabilidadeIndicadorId = Convert.ToInt32(idIndicador),
				Maximo = Convert.ToDouble(arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.PLUS_MAXIMA].Nome]),
				Realizado = Convert.ToDouble(arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.PLUS_REALIZADO].Nome])
			};
		}

		private RentabilidadeIndicadorApurado ObterMarketingRentabilidadeIndicadorApurado(ArquivoImportadoLinha arquivoImportadoLinha, IDictionary<int, ArquivoColuna> colunas, object idIndicador)
		{
			return new RentabilidadeIndicadorApuradoMarketing
			{
				RentabilidadeIndicadorId = Convert.ToInt32(idIndicador),
				Ativo = Convert.ToBoolean(Convert.ToInt16(arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.POSSUI_MARKETING].Nome]))
			};
		}

		private RentabilidadeIndicadorApurado ObterAutoRentabilidadeIndicadorApurado(ArquivoImportadoLinha arquivoImportadoLinha, IDictionary<int, ArquivoColuna> colunas, object idIndicador)
		{
			return new RentabilidadeIndicadorApuradoAuto
			{
				RentabilidadeIndicadorId = Convert.ToInt32(idIndicador),
				Maximo = Convert.ToDouble(arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.AUTO_MAXIMA].Nome]),
				Realizado = Convert.ToDouble(arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.AUTO_REALIZADO].Nome])
			};
		}
		private bool IsSnsMessageInvalid(SNSEvent snsEvent, out object arquivoId)
		{
			var result = false;

			arquivoId = null;

			var snsMessage = snsEvent.Records?[0].Sns.Message;

			if (string.IsNullOrEmpty(snsMessage))
			{
				result = true;
			}

			if (!ObjectId.TryParse(snsMessage, out ObjectId objectId))
			{
				result = true;
			}

			if (objectId != ObjectId.Empty)
			{
				arquivoId = objectId;
			}

			return result;
		}

		private void SalvarRentabilidadeIndicadorApuracoes(object arquivoImportadoId)
		{
			rentabilidadeIndicadorMongoDBRepository.Salvar(new RentabilidadeIndicadorRegistro
			{
				ArquivoImportadoId = arquivoImportadoId,
				DataInclusao = DateTime.Now,
				RentabilidadeApuracoes = rentabilidadeApuracoes
			});
		}

		#endregion
	}

}
