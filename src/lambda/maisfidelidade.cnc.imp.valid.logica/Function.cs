using Amazon.Lambda.Core;
using Amazon.Lambda.SQSEvents;
using maisfidelidade.cnc.imp.core.Domain.Model.Enums;
using maisfidelidade.cnc.imp.core.Common.AWS;
using maisfidelidade.cnc.imp.core.Common.Configuration;
using maisfidelidade.cnc.imp.core.Common.DependencyInjection;
using maisfidelidade.cnc.imp.core.Common.Helpers;
using maisfidelidade.cnc.imp.core.Common.Logging;
using maisfidelidade.cnc.imp.core.Domain;
using maisfidelidade.cnc.imp.core.Domain.Model.Arquivos;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace maisfidelidade.cnc.imp.validacao.logica
{
    public class Function : FunctionBase
    {
        private readonly IArquivoMongoDBRepository arquivoRepositorio;

        public Function()
            : base(LogContextEnum.ValidacaoLogica)
        {
            arquivoRepositorio = DependencyResolver.GetService<IArquivoMongoDBRepository>();           
        }

        public bool FunctionHandler(SQSEvent sqsEvent, ILambdaContext lambdaContext)
        {
            object arquivoImportadoId = null;

            try
            {
                BeginLogInfo(lambdaContext, arquivoImportadoId, "A solicitação de processamento da validação lógica foi iniciada.");

                lambdaContext.SetEnvironment();

                var sqsBody = sqsEvent.Records?[0].Body;

                if (!ValidarDadoRecebido(lambdaContext, sqsBody, out ObjectId arquivoId))
                    return false;

                ArquivoImportado arquivoImportado = arquivoRepositorio.BuscarPor(arquivoId);

                //if (arquivoImportado.StatusAndamentoProcesso != StatusAndamentoProcessoEnum.AguardandoValidacaoLogica)
                //{
                //    return true;
                //}

                arquivoImportado.StatusAndamentoProcesso = StatusAndamentoProcessoEnum.RealizandoValidacaoLogica;

                arquivoRepositorio.Salvar(arquivoImportado);  //arquivoMongoRepositorio.MudarStatusAndamentoProcesso(idArquivo, StatusAndamentoProcessoEnum.EfetuandoValidacaoLogica);

                ArquivoImportadoConteudo arquivoImportadoConteudo = arquivoRepositorio.BuscarConteudoPor(arquivoImportado.Id);

                var arquivoColunas = arquivoRepositorio.ObterColunas();

                var validacao = new List<MensagemValidacaoArquivoColuna>();

                foreach (var linha in arquivoImportadoConteudo.Linhas)
                {
                    foreach (var arquivoColuna in arquivoColunas.Values)
                    {
                        var valor = linha[arquivoColuna.Nome];                                               

                        if (!arquivoColuna.Validar(valor, out string erroMensagem))
                        {
                            int numeroLinha = arquivoImportadoConteudo.Linhas.IndexOf(linha) + 1;

                            validacao.Add(new MensagemValidacaoArquivoColuna(numeroLinha, arquivoColuna.Nome, string.Format("O dado [{0}] está inválido. {1}", valor, erroMensagem)));
                        }
                    }
                }

                if (validacao.Any())
                {
                    StringBuilder stringBuilder = new StringBuilder();

                    validacao.ForEach(m => stringBuilder.AppendLine(string.Format("Linha: {0} | Coluna: {1} | Mensagem: {2}", m.Linha, m.Coluna, m.Mensagem)));

                    //Logger.Log(arquivoId, StatusAndamentoProcessoEnum.RealizandoValidacaoLogica, StatusLogEnum.ValidacaoLogica, LogWatcher, TipoLog.Error, stringBuilder.ToString());                                       

                    arquivoImportado.StatusArquivo = StatusArquivoEnum.ErroAoProcessar; //arquivoMongoRepositorio.MudarStatus(idArquivo, StatusArquivoEnum.ErroAoProcessar);

                    arquivoRepositorio.Salvar(arquivoImportado);

                    return false;
                }

                AtualizarArquivo(arquivoImportado, arquivoImportadoConteudo, arquivoColunas);

                AWSHelper.EnviarSQS(arquivoId.ToString(), "sns-cadastros");

                EndLogInfo(lambdaContext, arquivoImportadoId, "A solicitação de processamento da validação lógica foi concluída com sucesso.");

                return true;
            }
            catch (Exception exception)
            {
                LogError(lambdaContext, arquivoImportadoId, $"Falha na solicitação de processamento da validação lógica - {exception.Message}", exception.StackTrace, exception);

                return true;
            }
        }

        #region Métodos Privados

        private bool ValidarDadoRecebido(ILambdaContext lambdaContext, string dadoRecebido, out ObjectId idArquivo)
        {
            if (string.IsNullOrEmpty(dadoRecebido))
            {
                //LogError(lambdaContext, LogContextEnum.ValidacaoLogica, LogWatcher, $"O conteúdo SQS está retornando nulo ou em branco.");

                idArquivo = ObjectId.Empty;

                return false;
            }

            if (!ObjectId.TryParse(dadoRecebido, out idArquivo))
            {
                //LogError(lambdaContext, LogContextEnum.ValidacaoLogica, LogWatcher, $"O formato do arquivoId está incorreto.");

                return false;
            }

            return true;
        }
        
        private void AtualizarArquivo(ArquivoImportado arquivoImportado, ArquivoImportadoConteudo arquivoImportadoConteudo, IDictionary<int, ArquivoColuna> arquivoColunas)
        {
            arquivoImportado.DataProcessamento = Convert.ToDateTime(arquivoImportadoConteudo.Linhas[0].Celunas[arquivoColunas[ArquivoColunaIndexes.DT_PROCESSAMENTO].Nome]);
            arquivoImportado.DataReferencia = Convert.ToDateTime(arquivoImportadoConteudo.Linhas[0].Celunas[arquivoColunas[ArquivoColunaIndexes.DT_REFERENCIA].Nome]);
            arquivoImportado.DataPrevisaoAtualizacao = Convert.ToDateTime(arquivoImportadoConteudo.Linhas[0].Celunas[arquivoColunas[ArquivoColunaIndexes.DT_PREVISAO_ATUALIZACAO].Nome]);
            arquivoImportado.TipoArquivo = arquivoImportadoConteudo.Linhas[0].Celunas[arquivoColunas[ArquivoColunaIndexes.REF_ARQUIVO].Nome].ToString() == "S" ? TipoArquivoEnum.Semanal : TipoArquivoEnum.Mensal;
                     
            // TODO Salvar status

            arquivoRepositorio.Salvar(arquivoImportado);
        }

        #endregion
    }
}
