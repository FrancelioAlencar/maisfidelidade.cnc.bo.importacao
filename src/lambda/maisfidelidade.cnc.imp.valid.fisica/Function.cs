using Amazon;
using Amazon.Lambda.Core;
using Amazon.Lambda.S3Events;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Util;
using maisfidelidade.cnc.imp.core.Common.AWS;
using maisfidelidade.cnc.imp.core.Common.DependencyInjection;
using maisfidelidade.cnc.imp.core.Common.Helpers;
using maisfidelidade.cnc.imp.core.Common.Logging;
using maisfidelidade.cnc.imp.core.Domain.Model.Arquivos;
using maisfidelidade.cnc.imp.core.Domain.Model.Enums;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;


[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace maisfidelidade.cnc.imp.valid.fisica
{
    public class Function : FunctionBase
    {
        private readonly IArquivoMySqlRepository arquivoMySqlRepository;
        private readonly IArquivoMongoDBRepository arquivoMongoDBRepository;

        public Function()
            : base(LogContextEnum.ValidacaoFisica)
        {
            arquivoMySqlRepository = DependencyResolver.GetService<IArquivoMySqlRepository>();
            arquivoMongoDBRepository = DependencyResolver.GetService<IArquivoMongoDBRepository>();
        }

        public async Task<bool> FunctionHandler(S3Event s3Event, ILambdaContext lambdaContext)
        {
            object arquivoImportadoId = null;

            try
            {
                BeginLogInfo(lambdaContext, arquivoImportadoId, "A solicitação de processamento da validação física foi iniciada.");

                LogInfo(lambdaContext, arquivoImportadoId, "Validar AWS S3 event e o nome do arquivo importado.");

                var s3 = s3Event.Records?[0].S3;

                if (s3 == null)
                {
                    throw new ArgumentNullException(nameof(s3), LogErrorMessage.S3Nulo);
                }

                string arquivoNome = ObterS3ArquivoNome(s3);

                if (ExtensaoArquivoImportadoInvalida(arquivoNome))
                {
                    throw new Exception(LogErrorMessage.ExtensaoArquivoImportadoInvalida);
                }

                if (ExisteArquivoImportadoOuPublicadoComMesmoNome(arquivoNome))
                {
                    throw new Exception(LogErrorMessage.ExisteArquivoImportadoOuPublicadoComMesmoNome);
                }

                LogInfo(lambdaContext, arquivoImportadoId, "Salvar novo registro de arquivo importado.");

                SalvarNovoArquivoImportado(arquivoNome);

                var arquivoImportado = ObterNovoArquivoImportadoCadastrado(arquivoNome);

                arquivoImportadoId = arquivoImportado.Id;

                if (arquivoImportado.StatusAndamentoProcesso != StatusAndamentoProcessoEnum.AguardandoValidacaoFisica) // se arquivo
                {
                    throw new Exception(LogErrorMessage.StatusDiferenteDeAguardandoValidacaoFisica);
                }

                LogInfo(lambdaContext, arquivoImportadoId, "Atualizar status do arquivo importado para RealizandoValidacaoFisica.");

                AtualizarStatusParaRealizandoValidacaoFisica(arquivoImportado);

                LogInfo(lambdaContext, arquivoImportadoId, "Criar client para acesso ao serviço da web Amazon S3");

                using (var client = new AmazonS3Client(RegionEndpoint.SAEast1))
                {
                    LogInfo(lambdaContext, arquivoImportadoId, "Obter arquivo .csv do bucket do S3");

                    var objectResponse = await client.GetObjectAsync(s3.Bucket.Name, s3.Object.Key);

                    using (var streamReader = new StreamReader(objectResponse.ResponseStream))
                    {
                        string csvConteudo = streamReader.ReadToEnd();

                        var csvLinhas = csvConteudo.Split(Environment.NewLine, StringSplitOptions.RemoveEmptyEntries);
                        var csvColunas = csvLinhas[0].Split(';');

                        var colunasFaltando = new List<string>();

                        LogInfo(lambdaContext, arquivoImportadoId, "Validar se arquivo importado possui todas as colunas exigidas.");

                        // TODO: Validar colunas com nome em branco
                        if (!ValidarColunas(csvColunas.ToList(), out colunasFaltando))
                        {
                            arquivoImportado.StatusArquivo = StatusArquivoEnum.ErroAoProcessar;

                            arquivoMongoDBRepository.Salvar(arquivoImportado);

                            throw new Exception($"Houve um erro ao validar as colunas do arquivo. Está faltando no arquivo as seguintes colunas: {string.Join(", ", colunasFaltando)}");
                        }

                        LogInfo(lambdaContext, arquivoImportadoId, "Criar objeto ArquivoImportadoConteudo a partir do conteúdo do arquivo .csv.");

                        ArquivoImportadoConteudo arquivoImportadoConteudo = CriarArquivoImportadoConteudo(arquivoImportado, csvLinhas, csvColunas);

                        LogInfo(lambdaContext, arquivoImportadoId, "Salvar conteúdo do arquivo importado.");

                        arquivoImportado.TotalLinhasProcessadas = arquivoImportadoConteudo.Linhas.Count;
                        arquivoImportado.StatusAndamentoProcesso = StatusAndamentoProcessoEnum.AguardandoValidacaoLogica;

                        arquivoMongoDBRepository.SalvarConteudo(arquivoImportadoConteudo);
                        arquivoMongoDBRepository.Salvar(arquivoImportado);

                        //await MoverArquivoAsync(s3.Bucket.Name, s3.Object.Key, s3.Bucket.Name, ObterNovoObjectKey(s3.Object, "processando"));
                    }
                }

                AWSHelper.EnviarSQS(arquivoImportado.Id.ToString(), "sqs-validacao-logica");

                EndLogInfo(lambdaContext, arquivoImportadoId, "A solicitação de processamento da validação física foi concluída com sucesso.");

                return true;
            }
            catch (Exception exception)
            {
                LogError(lambdaContext, arquivoImportadoId, $"Falha na solicitação de processamento da validação física - {exception.Message}", exception.StackTrace, exception);

                return true;
            }
        }

        #region Métodos Privados

        private static string ObterS3ArquivoNome(S3EventNotification.S3Entity s3)
        {
            return Path.GetFileName(s3.Object.Key);
        }

        private bool ExtensaoArquivoImportadoInvalida(string arquivoNome)
        {
            try
            {
                if (string.IsNullOrEmpty(arquivoNome))
                {
                    throw new ArgumentException("Nome do arquivo está nulo ou em branco.", nameof(arquivoNome));
                }

                return (Path.GetExtension(arquivoNome) == ExtensaoArquivo.Csv) ? false : true;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        private bool ExisteArquivoImportadoOuPublicadoComMesmoNome(string arquivoNome)
        {
            try
            {
                var existeArquivoImportado = arquivoMongoDBRepository.BuscarPor(arquivoNome) != null ? true : false; 
                var existeArquivoPublicado = arquivoMySqlRepository.ExisteArquivo(arquivoNome);

                return (existeArquivoImportado || existeArquivoPublicado);
            }
            catch (Exception exception)
            {
                throw new Exception("Erro ao verificar se existe algum arquivo importado ou publicado com o mesmo nome", exception); 
            }
        }

        private void SalvarNovoArquivoImportado(string arquivoNome)
        {
            arquivoMongoDBRepository.Salvar(new ArquivoImportado
            {
                Nome = arquivoNome,
                TipoArquivo = null,
                DataProcessamento = null,
                DataReferencia = null,
                DataPrevisaoAtualizacao = null,
                DataImportacao = DateTime.Now,
                DataPublicacao = null,
                TotalLinhasProcessadas = 0,
                StatusArquivo = StatusArquivoEnum.Pendente,
                StatusAndamentoProcesso = StatusAndamentoProcessoEnum.AguardandoValidacaoFisica,
                EtapaEmpresaConcluida = false,
                EtapaGerenteConcluida = false,
                EtapaBannerConcluida = false,
                EtapaCampanhaConcluida = false,
                EtapaClassificacaoConcluida = false,
                EtapaPerformanceConcluida = false,
                EtapaRentabilidadeConcluida = false,
                DataInclusao = DateTime.Now
            });
        }

        private ArquivoImportado ObterNovoArquivoImportadoCadastrado(string arquivoNome)
        {
            return arquivoMongoDBRepository.BuscarPor(arquivoNome);
        }

        private bool ValidarColunas(List<string> colunas, out List<string> colunasFaltando)
        {
            try
            {
                if (colunas == null)
                {
                    throw new ArgumentNullException("colunas", "colunas está nula.");
                }

                colunas = colunas.ConvertAll(c => c.Trim().ToUpper());

                var arquivoColunas = arquivoMongoDBRepository.ObterColunas(); // Remover daqui para outro lugar

                bool result = arquivoColunas.All(c => colunas.Contains(c.Value.Nome.ToUpper()));

                colunasFaltando = arquivoColunas.Where(c => colunas.Contains(c.Value.Nome.ToUpper()) == false).Select(c => c.Value.Nome).ToList();

                return result;
            }
            catch (Exception exception)
            {
                throw new Exception("ValidarColunas", exception);
            }
        }

        private async Task MoverArquivoAsync(string sourceBucketName, string sourceObjectKey, string destinationBucketName, string destinationObjectKey)
        {
            try
            {
                using (var client = new AmazonS3Client(RegionEndpoint.SAEast1))
                {
                    CopyObjectRequest copyRequest = new CopyObjectRequest
                    {
                        SourceBucket = sourceBucketName,
                        SourceKey = sourceObjectKey,
                        DestinationBucket = destinationBucketName,
                        DestinationKey = destinationObjectKey
                    };

                    await client.CopyObjectAsync(copyRequest);

                    var deleteRequest = new DeleteObjectRequest
                    {
                        BucketName = sourceBucketName,
                        Key = sourceObjectKey
                    };

                    await client.DeleteObjectAsync(deleteRequest);
                }
            }
            catch (Exception exception)
            {
                throw new Exception("MoverArquivoAsync", exception);
            }
        }

        private ArquivoImportadoConteudo CriarArquivoImportadoConteudo(ArquivoImportado arquivoImportado, string[] csvLinhas, string[] csvColunas)
        {
            ArquivoImportadoConteudo arquivoImportadoConteudo = new ArquivoImportadoConteudo
            {
                ArquivoImportadoId = arquivoImportado.Id,
                DataInclusao = DateTime.Now
            };

            for (int indexLinha = 1; indexLinha < csvLinhas.Length; indexLinha++)
            {
                ArquivoImportadoLinha linha = new ArquivoImportadoLinha();

                linha.AdicionarCelula("GUID", Guid.NewGuid());

                var csvCelulas = csvLinhas[indexLinha].Split(';');

                for (int indexColuna = 0; indexColuna < csvCelulas.Length; indexColuna++)
                {
                    linha.AdicionarCelula(csvColunas[indexColuna].Trim(), csvCelulas[indexColuna].Trim());
                }

                arquivoImportadoConteudo.Linhas.Add(linha);
            }

            return arquivoImportadoConteudo;
        }

        private void AtualizarStatusParaRealizandoValidacaoFisica(ArquivoImportado arquivoImportado)
        {
            arquivoImportado.StatusArquivo = StatusArquivoEnum.Processando;
            arquivoImportado.StatusAndamentoProcesso = StatusAndamentoProcessoEnum.RealizandoValidacaoFisica;

            arquivoMongoDBRepository.Salvar(arquivoImportado);
        }

        #endregion
    }
}