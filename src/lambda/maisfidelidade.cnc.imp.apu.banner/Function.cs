using Amazon.Lambda.Core;
using Amazon.Lambda.RuntimeSupport;
using Amazon.Lambda.Serialization.Json;
using Amazon.Lambda.SNSEvents;
using maisfidelidade.cnc.imp.core.Common.AWS;
using maisfidelidade.cnc.imp.core.Common.Helpers;
using maisfidelidade.cnc.imp.core.Common.DependencyInjection;
using maisfidelidade.cnc.imp.core.Common.Logging;
using maisfidelidade.cnc.imp.core.Domain.Model.Arquivos;
using maisfidelidade.cnc.imp.core.Domain.Model.Banners;
using maisfidelidade.cnc.imp.core.Domain.Model.Enums;
using MongoDB.Bson;
using MoreLinq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace maisfidelidade.cnc.imp.apu.banner
{
	public class Function : FunctionBase
	{
		private readonly IArquivoMongoDBRepository arquivoMongoDBRepository;
		private readonly IBannerMySqlRepository bannersMySqlRepository;
		private readonly IBannerMongoDBRepository bannersMongoDBRepository;
		private readonly IList<BannerApuracaoImportada> empresasApurados = new List<BannerApuracaoImportada>();

		public Function()
			: base(LogContextEnum.ApuracaoBanner)
		{
			arquivoMongoDBRepository = DependencyResolver.GetService<IArquivoMongoDBRepository>();
			bannersMySqlRepository = DependencyResolver.GetService<IBannerMySqlRepository>();
			bannersMongoDBRepository = DependencyResolver.GetService<IBannerMongoDBRepository>();
		}
     
        private static async Task Main(string[] args)
        {
            Func<string, ILambdaContext, string> func = null;// FunctionHandler;
            using(var handlerWrapper = HandlerWrapper.GetHandlerWrapper(func, new JsonSerializer()))
            using(var bootstrap = new LambdaBootstrap(handlerWrapper))
            {
                await bootstrap.RunAsync();
            }
        }

		public bool FunctionHandler(SNSEvent snsEvent, ILambdaContext lambdaContext)
		{
			object arquivoImportadoId = null;

			try
			{
				BeginLogInfo(lambdaContext, arquivoImportadoId, "A solicita��o de processamento da apura��o do banner foi iniciada.");

				if (IsSnsMessageInvalid(snsEvent, out object arquivoId))
				{
					throw new Exception ("");
				}

				var arquivoImportado = arquivoMongoDBRepository.BuscarPor(arquivoId);
				
				var bannersCadastrados = bannersMySqlRepository.ObterBanners(arquivoImportado.DataReferencia);

				var quantidadeLinhas = 5;
				var lotes = arquivoImportado.ObterQuantidadeLotes(quantidadeLinhas);
				var colunas = arquivoMongoDBRepository.ObterColunas(GrupoColunaEnum.Banner);

				for (int lote = 0; lote < lotes; lote++)
				{
					var skip = lote * quantidadeLinhas;

					var arquivoImportadoLinhas = arquivoMongoDBRepository.BuscarArquivoImportadoLinhas(arquivoImportado.Id, skip, quantidadeLinhas);

					foreach (var arquivoImportadoLinha in arquivoImportadoLinhas)
					{
						var clienteApurado = new BannerApuracaoImportada
						{
							Cnpj = arquivoImportadoLinha.Celunas[colunas[ArquivoColunaIndexes.NO_CNPJ].Nome].ToString()
						};

						bannersCadastrados.ForEach(banner =>
						{
							switch (banner.Tipo)
							{
								case BannerEnum.BannerPadrao:
									clienteApurado.BannerApurados.Add(ObterBannerBannerPadraoApurado(arquivoImportadoLinha, colunas, banner));
									break;
								case BannerEnum.BannerComLink:
									clienteApurado.BannerApurados.Add(ObterBannerComLinkApurado(arquivoImportadoLinha, colunas, banner));
									break;
							}
						});

						empresasApurados.Add(clienteApurado);
					}
				}

				SalvarRentabilidadeIndicadorApurados(arquivoId);

				AWSHelper.EnviarSNS(arquivoImportadoId.ToString(), "sns-orquestracao-apuracao");

				EndLogInfo(lambdaContext, arquivoImportadoId, "A solicita��o de processamento da apura��o do banner foi conclu�da com sucesso.");

				return true;
			}
			catch (Exception exception)
			{
				LogError(lambdaContext, arquivoImportadoId, $"Falha na solicita��o de processamento da apura��o do banner - {exception.Message}", exception.StackTrace, exception);

				return false;
			}


		}
		private BannerApurado ObterBannerBannerPadraoApurado(ArquivoImportadoLinha arquivoImportadoLinha, IDictionary<int, ArquivoColuna> colunas, Banner banner)
		{
			arquivoImportadoLinha.Celunas.TryGetValue(banner.Prefixo + "_ATINGIU", out object celula);
			return new BannerPadraoApurado
			{
				IdBanner = Convert.ToInt32(banner.Id),
				Ativo = Convert.ToBoolean(Convert.ToInt16(celula))
			};
		}
		private BannerApurado ObterBannerComLinkApurado(ArquivoImportadoLinha arquivoImportadoLinha, IDictionary<int, ArquivoColuna> colunas, Banner banner)
		{
			arquivoImportadoLinha.Celunas.TryGetValue(banner.Prefixo + "_ATINGIU", out object celulaAtingiu);
			arquivoImportadoLinha.Celunas.TryGetValue(banner.Prefixo + "_LINK", out object celulaLink);
			
			return new BannerComLinkApurado
			{
				IdBanner = Convert.ToInt32(banner.Id),
				Ativo = Convert.ToBoolean(Convert.ToInt16(celulaAtingiu)),
				Link = celulaLink.ToString(),
			};
			
		}
		private bool IsSnsMessageInvalid(SNSEvent snsEvent, out object arquivoId)
		{
			var result = false;

			arquivoId = null;

			var snsMessage = snsEvent.Records?[0].Sns.Message;

			if (string.IsNullOrEmpty(snsMessage))
			{
				result = true;
			}

			if (!ObjectId.TryParse(snsMessage, out ObjectId objectId))
			{
				result = true;
			}

			if (objectId != ObjectId.Empty)
			{
				arquivoId = objectId;
			}

			return result;
		}
		private void SalvarRentabilidadeIndicadorApurados(object arquivoImportadoId)
		{
			bannersMongoDBRepository.Salvar(new BannerRegistro
			{
				ArquivoImportadoId = arquivoImportadoId,
				DataInclusao = DateTime.Now,
				EmpresasApurados = empresasApurados
			});
		}
	}
	
}
