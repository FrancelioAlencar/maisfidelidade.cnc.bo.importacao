﻿using MongoDB.Bson.Serialization.Attributes;
namespace maisfidelidade.cnc.imp.core.Domain.Model.Banners
{
	public class BannerComLinkApurado : BannerApurado
	{
		[BsonElement("id_banner")]
		public int IdBanner { get; set; }
		[BsonElement("ativo")]
		public bool Ativo { get; set; }
		[BsonElement("link")]
		public string Link { get; set; }
	}
}