﻿using maisfidelidade.cnc.imp.core.Infrastructure.DomainBase;
using maisfidelidade.cnc.imp.core.Infrastructure.DomainBase.MongoDB;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace maisfidelidade.cnc.imp.core.Domain.Model.Banners
{
	public class BannerRegistro : MongoEntityBase, IAggregateRoot
	{
		public BannerRegistro()
		{
			EmpresasApurados = new List<BannerApuracaoImportada>();
		}

		[BsonElement("_id_arquivo")]
		public object ArquivoImportadoId { get; set; }

		[BsonElement("dt_inclusao")]
		public DateTime DataInclusao { get; set; }

		[BsonElement("clientes_apurados")]
		public IList<BannerApuracaoImportada> EmpresasApurados { get; set; }
	}
}