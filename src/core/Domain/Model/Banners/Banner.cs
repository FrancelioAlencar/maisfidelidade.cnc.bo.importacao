﻿using maisfidelidade.cnc.imp.core.Infrastructure.DomainBase;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace maisfidelidade.cnc.imp.core.Domain.Model.Banners
{
	public class Banner : EntityBase, IAggregateRoot
		{
			[Column("id_entidade")]
			public int EntidadeId { get; set; }

			[Column("nome")]
			public string Nome { get; set; }

			[Column("prefixo")]
			public string Prefixo { get; set; }

			[Column("id_tipo")]
			public int TipoBanner { get; set; }

			public BannerEnum Tipo => (BannerEnum)Enum.Parse(typeof(BannerEnum), TipoBanner.ToString());
		}
	}
