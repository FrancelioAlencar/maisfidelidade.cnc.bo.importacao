﻿using maisfidelidade.cnc.imp.core.Infrastructure.RepositoryFramework;

namespace maisfidelidade.cnc.imp.core.Domain.Model.Banners
{
	public interface IBannerMongoDBRepository : IRepository<BannerRegistro>
	{
		void Salvar(BannerRegistro ClassificacaoIndicadorRegistro);
	}
}
