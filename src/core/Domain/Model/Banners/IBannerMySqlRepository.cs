﻿using maisfidelidade.cnc.imp.core.Infrastructure.RepositoryFramework;
using System;
using System.Collections.Generic;

namespace maisfidelidade.cnc.imp.core.Domain.Model.Banners
{
	public interface IBannerMySqlRepository : IRepository<Banner>
		{
		IList<Banner> ObterBanners(DateTime? dataReferencia = default);
		}
	}

