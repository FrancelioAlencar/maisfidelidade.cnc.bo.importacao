﻿using maisfidelidade.cnc.imp.core.Infrastructure.DomainBase;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace maisfidelidade.cnc.imp.core.Domain.Model.Banners
{
    public class BannerApuracao : EntityBase
    {
        public Guid Guid { get; set; }

        [Column("id_apuracao")]
        public int ApuracaoId { get; set; }

        [Column("id_banner")]
        public int BannerId { get; set; }

        [Column("atingiu")]
        public bool Atingiu { get; set; }

        [Column("link")]
        public string Link { get; set; }
    }
}
