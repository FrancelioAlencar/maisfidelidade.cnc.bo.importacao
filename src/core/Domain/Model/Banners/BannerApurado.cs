﻿using MongoDB.Bson.Serialization.Attributes;

namespace maisfidelidade.cnc.imp.core.Domain.Model.Banners
{
	[BsonDiscriminator(RootClass = true)]
	[BsonKnownTypes(typeof(BannerComLinkApurado), typeof(BannerPadraoApurado))]
	public class BannerApurado
	{
	}

}
