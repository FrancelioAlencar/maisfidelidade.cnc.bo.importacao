﻿using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace maisfidelidade.cnc.imp.core.Domain.Model.Banners
{
	public class BannerApuracaoImportada
	{
		public BannerApuracaoImportada()
		{
			BannerApurados = new List<BannerApurado>();
		}

		[BsonElement("cnpj")]
		public string Cnpj { get; set; }

		[BsonElement("indicadores_classificacao_apurados")]
		public IList<BannerApurado> BannerApurados { get; set; }
	}
}

