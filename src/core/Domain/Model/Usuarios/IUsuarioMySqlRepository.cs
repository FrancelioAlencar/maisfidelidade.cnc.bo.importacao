﻿using maisfidelidade.cnc.imp.core.Infrastructure.RepositoryFramework;
using System.Collections.Generic;

namespace maisfidelidade.cnc.imp.core.Domain.Model.Usuarios
{
    public interface IUsuarioMySqlRepository : IRepository<Usuario>
    {
        IList<Usuario> ObterUsuariosGC();
        IList<Usuario> ObterUsuariosGR();
    }
}
