﻿namespace maisfidelidade.cnc.imp.core.Domain.Model.Empresas
{
    public class EmpresaKeys
    {
        public int EmpresaId { get; set; }
        public string Cnpj { get; set; }
    }
}
