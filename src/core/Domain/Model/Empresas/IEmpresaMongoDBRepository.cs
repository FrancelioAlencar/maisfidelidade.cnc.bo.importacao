﻿using maisfidelidade.cnc.imp.core.Infrastructure.RepositoryFramework;
using System.Collections.Generic;

namespace maisfidelidade.cnc.imp.core.Domain.Model.Empresas
{
    public interface IEmpresaMongoDBRepository : IRepository<EmpresaRegistro>
    {
        IList<EmpresaImportada> BuscarEmpresasImportadas(object arquivoId);
        void Salvar(EmpresaRegistro empresaRegistro);
    }
}
