﻿using maisfidelidade.cnc.imp.core.Infrastructure.DomainBase;
using maisfidelidade.cnc.imp.core.Infrastructure.DomainBase.MongoDB;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.imp.core.Domain.Model.Empresas
{
    public class EmpresaImportada : MongoEntityBase, IAggregateRoot
    {
        [BsonElement("cnpj")]
        public string Cnpj { get; set; }

        [BsonElement("no_tab")]
        public int NumeroTab { get; set; }

        [BsonElement("nome_fantasia")]
        public string NomeFantasia { get; set; }

        [BsonElement("nome_grupo")]
        public string NomeGrupo { get; set; }

        [BsonElement("id_segmento")]
        public int SegmentoId { get; set; }

        [BsonElement("email")]
        public string Email { get; set; }

        [BsonElement("matricula_gr")]
        public int MatriculaGR { get; set; }

        [BsonElement("matricula_gc")]
        public int MatriculaGC { get; set; }
    }
}
