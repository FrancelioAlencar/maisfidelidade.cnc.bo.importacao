﻿using maisfidelidade.cnc.imp.core.Infrastructure.RepositoryFramework;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.imp.core.Domain.Model.Empresas
{
    public interface IEmpresaMySqlRepository : IRepository<Empresa>
    {
        IList<Empresa> ObterEmpresas();
    }
}
