﻿using maisfidelidade.cnc.imp.core.Infrastructure.DomainBase;
using maisfidelidade.cnc.imp.core.Infrastructure.DomainBase.MongoDB;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace maisfidelidade.cnc.imp.core.Domain.Model.Empresas
{
    public class EmpresaRegistro : MongoEntityBase, IAggregateRoot
    {
        [BsonElement("_id_arquivo")]
        public object ArquivoImportadoId { get; set; }

        [BsonElement("dt_inclusao")]
        public DateTime DataInclusao { get; set; }

        [BsonElement("empresas_importadas")]
        public IList<EmpresaImportada> EmpresasImportadas { get; set; }
    }
}
