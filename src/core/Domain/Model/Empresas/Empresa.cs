﻿using maisfidelidade.cnc.imp.core.Infrastructure.DomainBase;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace maisfidelidade.cnc.imp.core.Domain.Model.Empresas
{
    public class Empresa : EntityBase, IAggregateRoot
    {
        [Column("cnpj")]
        public string Cnpj { get; set; }

        [Column("nome_fantasia")]
        public string NomeFantasia { get; set; }

        [Column("nome_grupo")]
        public string NomeGrupo { get; set; }

        [Column("email")]
        public string Email { get; set; }

        [Column("id_segmento")]
        public int SegmentoId { get; set; }

        [Column("matricula_gr")]
        public int MatriculaGR { get; set; }

        [Column("matricula_gc")]
        public int MatriculaGC { get; set; }
    }
}
