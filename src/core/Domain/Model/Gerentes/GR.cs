﻿using maisfidelidade.cnc.imp.core.Infrastructure.DomainBase;

namespace maisfidelidade.cnc.imp.core.Domain.Model.Gerentes
{
    public class GR : EntityBase, IAggregateRoot
    {
        public int Matricula { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }
    }
}
