﻿using maisfidelidade.cnc.imp.core.Infrastructure.DomainBase.MongoDB;
using MongoDB.Bson.Serialization.Attributes;

namespace maisfidelidade.cnc.imp.core.Domain.Model.Gerentes
{
	public class GRImportado : MongoEntityBase
    {
        public GRImportado()
        {

        }

        [BsonElement("matricula")]
        public int Matricula { get; set; }

        [BsonElement("nome")]
        public string Nome { get; set; }

        [BsonElement("email")]
        public string Email { get; set; }

        [BsonElement("telefone")]
        public string Telefone { get; set; }
    }
}
