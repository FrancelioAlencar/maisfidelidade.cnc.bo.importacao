﻿using maisfidelidade.cnc.imp.core.Infrastructure.DomainBase;
using maisfidelidade.cnc.imp.core.Infrastructure.DomainBase.MongoDB;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace maisfidelidade.cnc.imp.core.Domain.Model.Gerentes
{
    public class GRRegistro : MongoEntityBase, IAggregateRoot
    {
        [BsonElement("_id_arquivo")]
        public object ArquivoImportadoId { get; set; }

        [BsonElement("dt_inclusao")]
        public DateTime DataInclusao { get; set; }

        [BsonElement("gr_importados")]
        public IList<GRImportado> GRImportados { get; set; }
    }
}
