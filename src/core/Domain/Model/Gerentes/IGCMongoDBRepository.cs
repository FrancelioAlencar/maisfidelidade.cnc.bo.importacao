﻿using maisfidelidade.cnc.imp.core.Infrastructure.RepositoryFramework;
using System.Collections.Generic;

namespace maisfidelidade.cnc.imp.core.Domain.Model.Gerentes
{
    public interface IGCMongoDBRepository : IRepository<GCRegistro>
    {
        IList<GCImportado> BuscarGCImportados(object arquivoId);
        void Salvar(GCRegistro gcArquivo);
    }
}
