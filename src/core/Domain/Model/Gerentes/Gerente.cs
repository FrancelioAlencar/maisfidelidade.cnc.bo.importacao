﻿using maisfidelidade.cnc.imp.core.Infrastructure.DomainBase;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace maisfidelidade.cnc.imp.core.Domain.Model.Gerentes
{
	public class Gerente : EntityBase, IAggregateRoot
    {
        [Column("matricula")]
        public int Matricula { get; set; }

        [Column("nome")]
        public string Nome { get; set; }

        [Column("email")]
        public string Email { get; set; }

        [Column("telefone")]
        public string Telefone { get; set; }

        [Column("id_tipo_gerente")]
        public int TipoGerenteId { get; set; }

        public TipoGerenteEnum Tipo => (TipoGerenteEnum)Enum.Parse(typeof(TipoGerenteEnum), TipoGerenteId.ToString());
    }
}
