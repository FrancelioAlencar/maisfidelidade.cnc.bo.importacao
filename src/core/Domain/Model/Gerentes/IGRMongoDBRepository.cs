﻿using maisfidelidade.cnc.imp.core.Infrastructure.RepositoryFramework;
using System.Collections.Generic;

namespace maisfidelidade.cnc.imp.core.Domain.Model.Gerentes
{
	public interface IGRMongoDBRepository : IRepository<GRRegistro>
    {
        IList<GRImportado> BuscarGRImportados(object arquivoId);
        void Salvar(GRRegistro grRegistro);
    }
}
