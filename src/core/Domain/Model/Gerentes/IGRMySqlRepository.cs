﻿using maisfidelidade.cnc.imp.core.Infrastructure.RepositoryFramework;
using System.Collections.Generic;

namespace maisfidelidade.cnc.imp.core.Domain.Model.Gerentes
{
	public interface IGRMySqlRepository : IRepository<GR>
    {
        IList<GR> ObterTodosGRs();
    }
}
