﻿using MongoDB.Bson.Serialization.Attributes;
using System;

namespace maisfidelidade.cnc.imp.core.Domain.Model.Apuracoes
{
    public class ApuracaoImportada
    {
        public ApuracaoImportada()
        {

        }

        [BsonElement("guid")]
        public Guid Guid { get; set; }

        [BsonElement("cnpj")]
        public string Cnpj { get; set; }

        [BsonElement("id_categoria")]
        public int CategoriaId { get; set; }

        [BsonElement("financiamento_santander")]
        public decimal FinanciamentoSantander { get; set; }

        [BsonElement("financiamento_loja")]
        public decimal FinanciamentoLoja { get; set; }

        [BsonElement("qtd_contratos")]
        public int QuantidadeContratos { get; set; }

        [BsonElement("qtd_prestamista")]
        public int QuantidadePrestamista { get; set; }

        [BsonElement("qtd_auto")]
        public int QuantidadeAuto { get; set; }

        [BsonElement("qtd_ambos")]
        public int QuantidadeAmbos { get; set; }

        [BsonElement("plus_mes_anterior")]
        public double PlusMesAnterior { get; set; }

        [BsonElement("plus_mes_vigente")]
        public double PlusMesVigente { get; set; }

        [BsonElement("plus_proximo_mes")]
        public double PlusProximoMes { get; set; }

        [BsonElement("auto_mes_anterior")]
        public double AutoMesAnterior { get; set; }

        [BsonElement("auto_mes_vigente")]
        public double AutoMesVigente { get; set; }

        [BsonElement("auto_proximo_mes")]
        public double AutoProximoMes { get; set; }

        [BsonElement("prestamista_mes_anterior")]
        public double PrestamistaMesAnterior { get; set; }

        [BsonElement("prestamista_mes_vigente")]
        public double PrestamistaMesVigente { get; set; }

        [BsonElement("prestamista_proximo_mes")]
        public double PrestamistaProximoMes { get; set; }
    }
}
