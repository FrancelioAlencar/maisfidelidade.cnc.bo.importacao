﻿using maisfidelidade.cnc.imp.core.Infrastructure.DomainBase;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace maisfidelidade.cnc.imp.core.Domain.Model.Apuracoes
{
    public class Apuracao : EntityBase, IAggregateRoot
    {
        public string Cnpj { get; set; }

        [Column("guid")]
        public Guid Guid { get; set; }

        [Column("id_arquivo")]
        public int ArquivoId { get; set; }

        [Column("id_empresa")]
        public int EmpresaId { get; set; }

        [Column("id_categoria")]
        public int CategoriaId { get; set; }

        [Column("financiado_santander")]
        public decimal FinanciadoSantander { get; set; }

        [Column("financiado_loja")]
        public decimal FinanciadoLoja { get; set; }

        [Column("qtd_contratos")]
        public int QuantidadeContratos { get; set; }

        [Column("qtd_prestamista")]
        public int QuantidadePrestamista { get; set; }

        [Column("qtd_auto")]
        public int QuantidadeAuto { get; set; }

        [Column("qtd_ambos")]
        public int QuantidadeAmbos { get; set; }

        [Column("plus_mes_anterior")]
        public double PlusMesAnterior { get; set; }

        [Column("plus_mes_vigente")]
        public double PlusMesVigente { get; set; }

        [Column("plus_proximo_mes")]
        public double PlusProximoMes { get; set; }

        [Column("auto_mes_anterior")]
        public double AutoMesAnterior { get; set; }

        [Column("auto_mes_vigente")]
        public double AutoMesVigente { get; set; }

        [Column("auto_proximo_mes")]
        public double AutoProximoMes { get; set; }

        [Column("prestamista_mes_anterior")]
        public double PrestamistaMesAnterior { get; set; }

        [Column("prestamista_mes_vigente")]
        public double PrestamistaMesVigente { get; set; }

        [Column("prestamista_proximo_mes")]
        public double PrestamistaProximoMes { get; set; }
    }
}
