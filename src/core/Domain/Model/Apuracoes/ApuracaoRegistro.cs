﻿using maisfidelidade.cnc.imp.core.Infrastructure.DomainBase;
using maisfidelidade.cnc.imp.core.Infrastructure.DomainBase.MongoDB;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.imp.core.Domain.Model.Apuracoes
{
    public class ApuracaoRegistro : MongoEntityBase, IAggregateRoot
    {
        public ApuracaoRegistro()
        {
            Apuracoes = new List<ApuracaoImportada>();
        }

        [BsonElement("_id_arquivo")]
        public object ArquivoImportadoId { get; set; }

        [BsonElement("dt_inclusao")]
        public DateTime DataInclusao { get; set; }

        [BsonElement("apuracoes")]
        public IList<ApuracaoImportada> Apuracoes { get; set; }
    }
}
