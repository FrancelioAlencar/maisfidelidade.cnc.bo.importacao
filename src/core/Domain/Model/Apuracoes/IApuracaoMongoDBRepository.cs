﻿using maisfidelidade.cnc.imp.core.Infrastructure.RepositoryFramework;
using System.Collections.Generic;

namespace maisfidelidade.cnc.imp.core.Domain.Model.Apuracoes
{
    public interface IApuracaoMongoDBRepository : IRepository<ApuracaoRegistro>
    {
        IList<ApuracaoImportada> BuscarApuracoes(object arquivoId);
        void Salvar(ApuracaoRegistro apuracaoRegistro);
    }
}
