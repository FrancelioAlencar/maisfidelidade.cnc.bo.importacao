﻿using System;

namespace maisfidelidade.cnc.imp.core.Domain.Model.Apuracoes
{
    public class ApuracaoKeys
    {
        public int ApuracaoId { get; set; }
        public Guid Guid { get; set; }
    }
}
