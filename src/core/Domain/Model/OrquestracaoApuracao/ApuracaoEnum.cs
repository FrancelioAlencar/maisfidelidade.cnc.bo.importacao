﻿namespace maisfidelidade.cnc.imp.core.Domain.Model.OrquestracaoApuracao
{
	public enum ApuracaoEnum
	{
		Default = 0,
		Banner = 1,
		Campanha = 2,
		IndClassificacao = 3,
		IndPerformance = 4,
		IndRentabilidade = 5
	}
}
