﻿
namespace maisfidelidade.cnc.imp.core.Domain.Model.OrquestracaoApuracao
{
	public class SnsOrquestracaoApuracao
    {
        public string ObjectId { get; set; }
        public ApuracaoEnum EtapaApuracao { get; set; }

        public SnsOrquestracaoApuracao()
        { }

        public SnsOrquestracaoApuracao(string objectId, ApuracaoEnum etapaApuracao)
        {
            ObjectId = objectId;
            EtapaApuracao = etapaApuracao;
        }
    }
}
