﻿using MongoDB.Bson.Serialization.Attributes;

namespace maisfidelidade.cnc.imp.core.Domain.Model.PerformanceIndicadores
{
	public class SeguroAutoPerformanceIndicadorApurado : PerformanceIndicadorApurado
    {
        [BsonElement("porcentagem")]
        public double Porcentagem { get; set; }

        [BsonElement("contratos_elegiveis")]
        public int ContratosElegiveis { get; set; }

        [BsonElement("contratos_convertidos")]
        public int ContratosConvertidos { get; set; }
    }
}
