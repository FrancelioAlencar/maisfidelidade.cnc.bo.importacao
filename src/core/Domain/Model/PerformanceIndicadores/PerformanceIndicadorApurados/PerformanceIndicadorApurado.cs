﻿using MongoDB.Bson.Serialization.Attributes;

namespace maisfidelidade.cnc.imp.core.Domain.Model.PerformanceIndicadores
{
    [BsonDiscriminator(RootClass = true)]
    [BsonKnownTypes(typeof(AmbosPerformanceIndicadorApurado), typeof(SeguroAutoPerformanceIndicadorApurado), typeof(SeguroPrestamistaPerformanceIndicadorApurado))]
    public class PerformanceIndicadorApurado
    {
        [BsonElement("id_performance_indicador")]
        public int PerformanceIndicadorId { get; set; }
    }
}
