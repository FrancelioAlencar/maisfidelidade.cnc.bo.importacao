﻿using MongoDB.Bson.Serialization.Attributes;

namespace maisfidelidade.cnc.imp.core.Domain.Model.PerformanceIndicadores
{
	public class AmbosPerformanceIndicadorApurado : PerformanceIndicadorApurado
    {
        public AmbosPerformanceIndicadorApurado()
            : base()
        {

        }


        [BsonElement("qtd_contratos")]
        public decimal QuantidadeContratos { get; set; }
    }
}
