﻿using maisfidelidade.cnc.imp.core.Common.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace maisfidelidade.cnc.imp.core.Domain.Model.PerformanceIndicadores
{
    public class PerformanceIndicadorDadoPrimaryKeys
    {
        public PerformanceIndicadorDadoPrimaryKeys()
        {
            try
            {
                var performanceIndicadorMySqlRepository = DependencyResolver.GetService<IPerformanceIndicadorMySqlRepository>();

                var performanceIndicadorDados = performanceIndicadorMySqlRepository.ObterPerformanceIndicadorDados();

                AmbosQuantidadeContratosId = Convert.ToInt32(performanceIndicadorDados.Single(d => d.Tipo == PerformanceIndicadorDadoEnum.AmbosQuantidadeContratos).Id);
                SeguroAutoContratosElegiveisId = Convert.ToInt32(performanceIndicadorDados.Single(d => d.Tipo == PerformanceIndicadorDadoEnum.SeguroAutoContratosElegiveis).Id);
                SeguroAutoContratosConvertidosId = Convert.ToInt32(performanceIndicadorDados.Single(d => d.Tipo == PerformanceIndicadorDadoEnum.SeguroAutoContratosConvertidos).Id);
                SeguroPrestamistaContratosElegiveisId = Convert.ToInt32(performanceIndicadorDados.Single(d => d.Tipo == PerformanceIndicadorDadoEnum.SeguroPrestamistaContratosElegiveis).Id);
                SeguroPrestamistaContratosConvertidosId = Convert.ToInt32(performanceIndicadorDados.Single(d => d.Tipo == PerformanceIndicadorDadoEnum.SeguroPrestamistaContratosConvertidos).Id);
            }
            catch (Exception exception)
            {
                throw new Exception("some reason to rethrow", exception);
            }
        }

        public int AmbosQuantidadeContratosId { get; private set; }
        public int SeguroAutoContratosElegiveisId { get; private set; }
        public int SeguroAutoContratosConvertidosId { get; private set; }
        public int SeguroPrestamistaContratosElegiveisId { get; private set; }
        public int SeguroPrestamistaContratosConvertidosId { get; private set; }
    }
}
