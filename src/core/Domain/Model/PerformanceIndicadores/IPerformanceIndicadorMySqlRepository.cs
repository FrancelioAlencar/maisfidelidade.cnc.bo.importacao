﻿using maisfidelidade.cnc.imp.core.Infrastructure.RepositoryFramework;
using System.Collections.Generic;

namespace maisfidelidade.cnc.imp.core.Domain.Model.PerformanceIndicadores
{
	public interface IPerformanceIndicadorMySqlRepository : IRepository<PerformanceIndicador>
    {
        IList<PerformanceIndicador> ObterPerformanceIndicadores();
        IList<PerformanceIndicadorDado> ObterPerformanceIndicadorDados();
    }
}
