﻿using System;

namespace maisfidelidade.cnc.imp.core.Domain.Model.PerformanceIndicadores
{
    public class PerformanceIndicadorApuracaoKeys
    {
        public int PerformanceIndicadorApuracaoId { get; set; }
        public int PerformanceIndicadorId { get; set; }
        public Guid ApuracaoGuid { get; set; }
    }
}
