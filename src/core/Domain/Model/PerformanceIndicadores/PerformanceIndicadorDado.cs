﻿using maisfidelidade.cnc.imp.core.Infrastructure.DomainBase;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace maisfidelidade.cnc.imp.core.Domain.Model.PerformanceIndicadores
{
    public class PerformanceIndicadorDado : EntityBase
    {
        [Column("id_entidade")]
        public int EntidadeId { get; set; }

        [Column("id_indicador_performance")]
        public int PerformanceIndicadorId { get; set; }

        [Column("nome")]
        public string Nome { get; set; }

        [Column("descricao")]
        public string Descricao { get; set; }

        [Column("tooltip")]
        public string Tooltip { get; set; }

        public PerformanceIndicadorDadoEnum Tipo => (PerformanceIndicadorDadoEnum)Enum.Parse(typeof(PerformanceIndicadorDadoEnum), EntidadeId.ToString());
    }
}
