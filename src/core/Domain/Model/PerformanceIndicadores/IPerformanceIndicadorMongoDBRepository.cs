﻿using maisfidelidade.cnc.imp.core.Infrastructure.RepositoryFramework;
using System.Collections.Generic;

namespace maisfidelidade.cnc.imp.core.Domain.Model.PerformanceIndicadores
{
	public interface IPerformanceIndicadorMongoDBRepository : IRepository<PerformanceIndicadorRegistro>
    {
        IList<PerformanceIndicadorApuracaoImportado> BuscarPerformanceIndicadorApuracoes(object arquivoImportadoId);
        void Salvar(PerformanceIndicadorRegistro performanceIndicadorRegistro);
    }
}
