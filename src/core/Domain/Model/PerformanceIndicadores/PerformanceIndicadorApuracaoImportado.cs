﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace maisfidelidade.cnc.imp.core.Domain.Model.PerformanceIndicadores
{
	public class PerformanceIndicadorApuracaoImportado
    {
        public PerformanceIndicadorApuracaoImportado()
        {
            PerformanceIndicadorApurados = new List<PerformanceIndicadorApurado>();
        }

        [BsonElement("guid")]
        public Guid Guid { get; set; }

        [BsonElement("cnpj")]
        public string Cnpj { get; set; }

        [BsonElement("performance_indicador_apurados")]
        public IList<PerformanceIndicadorApurado> PerformanceIndicadorApurados { get; set; }
    }
}
