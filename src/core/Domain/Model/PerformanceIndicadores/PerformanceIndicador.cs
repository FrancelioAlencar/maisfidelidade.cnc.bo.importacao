﻿using maisfidelidade.cnc.imp.core.Infrastructure.DomainBase;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace maisfidelidade.cnc.imp.core.Domain.Model.PerformanceIndicadores
{
	public class PerformanceIndicador : EntityBase, IAggregateRoot
    {
        [Column("id_entidade")]
        public int EntidadeId { get; set; }

        [Column("id_tipo_indicador_performance")]
        public int TipoPerformanceIndicadorId { get; set; }

        [Column("nome")]
        public string Nome { get; set; }

        [Column("descricao")]
        public string Descricao { get; set; }

        [Column("tooltip")]
        public string Tooltip { get; set; }

        public PerformanceIndicadorEnum Tipo => (PerformanceIndicadorEnum)Enum.Parse(typeof(PerformanceIndicadorEnum), EntidadeId.ToString());
    }
}
