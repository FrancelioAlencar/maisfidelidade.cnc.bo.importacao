﻿using maisfidelidade.cnc.imp.core.Infrastructure.DomainBase;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace maisfidelidade.cnc.imp.core.Domain.Model.PerformanceIndicadores
{
    public class PerformanceIndicadorDadoApuracao : EntityBase
    {
        public Guid ApuracaoGuid { get; set; }

        public int PerformanceIndicadorId { get; set; }

        [Column("id_apuracao_indicador_performance")]
        public int PerformanceIndicadorApuracaoId { get; set; }

        [Column("id_indicador_performance_dado")]
        public int PerformanceIndicadorDadoId { get; set; }

        [Column("valor")]
        public string Valor { get; set; }
    }
}
