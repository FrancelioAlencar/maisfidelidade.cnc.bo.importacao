﻿using maisfidelidade.cnc.imp.core.Infrastructure.DomainBase;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace maisfidelidade.cnc.imp.core.Domain.Model.PerformanceIndicadores
{
    public class PerformanceIndicadorApuracao : EntityBase
    {
        public Guid ParentGuid { get; set; }

        public PerformanceIndicadorEnum Tipo { get; set; }

        [Column("id_apuracao")]
        public int ApuracaoId { get; set; }

        [Column("id_indicador_performance")]
        public int PerformanceIndicadorId { get; set; }

        [Column("valor")]
        public string Valor { get; set; }
    }
}
