﻿using maisfidelidade.cnc.imp.core.Infrastructure.DomainBase;
using maisfidelidade.cnc.imp.core.Infrastructure.DomainBase.MongoDB;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace maisfidelidade.cnc.imp.core.Domain.Model.PerformanceIndicadores
{
	public class PerformanceIndicadorRegistro : MongoEntityBase, IAggregateRoot
    {
        public PerformanceIndicadorRegistro()
        {
            PerformanceIndicadorApuracoes = new List<PerformanceIndicadorApuracaoImportado>();
        }

        [BsonElement("_id_arquivo")]
        public object ArquivoImportadoId { get; set; }

        [BsonElement("dt_inclusao")]
        public DateTime DataInclusao { get; set; }

        [BsonElement("performance_indicador_apuracoes")]
        public IList<PerformanceIndicadorApuracaoImportado> PerformanceIndicadorApuracoes { get; set; }
    }
}
