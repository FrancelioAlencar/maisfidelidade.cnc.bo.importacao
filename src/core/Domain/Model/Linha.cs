﻿namespace maisfidelidade.cnc.imp.core.Domain.Model
{
    public class Linha<T>
    {
        public string Cnpj { get; set; }
        public T Entity { get; set; }
    }
}
