﻿using MongoDB.Bson.Serialization.Attributes;

namespace maisfidelidade.cnc.imp.core.Domain.Model.RentabilidadeIndicadores
{
	public class RentabilidadeBeneficioApuradoCRV : RentabilidadeBeneficioApurado
	{
		[BsonElement("possui_crv")]
		public bool PossuiCRV { get; set; }
	}
}
