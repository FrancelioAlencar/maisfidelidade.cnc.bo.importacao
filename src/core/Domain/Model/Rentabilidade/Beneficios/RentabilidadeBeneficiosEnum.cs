﻿namespace maisfidelidade.cnc.imp.core.Domain.Model.RentabilidadeIndicadores
{
	public enum RentabilidadeBeneficiosEnum
	{
		Gravame = 1,
		Crv = 2,
		MesaCredito = 3
	}
}
