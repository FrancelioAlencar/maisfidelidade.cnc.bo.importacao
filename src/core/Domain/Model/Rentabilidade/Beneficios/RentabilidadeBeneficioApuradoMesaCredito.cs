﻿using MongoDB.Bson.Serialization.Attributes;

namespace maisfidelidade.cnc.imp.core.Domain.Model.RentabilidadeIndicadores
{
	public class RentabilidadeBeneficioApuradoMesaCredito : RentabilidadeBeneficioApurado
	{
		[BsonElement("possui_mesa_credito")]
		public bool PossuiMesaCredito { get; set; }
	}
}
