﻿using MongoDB.Bson.Serialization.Attributes;

namespace maisfidelidade.cnc.imp.core.Domain.Model.RentabilidadeIndicadores
{
	public class RentabilidadeBeneficioApuradoGravame : RentabilidadeBeneficioApurado
	{
		[BsonElement("qtd_gravame")]
		public int QuantidadeGravame { get; set; }
	}
}
