﻿using MongoDB.Bson.Serialization.Attributes;

namespace maisfidelidade.cnc.imp.core.Domain.Model.RentabilidadeIndicadores
{
	[BsonDiscriminator(RootClass = true)]
	[BsonKnownTypes(typeof(RentabilidadeBeneficioApuradoCRV), typeof(RentabilidadeBeneficioApuradoGravame), typeof(RentabilidadeBeneficioApuradoMesaCredito))]
	public class RentabilidadeBeneficioApurado
	{
		[BsonElement("id_rentabilidade_beneficio")]
		public int RentabilidadeBeneficioId { get; set; }
	}
}
