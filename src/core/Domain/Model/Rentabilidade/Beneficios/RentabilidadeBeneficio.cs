﻿using maisfidelidade.cnc.imp.core.Infrastructure.DomainBase;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace maisfidelidade.cnc.imp.core.Domain.Model.RentabilidadeIndicadores
{
	public class RentabilidadeBeneficio: EntityBase, IAggregateRoot
	{
		[Column("id_entidade")]
		public int EntidadeId { get; set; }

		[Column("nome")]
		public string Nome { get; set; }

		public RentabilidadeBeneficiosEnum Tipo => (RentabilidadeBeneficiosEnum)Enum.Parse(typeof(RentabilidadeBeneficiosEnum), EntidadeId.ToString());
	}
}