﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace maisfidelidade.cnc.imp.core.Domain.Model.RentabilidadeIndicadores
{
    public class RentabilidadeBeneficioApuracao
    {
        public Guid Guid { get; set; }

        [Column("id_apuracao")]
        public int ApuracaoId { get; set; }

        [Column("id_beneficio_rentabilidade")]
        public int RentabilidadeBeneficioId { get; set; }

        [Column("valor")]
        public string Valor { get; set; }
     }
}
