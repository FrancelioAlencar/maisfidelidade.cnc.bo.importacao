﻿namespace maisfidelidade.cnc.imp.core.Domain.Model.RentabilidadeIndicadores
{
	public enum RentabilidadeIndicadorEnum
	{
		Consolidado = 0,
		Marketing = 1,
		Financiamentos = 2,
		Seguros = 3,
		Plus = 4,
		Retorno = 5,
		Prestamista = 6,
		Auto = 7
	}
}