﻿using MongoDB.Bson.Serialization.Attributes;

namespace maisfidelidade.cnc.imp.core.Domain.Model.RentabilidadeIndicadores
{
	[BsonDiscriminator(RootClass = true)]
	[BsonKnownTypes(typeof(RentabilidadeIndicadorApuradoPlus), typeof(RentabilidadeIndicadorApuradoRetorno), typeof(RentabilidadeIndicadorApuradoAuto),
		typeof(RentabilidadeIndicadorApuradoPrestamista), typeof(RentabilidadeIndicadorApuradoMarketing), typeof(RentabilidadeIndicadorApuradoFinanciamentos),
		typeof(RentabilidadeIndicadorApuradoSeguros), typeof(RentabilidadeIndicadorApuradoConsolidado))]
	public class RentabilidadeIndicadorApurado
	{
		[BsonElement("id_rentabilidade_indicador")]
		public int RentabilidadeIndicadorId { get; set; }
	}
}