﻿using MongoDB.Bson.Serialization.Attributes;

namespace maisfidelidade.cnc.imp.core.Domain.Model.RentabilidadeIndicadores
{
	public class RentabilidadeIndicadorApuradoPlus : RentabilidadeIndicadorApurado
	{
		[BsonElement("maximo")]
		public double Maximo { get; set; }

		[BsonElement("realizado")]
		public double Realizado { get; set; }
	}
}
