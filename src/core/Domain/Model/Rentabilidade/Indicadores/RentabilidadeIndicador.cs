﻿using maisfidelidade.cnc.imp.core.Infrastructure.DomainBase;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace maisfidelidade.cnc.imp.core.Domain.Model.RentabilidadeIndicadores
{
	public class RentabilidadeIndicador : EntityBase, IAggregateRoot
	{
		[Column("id_entidade")]
		public int EntidadeId { get; set; }

		[Column("id_indicador_rentabilidade_pai")]
		public int IdIndicadorRentabilidadePai { get; set; }

		[Column("nome")]
		public string Nome { get; set; }

		public RentabilidadeIndicadorEnum Tipo => (RentabilidadeIndicadorEnum)Enum.Parse(typeof(RentabilidadeIndicadorEnum), EntidadeId.ToString());
	}
}
