﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.imp.core.Domain.Model.RentabilidadeIndicadores
{
	public class RentabilidadeIndicadorApuradoAuto : RentabilidadeIndicadorApurado
	{		
		[BsonElement("maximo")]
		public double Maximo { get; set; }
		
		[BsonElement("realizado")]
		public double Realizado { get; set; }
	}
}
