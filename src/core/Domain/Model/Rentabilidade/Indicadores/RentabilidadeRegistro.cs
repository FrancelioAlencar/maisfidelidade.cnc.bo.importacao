﻿using maisfidelidade.cnc.imp.core.Domain.Model.RentabilidadeIndicadores;
using maisfidelidade.cnc.imp.core.Infrastructure.DomainBase;
using maisfidelidade.cnc.imp.core.Infrastructure.DomainBase.MongoDB;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;
using System;

namespace maisfidelidade.cnc.imp.core.Domain.Model.RentabilidadeIndicadores
{
	public class RentabilidadeIndicadorRegistro : MongoEntityBase, IAggregateRoot
	{
		public RentabilidadeIndicadorRegistro()
		{
			RentabilidadeApuracoes = new List<RentabilidadeApuracaoImportada>();
		}

		[BsonElement("_id_arquivo")]
		public object ArquivoImportadoId { get; set; }

		[BsonElement("dt_inclusao")]
		public DateTime DataInclusao { get; set; }

		[BsonElement("rentabilidade_apuracoes")]
		public IList<RentabilidadeApuracaoImportada> RentabilidadeApuracoes { get; set; }
	}
}

