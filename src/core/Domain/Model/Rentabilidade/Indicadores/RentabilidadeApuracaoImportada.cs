﻿using maisfidelidade.cnc.imp.core.Domain.Model.RentabilidadeIndicadores;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace maisfidelidade.cnc.imp.core.Domain.Model.RentabilidadeIndicadores
{
	public class RentabilidadeApuracaoImportada
	{
		public RentabilidadeApuracaoImportada()
		{
			RentabilidadeIndicadorApurados = new List<RentabilidadeIndicadorApurado>();
			RentabilidadeBeneficioApurados = new List<RentabilidadeBeneficioApurado>();
		}

		[BsonElement("guid")]
		public Guid Guid { get; set; }

		[BsonElement("cnpj")]
		public string Cnpj { get; set; }

		[BsonElement("rentabilidade_indicador_apurados")]
		public IList<RentabilidadeIndicadorApurado> RentabilidadeIndicadorApurados { get; set; }

		[BsonElement("rentabilidade_beneficio_apurados")]
		public IList<RentabilidadeBeneficioApurado> RentabilidadeBeneficioApurados { get; set; }
	}
}
