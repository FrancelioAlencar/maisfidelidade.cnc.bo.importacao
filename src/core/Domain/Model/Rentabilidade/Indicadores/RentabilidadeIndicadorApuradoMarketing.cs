﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.imp.core.Domain.Model.RentabilidadeIndicadores
{
	public class RentabilidadeIndicadorApuradoMarketing : RentabilidadeIndicadorApurado
	{
		[BsonElement("ativo")]
		public bool Ativo { get; set; }
	}
}
