﻿using maisfidelidade.cnc.imp.core.Domain.Model.RentabilidadeIndicadores;
using maisfidelidade.cnc.imp.core.Infrastructure.RepositoryFramework;
using System.Collections.Generic;

namespace maisfidelidade.cnc.imp.core.Domain.Model.RentabilidadeIndicadores
{
	public interface IRentabilidadeIndicadorMySqlRepository : IRepository<RentabilidadeIndicador>
	{
		IList<RentabilidadeIndicador> ObterRentabilidadeIndicadores();
		IList<RentabilidadeBeneficio> ObterRentabilidadeBeneficios();
	}
}