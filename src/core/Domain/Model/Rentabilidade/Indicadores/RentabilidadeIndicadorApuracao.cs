﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace maisfidelidade.cnc.imp.core.Domain.Model.RentabilidadeIndicadores
{
    public class RentabilidadeIndicadorApuracao
    {
        public Guid Guid { get; set; }

        [Column("id_apuracao")]
        public int ApuracaoId { get; set; }

        [Column("id_indicador_rentabilidade")]
        public int RentabilidadeIndicadorId { get; set; }

        [Column("maximo")]
        public string Maximo { get; set; }

        [Column("realizado")]
        public string Realizado { get; set; }
    }
}
