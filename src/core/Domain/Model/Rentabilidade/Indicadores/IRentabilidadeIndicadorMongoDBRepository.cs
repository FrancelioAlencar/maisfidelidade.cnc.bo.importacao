﻿using maisfidelidade.cnc.imp.core.Infrastructure.RepositoryFramework;
using System.Collections.Generic;

namespace maisfidelidade.cnc.imp.core.Domain.Model.RentabilidadeIndicadores
{
	public interface IRentabilidadeIndicadorMongoDBRepository : IRepository<RentabilidadeIndicadorRegistro>
	{
		IList<RentabilidadeApuracaoImportada> BuscarRentabilidadeApuracoes(object arquivoImportadoId);

		void Salvar(RentabilidadeIndicadorRegistro rentabilidadeIndicadorRegistro);
	}
}