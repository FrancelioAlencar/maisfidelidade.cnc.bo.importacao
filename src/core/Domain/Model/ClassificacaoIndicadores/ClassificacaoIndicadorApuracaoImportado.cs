﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace maisfidelidade.cnc.imp.core.Domain.Model.ClassificacaoIndicadores
{
	public class ClassificacaoIndicadorApuracaoImportado
	{
		public ClassificacaoIndicadorApuracaoImportado()
		{
			ClassificacaoIndicadorApurados = new List<ClassificacaoIndicadorApurado>();
		}

		[BsonElement("guid")]
		public Guid Guid { get; set; }

		[BsonElement("cnpj")]
		public string Cnpj { get; set; }

		[BsonElement("classificacao_indicador_apurados")]
		public IList<ClassificacaoIndicadorApurado> ClassificacaoIndicadorApurados { get; set; }
	}
}

