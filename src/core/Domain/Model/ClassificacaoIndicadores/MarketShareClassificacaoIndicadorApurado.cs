﻿using MongoDB.Bson.Serialization.Attributes;

namespace maisfidelidade.cnc.imp.core.Domain.Model.ClassificacaoIndicadores
{
	public class MarketShareClassificacaoIndicadorApurado : ClassificacaoIndicadorApurado
	{
		[BsonElement("market_share_pc")]
		public int Porcentagem { get; set; }
	}
}