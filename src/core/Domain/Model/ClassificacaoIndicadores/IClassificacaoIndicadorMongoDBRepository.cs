﻿using maisfidelidade.cnc.imp.core.Infrastructure.RepositoryFramework;
using System.Collections.Generic;

namespace maisfidelidade.cnc.imp.core.Domain.Model.ClassificacaoIndicadores
{
	public interface IClassificacaoIndicadorMongoDBRepository : IRepository<ClassificacaoIndicadorRegistro>
	{
		IList<ClassificacaoIndicadorApuracaoImportado> BuscarClassificacaoIndicadorApuracoes(object arquivoImportadoId);

		void Salvar(ClassificacaoIndicadorRegistro ClassificacaoIndicadorRegistro);
	}
}
