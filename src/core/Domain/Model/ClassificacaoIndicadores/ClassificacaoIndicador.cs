﻿using maisfidelidade.cnc.imp.core.Infrastructure.DomainBase;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace maisfidelidade.cnc.imp.core.Domain.Model.ClassificacaoIndicadores
{
	public class ClassificacaoIndicador : EntityBase, IAggregateRoot
		{
			[Column("id_entidade")]
			public int EntidadeId { get; set; }

			[Column("nome")]
			public string Nome { get; set; }

			[Column("descricao")]
			public string Descricao { get; set; }

			[Column("tooltip")]
			public string Tooltip { get; set; }

			public ClassificacaoIndicadorEnum Tipo => (ClassificacaoIndicadorEnum)Enum.Parse(typeof(ClassificacaoIndicadorEnum), EntidadeId.ToString());
		}
	}
