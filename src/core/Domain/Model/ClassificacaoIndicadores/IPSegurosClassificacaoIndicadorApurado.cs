﻿using MongoDB.Bson.Serialization.Attributes;

namespace maisfidelidade.cnc.imp.core.Domain.Model.ClassificacaoIndicadores
{
	public class IPSegurosClassificacaoIndicadorApurado : ClassificacaoIndicadorApurado
	{
		[BsonElement("ipseguro_pc")]
		public int Porcentagem { get; set; }
	}
}
