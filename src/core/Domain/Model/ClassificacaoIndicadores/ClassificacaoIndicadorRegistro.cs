﻿using maisfidelidade.cnc.imp.core.Infrastructure.DomainBase;
using maisfidelidade.cnc.imp.core.Infrastructure.DomainBase.MongoDB;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace maisfidelidade.cnc.imp.core.Domain.Model.ClassificacaoIndicadores
{
	public class ClassificacaoIndicadorRegistro : MongoEntityBase, IAggregateRoot
	{
		public ClassificacaoIndicadorRegistro()
		{
			ClassificacaoIndicadorApuracoes = new List<ClassificacaoIndicadorApuracaoImportado>();
		}

		[BsonElement("_id_arquivo")]
		public object ArquivoImportadoId { get; set; }

		[BsonElement("dt_inclusao")]
		public DateTime DataInclusao { get; set; }

		[BsonElement("classificacao_indicador_apuracoes")]
		public IList<ClassificacaoIndicadorApuracaoImportado> ClassificacaoIndicadorApuracoes { get; set; }
	}
}