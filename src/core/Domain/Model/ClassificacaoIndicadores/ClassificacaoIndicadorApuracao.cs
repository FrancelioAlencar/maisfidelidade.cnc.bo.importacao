﻿using maisfidelidade.cnc.imp.core.Infrastructure.DomainBase;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace maisfidelidade.cnc.imp.core.Domain.Model.ClassificacaoIndicadores
{
    public class ClassificacaoIndicadorApuracao : EntityBase
    {
        public Guid Guid { get; set; }

        [Column("id_apuracao")]
        public int ApuracaoId { get; set; }

        [Column("id_indicador_classificacao")]
        public int ClassificacaoIndicadorId { get; set; }

        [Column("valor")]
        public string Valor { get; set; }
    }
}
