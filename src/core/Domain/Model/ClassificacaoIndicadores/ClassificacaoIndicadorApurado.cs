﻿using MongoDB.Bson.Serialization.Attributes;

namespace maisfidelidade.cnc.imp.core.Domain.Model.ClassificacaoIndicadores
{
	[BsonDiscriminator(RootClass = true)]
	[BsonKnownTypes(typeof(IPSegurosClassificacaoIndicadorApurado), typeof(MarketShareClassificacaoIndicadorApurado))]
	public class ClassificacaoIndicadorApurado
	{
		[BsonElement("id_classificacao_indicador")]
		public int ClassificacaoIndicadorId { get; set; }
	}
}
