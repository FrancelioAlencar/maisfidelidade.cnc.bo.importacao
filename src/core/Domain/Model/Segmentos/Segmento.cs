﻿using maisfidelidade.cnc.imp.core.Infrastructure.DomainBase;
using System.ComponentModel.DataAnnotations.Schema;

namespace maisfidelidade.cnc.imp.core.Domain.Model.Segmentos
{
	public class Segmento : EntityBase, IAggregateRoot
    {
        [Column("nome")]
        public string Nome { get; set; }

        [Column("codigo_santander")]
        public string CodigoSantander { get; set; }
    }
}
