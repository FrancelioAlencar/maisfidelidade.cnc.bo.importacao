﻿using maisfidelidade.cnc.imp.core.Infrastructure.RepositoryFramework;
using System.Collections.Generic;

namespace maisfidelidade.cnc.imp.core.Domain.Model.Segmentos
{
    public interface ISegmentoMySqlRepository : IRepository<Segmento>
    {
        IList<Segmento> ObterSegmentos();
    }
}
