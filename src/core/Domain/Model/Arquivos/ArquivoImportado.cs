﻿using maisfidelidade.cnc.imp.core.Domain.Model.Enums;
using maisfidelidade.cnc.imp.core.Infrastructure.DomainBase;
using maisfidelidade.cnc.imp.core.Infrastructure.DomainBase.MongoDB;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace maisfidelidade.cnc.imp.core.Domain.Model.Arquivos
{
    public class ArquivoImportado : MongoEntityBase, IAggregateRoot
    {
        [BsonElement("nome")]
        public string Nome { get; set; }

        [BsonElement("tipo")]
        public TipoArquivoEnum? TipoArquivo { get; set; }

        [BsonDateTimeOptions(DateOnly = true, Kind = DateTimeKind.Local)]
        [BsonElement("dt_processamento")]
        public DateTime? DataProcessamento { get; set; }

        [BsonDateTimeOptions(DateOnly = true, Kind = DateTimeKind.Local)]
        [BsonElement("dt_referencia")]
        public DateTime? DataReferencia { get; set; }

        [BsonDateTimeOptions(DateOnly = true, Kind = DateTimeKind.Local)]
        [BsonElement("dt_previsao_atualizacao")]
        public DateTime? DataPrevisaoAtualizacao { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        [BsonElement("dt_importacao")]
        public DateTime DataImportacao { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        [BsonElement("dt_publicacao")]
        public DateTime? DataPublicacao { get; set; }

        [BsonElement("total_linhas_processadas")]
        public int TotalLinhasProcessadas { get; set; }

        [BsonElement("status_arquivo")]
        public StatusArquivoEnum StatusArquivo { get; set; }

        [BsonElement("status_andamento_processo")]
        public StatusAndamentoProcessoEnum StatusAndamentoProcesso { get; set; }

        [BsonElement("etapa_empresa_concluida")]
        public bool EtapaEmpresaConcluida { get; set; }

        [BsonElement("etapa_gerente_concluida")]
        public bool EtapaGerenteConcluida { get; set; }

        [BsonElement("etapa_banner_concluida")]
        public bool EtapaBannerConcluida { get; set; }

        [BsonElement("etapa_campanha_concluida")]
        public bool EtapaCampanhaConcluida { get; set; }

        [BsonElement("etapa_indicador_classificacao_concluida")]
        public bool EtapaClassificacaoConcluida { get; set; }

        [BsonElement("etapa_indicador_performance_concluida")]
        public bool EtapaPerformanceConcluida { get; set; }

        [BsonElement("etapa_indicador_rentabilidade_concluida")]
        public bool EtapaRentabilidadeConcluida { get; set; }

        [BsonElement("dt_inclusao")]
        public DateTime DataInclusao { get; set; }

        #region Métodos Públicos

        public int ObterQuantidadeLotes(int quantidadeLinhasPorLote)
        {
            return TotalLinhasProcessadas / quantidadeLinhasPorLote;
        }

        #endregion
    }
}
