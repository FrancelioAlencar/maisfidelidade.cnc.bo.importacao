﻿namespace maisfidelidade.cnc.imp.core.Domain.Model.Arquivos
{
	public class ArquivoImportadoCelula
    {
        public string Coluna { get; set; }
        public object Valor { get; set; }
    }
}
