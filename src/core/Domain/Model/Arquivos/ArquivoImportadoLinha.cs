﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace maisfidelidade.cnc.imp.core.Domain.Model.Arquivos
{
    public class ArquivoImportadoLinha
    {
        public ArquivoImportadoLinha()
        {
            Celunas = new Dictionary<string, object>();
        }

        [BsonElement("celulas")]
        public Dictionary<string, object> Celunas { get; set; }

        public object this[string coluna]
        {
            get { return FindEntry(coluna); }
        }

        public void AdicionarCelula(string coluna, object valor)
        {
            this.Insert(coluna, valor);
        }

        private void Insert(string coluna, object valor)
        {
            if (string.IsNullOrWhiteSpace(coluna))
            {
                throw new ArgumentException("O nome da coluna está nula, vazia ou composta por espaços em branco.", nameof(coluna));
            }

            if (Celunas.ContainsKey(coluna))
            {
                throw new ArgumentException("Uma coluna com o mesmo nome já foi adicionada.", nameof(coluna));
            }

            Celunas.Add(coluna, valor);
        }

        private object FindEntry(string coluna)
        {
            if (string.IsNullOrWhiteSpace(coluna))
            {
                throw new ArgumentException("O nome da coluna está nula, vazia ou composta por espaços em branco.", nameof(coluna));
            }

            if (!Celunas.ContainsKey(coluna))
            {
                throw new KeyNotFoundException("O nome da coluna não foi encontrado.");
            }

            return Celunas[coluna];
        }

        //[BsonElement("celunas")]
        //public IList<ArquivoImportadoCelula> Celunas { get; set; }
    }
}
