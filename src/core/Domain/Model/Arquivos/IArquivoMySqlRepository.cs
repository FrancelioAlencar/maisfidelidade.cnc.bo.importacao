﻿using maisfidelidade.cnc.imp.core.Infrastructure.RepositoryFramework;

namespace maisfidelidade.cnc.imp.core.Domain.Model.Arquivos
{
    public interface IArquivoMySqlRepository : IRepository<ArquivoPublicado>
    {
        bool ExisteArquivo(string arquivoNome);
    }
}
