﻿using maisfidelidade.cnc.imp.core.Domain.Model.Enums;

namespace maisfidelidade.cnc.imp.core.Domain.Model.Arquivos
{
    public class ArquivoColunaBooleano : ArquivoColuna
    {
        public ArquivoColunaBooleano()
            : base(TipoColunaEnum.Booleano)
        {

        }

        protected override bool ValidarValor(string valorString, out string erroMensagem)
        {
            bool validado;

            if (PreenchimentoObrigatorio)
            {
                validado = sbyte.TryParse(valorString, out sbyte valorResultado);

                validado = validado && (valorResultado == 0 || valorResultado == 1);
            }
            else
            {
                validado = true;
            }

            erroMensagem = validado ? string.Empty : "O campo deve ser um valor booleano.";

            return validado;
        }
    }
}
