﻿using maisfidelidade.cnc.imp.core.Domain.Model.Enums;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace maisfidelidade.cnc.imp.core.Domain.Model.Arquivos
{
    public class ArquivoColunaCNPJ : ArquivoColuna
    {
        private readonly IList<string> CNPJsInvalidos;

        public ArquivoColunaCNPJ()
            : base(TipoColunaEnum.CNPJ)
        {
            CNPJsInvalidos = new List<string>
            {
                "00000000000000", "11111111111111", "22222222222222", "33333333333333", "44444444444444",
                "55555555555555", "66666666666666", "77777777777777", "88888888888888", "99999999999999"
            };
        }

        protected override bool ValidarValor(string valorString, out string erroMensagem)
        {
            bool validado;

            if (PreenchimentoObrigatorio)
            {
                validado = valorString.Length == 14;

                validado = validado && !CNPJsInvalidos.Any(cnpj => cnpj.ToUpper().Equals(valorString.ToUpper()))
                    && Regex.IsMatch(valorString, @"[0-9]{2}?[0-9]{3}?[0-9]{3}?[0-9]{4}?[0-9]{2}");

                validado = validado && DigitoVerificadorValido(valorString);
            }
            else
            {
                validado = true;
            }

            erroMensagem = validado ? string.Empty : "O campo deve ser um CNPJ válido";

            return validado;
        }

        #region Métodos Privados

        private static bool DigitoVerificadorValido(string cnpj)
        {
            var dozePrimeirosDigitosCnpj = ObterDozePrimeirosDigitosCnpj(cnpj);

            var primeiroDV = ObterPrimeiroDigitoVerificador(dozePrimeirosDigitosCnpj);
            var segundoDV = ObterSegundoDigitoVerificador(dozePrimeirosDigitosCnpj, primeiroDV);

            var digitosVerificadores = $"{primeiroDV}{segundoDV}";

            return cnpj.EndsWith(digitosVerificadores);
        }

        private static int ObterPrimeiroDigitoVerificador(string dozePrimeirosDigitosCnpj)
        {
            var multiplicador1 = new int[12] { 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };

            var soma = 0;

            for (int indice = 0; indice < 12; indice++)
                soma += int.Parse(dozePrimeirosDigitosCnpj[indice].ToString()) * multiplicador1[indice];

            var resto = (soma % 11);

            return resto < 2 ? 0 : 11 - resto;
        }

        private static int ObterSegundoDigitoVerificador(string dozePrimeirosDigitosCnpj, int primeiroDV)
        {
            var multiplicador2 = new int[13] { 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };

            var trezePrimeirosDigitosCnpj = dozePrimeirosDigitosCnpj + primeiroDV;

            var soma = 0;

            for (int i = 0; i < 13; i++)
                soma += int.Parse(trezePrimeirosDigitosCnpj[i].ToString()) * multiplicador2[i];

            var resto = (soma % 11);

            return resto < 2 ? 0 : 11 - resto;
        }

        private static string ObterDozePrimeirosDigitosCnpj(string cnpj)
        {
            return cnpj.Substring(0, 12);
        }

        #endregion
    }
}
