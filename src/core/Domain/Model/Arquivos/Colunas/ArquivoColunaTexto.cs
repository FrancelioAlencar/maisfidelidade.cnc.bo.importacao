﻿using maisfidelidade.cnc.imp.core.Domain.Model.Enums;
using System.Linq;

namespace maisfidelidade.cnc.imp.core.Domain.Model.Arquivos
{
	public class ArquivoColunaTexto : ArquivoColuna
    {
        public ArquivoColunaTexto()
            : base(TipoColunaEnum.Texto)
        {

        }
        protected override bool ValidarValor(string valorString, out string erroMensagem)
        {
            bool validado;

            if (PreenchimentoObrigatorio)
            {
                validado = !string.IsNullOrWhiteSpace(valorString);
            }
            else
            {
                validado = true;
            }

            erroMensagem = validado ? string.Empty : "O campo deve estar em branco.";

            if (validado && Opcoes.Count > 0)
            {
                validado = Opcoes.Any(opcao => opcao.ToUpper().Equals(valorString.ToUpper()));

                erroMensagem = validado ? string.Empty : $"O campo deve conter um dos seguintes valores válidos: {string.Join(", ", Opcoes)}";
            }

            return validado;
        }
    }
}
