﻿using maisfidelidade.cnc.imp.core.Common.Helpers;
using maisfidelidade.cnc.imp.core.Domain.Model.Enums;
using maisfidelidade.cnc.imp.core.Infrastructure.DomainBase;
using maisfidelidade.cnc.imp.core.Infrastructure.DomainBase.MongoDB;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace maisfidelidade.cnc.imp.core.Domain.Model.Arquivos
{
	[BsonDiscriminator(RootClass = true)]
	[BsonKnownTypes(typeof(ArquivoColunaTexto), typeof(ArquivoColunaNumeroInteiro), typeof(ArquivoColunaNumeroDecimal), typeof(ArquivoColunaBooleano),
		typeof(ArquivoColunaData), typeof(ArquivoColunaCNPJ), typeof(ArquivoColunaEmail), typeof(ArquivoColunaTelefone))]
	public abstract class ArquivoColuna : MongoEntityBase, IEntity
	{
		protected ArquivoColuna(TipoColunaEnum tipoColuna)
		{
			this.TipoColuna = tipoColuna;

			this.Opcoes = new List<string>();
		}

		[BsonElement("_id_coluna")]
		public int ColunaId { get; set; }

		[BsonElement("_id_coluna_tipo")]
		public int TipoColunaId { get; set; }

		[BsonElement("coluna_grupos")]
		public IList<int> Grupos { get; set; }

		[BsonElement("nome")]
		public string Nome { get; set; }

		[BsonElement("preenchimento_obrigatorio")]
		public bool PreenchimentoObrigatorio { get; set; }

		[BsonElement("ativo")]
		public bool Ativo { get; set; }

		[BsonElement("opcoes")]
		public IList<string> Opcoes { get; set; }

		public TipoColunaEnum TipoColuna { get; private set; }

		public virtual bool Validar(object valor, out string erroMensagem)
		{
			bool validado;

			if (Validador.Nulo(valor))
			{
				validado = PreenchimentoObrigatorio ? false : true;

				erroMensagem = PreenchimentoObrigatorio ? "O campo não pode está nulo." : string.Empty;
			}
			else
			{
				validado = ValidarValor(valor.ToString(), out erroMensagem);
			}

			return validado;
		}

		protected abstract bool ValidarValor(string valorString, out string erroMensagem);
	}
}