﻿using maisfidelidade.cnc.imp.core.Domain.Model.Enums;
using System.Text.RegularExpressions;

namespace maisfidelidade.cnc.imp.core.Domain.Model.Arquivos
{
    public class ArquivoColunaEmail : ArquivoColuna
    {
        public ArquivoColunaEmail()
            : base(TipoColunaEnum.Email)
        {

        }

        protected override bool ValidarValor(string valorString, out string erroMensagem)
        {
            bool validado;

            if (PreenchimentoObrigatorio)
            {
                validado = Regex.IsMatch(valorString, @"^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$");
            }
            else
            {
                validado = true;
            }

            erroMensagem = validado ? string.Empty : "O campo deve conter um e-mail válido";

            return validado;
        }
    }
}
