﻿using maisfidelidade.cnc.imp.core.Domain.Model.Enums;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace maisfidelidade.cnc.imp.core.Domain.Model.Arquivos
{
    public class ArquivoColunaTelefone : ArquivoColuna
    {
        public ArquivoColunaTelefone()
            : base(TipoColunaEnum.Telefone)
        {

        }

        protected override bool ValidarValor(string valorString, out string erroMensagem)
        {
            bool validado;

            if (PreenchimentoObrigatorio)
            {
                validado = Regex.IsMatch(valorString, @"\(\d{2,}\)[\s]{0,1}\d{4,}\-\d{4}$");
            }
            else
            {
                validado = true;
            }

            erroMensagem = validado ? string.Empty : "O campo deve ser um telefone válido";

            return validado;
        }
    }
}
