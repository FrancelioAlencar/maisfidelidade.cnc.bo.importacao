﻿using maisfidelidade.cnc.imp.core.Domain.Model.Enums;
using System;
using System.Globalization;

namespace maisfidelidade.cnc.imp.core.Domain.Model.Arquivos
{
    public class ArquivoColunaData : ArquivoColuna
    {
        public ArquivoColunaData()
            : base(TipoColunaEnum.Data)
        {

        }

        protected override bool ValidarValor(string valorString, out string erroMensagem)
        {
            bool validado;

            if (PreenchimentoObrigatorio)
            {
                validado = DateTime.TryParse(valorString, new CultureInfo("pt-BR"), DateTimeStyles.AssumeLocal, out _);
            }
            else
            {
                validado = true;
            }

            erroMensagem = validado ? string.Empty : "O campo deve ser um formato de data válido.";

            return validado;
        }
    }
}
