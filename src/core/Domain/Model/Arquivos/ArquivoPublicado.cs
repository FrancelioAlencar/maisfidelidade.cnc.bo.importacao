﻿using maisfidelidade.cnc.imp.core.Infrastructure.DomainBase;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace maisfidelidade.cnc.imp.core.Domain.Model.Arquivos
{
	public class ArquivoPublicado : EntityBase, IAggregateRoot
    {
        [Column("id_stage_arquivo")]
        public object StageArquivoId { get; set; }

        [Column("nome")]
        public string Nome { get; set; }

        [Column("tipo")]
        public int TipoArquivoId { get; set; }

        [Column("dt_processamento")]
        public DateTime DataProcessamento { get; set; }

        [Column("dt_referencia")]
        public DateTime DataReferencia { get; set; }

        [Column("dt_previsao_atualizacao")]
        public DateTime DataPrevisaoAtualizacao { get; set; }

        [Column("dt_importacao")]
        public DateTime DataImportacao { get; set; }

        [Column("dt_publicacao")]
        public DateTime DataPublicacao { get; set; }

        [Column("total_linhas")]
        public int TotalLinhas { get; set; }
    }
}
