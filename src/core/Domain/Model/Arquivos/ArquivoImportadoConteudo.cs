﻿using maisfidelidade.cnc.imp.core.Infrastructure.DomainBase.MongoDB;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace maisfidelidade.cnc.imp.core.Domain.Model.Arquivos
{
	public class ArquivoImportadoConteudo : MongoEntityBase
    {
        public ArquivoImportadoConteudo()
        {
            Linhas = new List<ArquivoImportadoLinha>();
        }

        [BsonElement("_id_arquivo")]
        public object ArquivoImportadoId { get; set; }

        [BsonElement("dt_inclusao")]
        public DateTime DataInclusao { get; set; }

        [BsonElement("linhas")]
        public IList<ArquivoImportadoLinha> Linhas { get; set; }

        //public DateTime? ObterDataReferencia(string colunaNome)
        //{
        //    return null;
        //}
    }
}
