﻿using maisfidelidade.cnc.imp.core.Domain.Model.Enums;
using maisfidelidade.cnc.imp.core.Infrastructure.RepositoryFramework;
using System.Collections.Generic;

namespace maisfidelidade.cnc.imp.core.Domain.Model.Arquivos
{
    public interface IArquivoMongoDBRepository : IRepository<ArquivoImportado>
    {
        ArquivoImportado BuscarPor(object id);
        ArquivoImportado BuscarPor(string nome);
        ArquivoImportadoConteudo BuscarConteudoPor(object idArquivo);
        IList<ArquivoImportadoLinha> BuscarArquivoImportadoLinhas(object idArquivo, int skip, int quantidadeLinhas);        
        void Salvar(ArquivoImportado arquivo);
        void SalvarConteudo(ArquivoImportadoConteudo arquivoConteudo);
        IDictionary<int, ArquivoColuna> ObterColunas();
        IDictionary<int, ArquivoColuna> ObterColunas(GrupoColunaEnum grupoColuna);
    }
}
