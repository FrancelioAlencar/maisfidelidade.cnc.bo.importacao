﻿using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.imp.core.Domain.Model.Arquivos
{
    public static class ArquivoColunaIndexes
    {
        public const string GUID = "GUID";
        public const int NO_CNPJ = 1;
        public const int NO_TAB = 2;
        public const int NM_FANTASIA = 3;
        public const int NM_GRUPO = 4;
        public const int NM_SEGMENTO = 5;
        public const int DS_EMAIL = 6;
        public const int DT_PROCESSAMENTO = 7;
        public const int DT_REFERENCIA = 8;
        public const int REF_ARQUIVO = 9;
        public const int DT_PREVISAO_ATUALIZACAO = 10;
        public const int FINANCIADO_SANTANDER = 11;
        public const int FINANCIADO_LOJA = 12;
        public const int QTD_CONTRATOS = 16;
        public const int QTD_PRESTAMISTA = 17;
        public const int QTD_AUTO = 18;
        public const int QTD_AMBOS = 19;
		public const int MARKET_SHARE_PC = 20;
		public const int IPS_PC= 21;
		public const int MATRICULA_GR = 23;
        public const int NOME_GR = 24;
        public const int EMAIL_GR = 25;
        public const int TELEFONE_GR = 26;
        public const int MATRICULA_GC = 27;
        public const int NOME_GC = 28;
        public const int EMAIL_GC = 29;
        public const int TELEFONE_GC = 30;
        public const int SEGURO_AUTO_PC = 33;
        public const int SEGURO_AUTO_CONTRATOS_ELEGIVEIS = 34;
        public const int SEGURO_AUTO_CONTRATOS_CONVERTIDOS = 35;
        public const int SEGURO_PRESTAMISTA_PC = 36;
        public const int SEGURO_PRESTAMISTA_CONTRATOS_ELEGIVEIS = 37;
        public const int SEGURO_PRESTAMISTA_CONTRATOS_CONVERTIDOS = 38;
        public const int PLUS_MES_VIGENTE = 39;
        public const int PLUS_MES_ANTERIOR = 40;
        public const int PLUS_PROXIMO_MES = 41;
        public const int AUTO_MES_VIGENTE = 42;
        public const int AUTO_MES_ANTERIOR = 43;
        public const int AUTO_PROXIMO_MES = 44;
        public const int PRESTAMISTA_MES_VIGENTE = 45;
        public const int PRESTAMISTA_MES_ANTERIOR = 46;
		public const int PRESTAMISTA_PROXIMO_MES = 47;
        public const int CATEGORIA_ATUAL = 48;
        public const int FINANCIAMENTOS_REALIZADO = 49;
        public const int FINANCIAMENTOS_MAXIMA = 50;
        public const int SEGUROS_REALIZADO = 51;
        public const int SEGUROS_MAXIMA = 52;
        public const int PLUS_REALIZADO = 53;
        public const int PLUS_MAXIMA = 54;
        public const int RETORNO_REALIZADO = 55;
        public const int RETORNO_MAXIMA = 56;
        public const int PRESTAMISTA_REALIZADO = 57;
        public const int PRESTAMISTA_MAXIMA = 58;
        public const int AUTO_REALIZADO = 59;
        public const int AUTO_MAXIMA = 60;
        public const int POSSUI_MARKETING = 61;
        public const int POSSUI_CRV = 62;
        public const int POSSUI_MESA_CREDITO = 63;
        public const int QTD_GRAVAME = 64;
        public const int AMBOS_CONTRATOS = 65;
        public const int CONSOLIDADO_REALIZADO = 66;
        public const int CONSOLIDADO_MAXIMA = 67;
        public const int BANNER_VAI_ATINGIU = 68;
        public const int BANNER_CINEMA_ATINGIU = 69;
        public const int BANNER_CINEMA_LINK = 70;
    }
}
