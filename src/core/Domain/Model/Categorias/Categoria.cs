﻿using maisfidelidade.cnc.imp.core.Infrastructure.DomainBase;
using System.ComponentModel.DataAnnotations.Schema;

namespace maisfidelidade.cnc.imp.core.Domain.Model.Categorias
{
    public class Categoria : EntityBase, IAggregateRoot
    {
        [Column("nome")]
        public string Nome { get; set; }

        [Column("descricao")]
        public string Descricao { get; set; }
    }
}
