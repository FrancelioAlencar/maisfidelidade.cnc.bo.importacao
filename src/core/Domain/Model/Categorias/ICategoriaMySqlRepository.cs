﻿using maisfidelidade.cnc.imp.core.Infrastructure.RepositoryFramework;
using System.Collections.Generic;

namespace maisfidelidade.cnc.imp.core.Domain.Model.Categorias
{
    public interface ICategoriaMySqlRepository : IRepository<Categoria>
    {
        IList<Categoria> ObterCategorias();
    }
}
