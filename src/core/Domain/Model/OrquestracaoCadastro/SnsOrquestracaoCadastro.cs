﻿namespace maisfidelidade.cnc.imp.core.Domain.Model.OrquestracaoCadastro
{
	public class SnsOrquestracaoCadastro
    {
        public string ObjectId { get; set; }
        public CadastroEnum EtapaCadastro { get; set; }

        public SnsOrquestracaoCadastro()
        { }

        public SnsOrquestracaoCadastro(string objectId, CadastroEnum etapaCadastro)
        {
            ObjectId = objectId;
            EtapaCadastro = etapaCadastro;
        }
    }
}
