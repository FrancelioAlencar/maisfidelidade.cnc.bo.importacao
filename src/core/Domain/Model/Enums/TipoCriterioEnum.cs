namespace maisfidelidade.cnc.imp.core.Domain.Model.Enums
{
    public enum TipoCriterioEnum : int {

		FinanceiraMarketShare = 1,
		FinanceiraPagamentoCC = 2,
		FinanceiraIPSeguro = 3,
		WebmotorsAceiteLeads = 4,
		WebmotorsInadimplencia = 5,
		WebmotorsEstoquePotencial = 6,
		FinanceiraGetNet = 7,
		FinanceiraIndicacaoCC = 9
	}
}