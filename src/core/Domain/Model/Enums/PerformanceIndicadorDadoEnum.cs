﻿using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.imp.core.Domain.Model.PerformanceIndicadores
{
    public enum PerformanceIndicadorDadoEnum
    {
        AmbosQuantidadeContratos = 11,
        SeguroAutoContratosElegiveis = 21,
        SeguroAutoContratosConvertidos = 22,
        SeguroPrestamistaContratosElegiveis = 31,
        SeguroPrestamistaContratosConvertidos = 32
    }
}
