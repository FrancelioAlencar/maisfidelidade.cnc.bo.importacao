﻿namespace maisfidelidade.cnc.imp.core.Domain.Model.Enums
{
	public enum StatusLogEnum
    {
        ProcessandoWebmotors = 1,
        ProcessandoCriterios = 2,
        ProcessandoCampanhas = 3,
        ProcessandoIndicadores = 4,
        ProcessandoExtrato = 5,
        ProcessandoAcaoPlatinum = 6,
        ValidacaoFisica = 7,
        ValidacaoLogica = 8,
        Orquestracao = 9,
        Publicando = 10,
        CanceladoParaReenvio = 11
    }
}
