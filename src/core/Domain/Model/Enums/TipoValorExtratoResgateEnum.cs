﻿namespace maisfidelidade.cnc.imp.core.Domain.Model.Enums
{
	public enum TipoValorExtratoResgate
	{
		Quantidade = 1,
		Ponto = 2,
		Monetario = 3
	}
}
