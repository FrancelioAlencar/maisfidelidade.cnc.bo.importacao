﻿namespace maisfidelidade.cnc.imp.core.Domain.Model.Enums
{
	public enum StatusAndamentoProcessoEnum
    {
        EnvioArquivoEfetuado = 0,
        AguardandoValidacaoFisica = 101,
        RealizandoValidacaoFisica = 102,
        AguardandoValidacaoLogica = 201,
        RealizandoValidacaoLogica = 202,
        ProcessandoComplementosWebmotors = 3,
        ProcessandoApuracao = 4,
        AguardandoPublicacao = 5,
        PublicandoImportacao = 6,
        Concluido = 7,
        Cancelado = 8
    }
}
