﻿namespace maisfidelidade.cnc.imp.core.Domain.Model.PerformanceIndicadores
{
	public enum PerformanceIndicadorEnum
    {
        Ambos = 10,
        SeguroAuto = 20,
        SeguroPrestamista = 30
    }
}
