namespace maisfidelidade.cnc.imp.core.Domain.Model.Enums
{
    public enum TipoArquivoEnum
	{
		Semanal = 1,
		Mensal = 2
	}
}