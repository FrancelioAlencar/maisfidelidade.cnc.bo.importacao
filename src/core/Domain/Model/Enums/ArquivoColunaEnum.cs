﻿namespace maisfidelidade.cnc.imp.core.Domain.Model.Enums
{
	public enum ArquivoColunaEnum
    {
        NO_CNPJ_INTERMEDIARIO = 1,
        MATRICULA_GR = 21,
        NOME_GR = 22,
        EMAIL_GR = 23,
        TELEFONE_GR = 24,
        MATRICULA_GC = 25,
        NOME_GC = 26,
        EMAIL_GC = 27,
        TELEFONE_GC = 28
    }
}
