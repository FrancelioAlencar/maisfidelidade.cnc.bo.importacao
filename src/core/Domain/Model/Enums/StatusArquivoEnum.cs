namespace maisfidelidade.cnc.imp.core.Domain.Model.Enums
{
    public enum StatusArquivoEnum : int {

		Pendente = 1,
		Processando = 2,
		ProcessadoComSucesso = 3,
		ErroAoProcessar = 5,
		AguardandoAprovacao = 4,
		AguardandoPublicacao = 6,
		Publicando = 7,
		PublicadoComSucesso = 8,
		ErroAoPublicar = 9,
		ErroOrquestracao = 10,
		Cancelado = 11
	}
}