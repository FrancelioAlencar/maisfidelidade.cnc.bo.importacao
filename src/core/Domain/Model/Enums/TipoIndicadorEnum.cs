namespace maisfidelidade.cnc.imp.core.Domain.Model.Enums
{
    public enum TipoIndicadorEnum : int {

		Indefinido = 0,
		Reclamacoes = 1,
		Inadimplencia = 2,
		ParceriaCerta = 3,
		SeguroAuto = 4,
		TempoPrimeiraResposta = 5,
		CRMSmart = 6
	}

}
