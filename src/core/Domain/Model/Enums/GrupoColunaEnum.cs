﻿namespace maisfidelidade.cnc.imp.core.Domain.Model.Enums
{
	public enum GrupoColunaEnum
    {
        DataProcessamento = 1,
        DataReferencia = 2,
        ReferenciaArquivo = 3,
        DataPrevisaoAtualizacao = 4,
        Empresa = 10,
        Gerente = 11,
        Apuracao = 12,
        PerformanceIndicador = 13,
		RentabilidadeIndicador = 14,
		ClassificacaoIndicador = 15,
        Banner = 16
	}
}
