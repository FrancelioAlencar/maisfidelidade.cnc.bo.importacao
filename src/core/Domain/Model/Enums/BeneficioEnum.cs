﻿namespace maisfidelidade.cnc.imp.core.Domain.Model.Enums
{
	public enum BeneficioEnum
	{
		Feirao = 2,
		MaterialPDV = 3,
		DescontoPlanoLead = 7,
		ProtecaoPremiada = 9,
		Blindagem = 10,
		GearBox = 11,
		AutoGuru = 12,
		Gravame = 13,
		NegociacaoTaxa = 14,
		AutoCompara = 16,
		FeiraoExtensaoMega = 17,
		ProrrogacaoDocumentos = 18,
		CRMSmart = 19,
		BonusIndicacaoCCPF = 20,
		EsquentaLead = 21,
		PagamentoFDS = 22,
		Plus = 23,
		MaxBonusPF = 24
	}
}
