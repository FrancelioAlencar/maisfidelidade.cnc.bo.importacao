﻿namespace maisfidelidade.cnc.imp.core.Domain.Model.Enums
{
	public enum TipoCampanhaEnum
    {
        Padrao = 1,
        Banner = 2,
        Todos = 3
    }
}
