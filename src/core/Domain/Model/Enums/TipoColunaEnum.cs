﻿namespace maisfidelidade.cnc.imp.core.Domain.Model.Enums
{
	public enum TipoColunaEnum
    {
        NaoDefinido = 0,
        Texto = 1,
        NumeroInteiro = 2,
        NumeroDecimal = 3,
        Booleano = 4,
        Data = 5,
        CNPJ = 6,
        Email = 7,
        Telefone = 8
    }
}
