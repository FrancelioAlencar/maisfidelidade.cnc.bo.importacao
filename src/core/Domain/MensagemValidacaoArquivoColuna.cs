﻿namespace maisfidelidade.cnc.imp.core.Domain
{
	public class MensagemValidacaoArquivoColuna
    {
        public MensagemValidacaoArquivoColuna(int linha, string coluna, string mensagem)
        {
            Linha = linha;
            Coluna = coluna;
            Mensagem = mensagem;
        }

        public int Linha { get; private set; }
        public string Coluna { get; private set; }
        public string Mensagem { get; private set; }
    }
}
