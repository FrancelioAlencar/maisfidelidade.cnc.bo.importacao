﻿using maisfidelidade.cnc.imp.core.Common.DependencyInjection;
using maisfidelidade.cnc.imp.core.Domain.Model.Apuracoes;
using maisfidelidade.cnc.imp.core.Domain.Model.Arquivos;
using maisfidelidade.cnc.imp.core.Domain.Model.Banners;
using maisfidelidade.cnc.imp.core.Domain.Model.ClassificacaoIndicadores;
using maisfidelidade.cnc.imp.core.Domain.Model.Empresas;
using maisfidelidade.cnc.imp.core.Domain.Model.Enums;
using maisfidelidade.cnc.imp.core.Domain.Model.Gerentes;
using maisfidelidade.cnc.imp.core.Domain.Model.PerformanceIndicadores;
using maisfidelidade.cnc.imp.core.Domain.Model.RentabilidadeIndicadores;
using System;
using System.Collections.Generic;
using System.Linq;

namespace maisfidelidade.cnc.imp.core.Infrastructure.Publication
{
    public static class PublicationService
    {
        private static readonly IArquivoMongoDBRepository arquivoMongoDBRepository;
        private static readonly IEmpresaMongoDBRepository empresaMongoDBRepository;
        private static readonly IGRMongoDBRepository grMongoDBRepository;
        private static readonly IGCMongoDBRepository gcMongoDBRepository;
        private static readonly IApuracaoMongoDBRepository apuracaoMongoDBRepository;
        private static readonly IClassificacaoIndicadorMongoDBRepository classificacaoIndicadorMongoDBRepository;
        private static readonly IPerformanceIndicadorMongoDBRepository performanceIndicadorMongoDBRepository;
        private static readonly IRentabilidadeIndicadorMongoDBRepository rentabilidadeIndicadorMongoDBRepository;

        static PublicationService()
        {
            arquivoMongoDBRepository = DependencyResolver.GetService<IArquivoMongoDBRepository>();
            empresaMongoDBRepository = DependencyResolver.GetService<IEmpresaMongoDBRepository>();
            grMongoDBRepository = DependencyResolver.GetService<IGRMongoDBRepository>();
            gcMongoDBRepository = DependencyResolver.GetService<IGCMongoDBRepository>();
            apuracaoMongoDBRepository = DependencyResolver.GetService<IApuracaoMongoDBRepository>();
            classificacaoIndicadorMongoDBRepository = DependencyResolver.GetService<IClassificacaoIndicadorMongoDBRepository>();
            performanceIndicadorMongoDBRepository = DependencyResolver.GetService<IPerformanceIndicadorMongoDBRepository>();
            rentabilidadeIndicadorMongoDBRepository = DependencyResolver.GetService<IRentabilidadeIndicadorMongoDBRepository>();
        }

        public static PublishedData GetPublishedData(object arquivoId)
        {
            try
            {
                if (ExisteArquivoPublicado(arquivoId, out ArquivoImportado arquivoImportado))
                {
                    throw new Exception("");
                }

                //if (arquivoImportado.StatusArquivo != StatusArquivoEnum.AguardandoPublicacao)
                //{
                //    throw new Exception("");
                //}

                AtualizarStatusParaPublicando(arquivoImportado);
                MoverArquivoFisicoParaPastaPublicando(arquivoImportado.Nome);

                var publishedData = new PublishedData
                {
                    Arquivo = GetArquivo(arquivoImportado),
                    Empresas = GetEmpresas(arquivoImportado),
                    Gerentes = GetGerentes(arquivoImportado),
                    Apuracoes = GetApuracoes(arquivoImportado),
                    ClassificacaoIndicadorApuracoes = GetClassificacaoIndicadorApuracoes(arquivoImportado),
                };

                AtribuirPerformanceIndicadorApuracoes(arquivoImportado, publishedData);
                AtribuirRentabilidadeApuracoes(arquivoImportado, publishedData);

                return publishedData;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #region Métodos Privados

        private static bool ExisteArquivoPublicado(object arquivoId, out ArquivoImportado arquivoImportado)
        {
            var result = false;

            arquivoImportado = arquivoMongoDBRepository.BuscarPor(arquivoId);

            return result;
        }

        private static void AtualizarStatusParaPublicando(ArquivoImportado arquivoImportado)
        {
            try
            {
                //arquivoImportado.StatusArquivo = StatusArquivoEnum.Publicando;
                arquivoImportado.DataPublicacao = DateTime.Now;

                arquivoMongoDBRepository.Salvar(arquivoImportado);
            }
            catch (Exception exception)
            {
                throw new Exception("some reason to rethrow", exception);
            }
        }

        private static void AtualizarStatusParaPublicado(ArquivoImportado arquivoImportado)
        {
            try
            {
                arquivoImportado.StatusArquivo = StatusArquivoEnum.PublicadoComSucesso;

                //arquivoMongoDBRepositorio.Salvar(arquivoImportado);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        private static void MoverArquivoFisicoParaPastaPublicando(string arquivoNome)
        {
            try
            {

            }
            catch (Exception exception)
            {

            }
        }

        private static void MoverArquivoFisicoParaPastaPublicado(string arquivoNome)
        {
            try
            {

            }
            catch (Exception exception)
            {

            }
        }

        private static ArquivoPublicado GetArquivo(ArquivoImportado arquivoImportado)
        {
            return new ArquivoPublicado
            {
                StageArquivoId = arquivoImportado.Id,
                Nome = arquivoImportado.Nome,
                TipoArquivoId = (int)arquivoImportado.TipoArquivo,
                DataProcessamento = Convert.ToDateTime(arquivoImportado.DataProcessamento),
                DataReferencia = Convert.ToDateTime(arquivoImportado.DataReferencia),
                DataPrevisaoAtualizacao = Convert.ToDateTime(arquivoImportado.DataPrevisaoAtualizacao),
                DataImportacao = arquivoImportado.DataImportacao,
                DataPublicacao = Convert.ToDateTime(arquivoImportado.DataPublicacao),
                TotalLinhas = arquivoImportado.TotalLinhasProcessadas
            };
        }

        private static IList<Empresa> GetEmpresas(ArquivoImportado arquivoImportado)
        {
            var empresas = new List<Empresa>();

            var empresaImportadas = empresaMongoDBRepository.BuscarEmpresasImportadas(arquivoImportado.Id);

            empresas.AddRange(empresaImportadas.Select(e => new Empresa
            {
                Cnpj = e.Cnpj,
                NomeFantasia = e.NomeFantasia,
                NomeGrupo = e.NomeGrupo,
                Email = e.Email,
                SegmentoId = e.SegmentoId,
                MatriculaGR = e.MatriculaGR,
                MatriculaGC = e.MatriculaGC
            }));

            return empresas;
        }

        private static IList<Gerente> GetGerentes(ArquivoImportado arquivoImportado)
        {
            var gerentes = new List<Gerente>();

            var grImportados = grMongoDBRepository.BuscarGRImportados(arquivoImportado.Id);
            var gcImportados = gcMongoDBRepository.BuscarGCImportados(arquivoImportado.Id);

            gerentes.AddRange(grImportados.Select(gr => new Gerente
            {
                Matricula = gr.Matricula,
                Nome = gr.Nome,
                Email = gr.Email,
                Telefone = gr.Telefone,
                TipoGerenteId = (int)TipoGerenteEnum.GR
            }).ToList());
            gerentes.AddRange(gcImportados.Select(gc => new Gerente
            {
                Matricula = gc.Matricula,
                Nome = gc.Nome,
                Email = gc.Email,
                Telefone = gc.Telefone,
                TipoGerenteId = (int)TipoGerenteEnum.GC
            }).ToList());

            return gerentes;
        }

        private static IList<Apuracao> GetApuracoes(ArquivoImportado arquivoImportado)
        {
            var apuracoes = new List<Apuracao>();

            var empresaApuracoes = apuracaoMongoDBRepository.BuscarApuracoes(arquivoImportado.Id);

            apuracoes.AddRange(empresaApuracoes.Select(a => new Apuracao
            {
                Guid = a.Guid,
                Cnpj = a.Cnpj,
                CategoriaId = a.CategoriaId,
                FinanciadoSantander = a.FinanciamentoSantander,
                FinanciadoLoja = a.FinanciamentoLoja,
                QuantidadeContratos = a.QuantidadeContratos,
                QuantidadePrestamista = a.QuantidadePrestamista,
                QuantidadeAuto = a.QuantidadeAuto,
                QuantidadeAmbos = a.QuantidadeAmbos,
                PlusMesAnterior = a.PlusMesAnterior,
                PlusMesVigente = a.PlusMesVigente,
                PlusProximoMes = a.PlusProximoMes,
                AutoMesAnterior = a.AutoMesAnterior,
                AutoMesVigente = a.AutoMesVigente,
                AutoProximoMes = a.AutoProximoMes,
                PrestamistaMesAnterior = a.PrestamistaMesAnterior,
                PrestamistaMesVigente = a.PrestamistaMesVigente,
                PrestamistaProximoMes = a.PrestamistaProximoMes
            }));

            return apuracoes;
        }

        private static IList<BannerApuracao> GetBannerApuracoes(ArquivoImportado arquivoImportado)
        {
            var apuracoes = new List<BannerApuracao>();

            return apuracoes;
        }

        private static IList<ClassificacaoIndicadorApuracao> GetClassificacaoIndicadorApuracoes(ArquivoImportado arquivoImportado)
        {
            var apuracoes = new List<ClassificacaoIndicadorApuracao>();

            var classificacaoIndicadorApuracoes = classificacaoIndicadorMongoDBRepository.BuscarClassificacaoIndicadorApuracoes(arquivoImportado.Id);

            foreach (var classificacaoIndicadorApuracao in classificacaoIndicadorApuracoes)
            {
                foreach (var classificacaoIndicadorApurado in classificacaoIndicadorApuracao.ClassificacaoIndicadorApurados)
                {
                    if (classificacaoIndicadorApurado is MarketShareClassificacaoIndicadorApurado)
                    {
                        var marketShareClassificacaoIndicadorApurado = classificacaoIndicadorApurado as MarketShareClassificacaoIndicadorApurado;

                        apuracoes.Add(new ClassificacaoIndicadorApuracao
                        {
                            Guid = classificacaoIndicadorApuracao.Guid,
                            ClassificacaoIndicadorId = marketShareClassificacaoIndicadorApurado.ClassificacaoIndicadorId,
                            Valor = marketShareClassificacaoIndicadorApurado.Porcentagem.ToString()
                        });
                    }

                    if (classificacaoIndicadorApurado is IPSegurosClassificacaoIndicadorApurado)
                    {
                        var ipSeguroClassificacaoIndicadorApurado = classificacaoIndicadorApurado as IPSegurosClassificacaoIndicadorApurado;

                        apuracoes.Add(new ClassificacaoIndicadorApuracao
                        {
                            Guid = classificacaoIndicadorApuracao.Guid,
                            ClassificacaoIndicadorId = ipSeguroClassificacaoIndicadorApurado.ClassificacaoIndicadorId,
                            Valor = ipSeguroClassificacaoIndicadorApurado.Porcentagem.ToString()
                        });
                    }
                }
            }

            return apuracoes;
        }

        public static IList<PerformanceIndicadorDadoApuracao> GetAmbosPerformanceIndicadorDadoApuracoes(PerformanceIndicadorApuracaoImportado performanceIndicadorApuracaoImportado,
            AmbosPerformanceIndicadorApurado ambosPerformanceIndicadorApurado, PerformanceIndicadorDadoPrimaryKeys performanceIndicadorDadoPrimaryKeys)
        {
            var performanceIndicadorDadoApuracoes = new List<PerformanceIndicadorDadoApuracao>
            {
                new PerformanceIndicadorDadoApuracao
                {
                    ApuracaoGuid = performanceIndicadorApuracaoImportado.Guid,
                    PerformanceIndicadorId = ambosPerformanceIndicadorApurado.PerformanceIndicadorId,
                    PerformanceIndicadorDadoId = performanceIndicadorDadoPrimaryKeys.AmbosQuantidadeContratosId,
                    Valor = ambosPerformanceIndicadorApurado.QuantidadeContratos.ToString()
                }
            };

            return performanceIndicadorDadoApuracoes;
        }

        public static IList<PerformanceIndicadorDadoApuracao> GetSeguroAutoPerformanceIndicadorDadoApuracoes(PerformanceIndicadorApuracaoImportado performanceIndicadorApuracaoImportado, 
            SeguroAutoPerformanceIndicadorApurado seguroAutoPerformanceIndicadorApurado, PerformanceIndicadorDadoPrimaryKeys performanceIndicadorDadoPrimaryKeys)
        {
            var performanceIndicadorDadoApuracoes = new List<PerformanceIndicadorDadoApuracao>
            {
                new PerformanceIndicadorDadoApuracao
                {
                    ApuracaoGuid = performanceIndicadorApuracaoImportado.Guid,
                    PerformanceIndicadorId = seguroAutoPerformanceIndicadorApurado.PerformanceIndicadorId,
                    PerformanceIndicadorDadoId = performanceIndicadorDadoPrimaryKeys.SeguroAutoContratosElegiveisId,
                    Valor = seguroAutoPerformanceIndicadorApurado.ContratosElegiveis.ToString()
                },
                new PerformanceIndicadorDadoApuracao
                {
                    ApuracaoGuid = performanceIndicadorApuracaoImportado.Guid,
                    PerformanceIndicadorId = seguroAutoPerformanceIndicadorApurado.PerformanceIndicadorId,
                    PerformanceIndicadorDadoId = performanceIndicadorDadoPrimaryKeys.SeguroAutoContratosConvertidosId,
                    Valor = seguroAutoPerformanceIndicadorApurado.ContratosConvertidos.ToString()
                }
            };

            return performanceIndicadorDadoApuracoes;
        }

        public static IList<PerformanceIndicadorDadoApuracao> GetSeguroPrestamistaPerformanceIndicadorDadoApuracoes(PerformanceIndicadorApuracaoImportado performanceIndicadorApuracaoImportado, 
            SeguroPrestamistaPerformanceIndicadorApurado seguroPrestamistaPerformanceIndicadorApurado, PerformanceIndicadorDadoPrimaryKeys performanceIndicadorDadoPrimaryKeys)
        {
            var performanceIndicadorDadoApuracoes = new List<PerformanceIndicadorDadoApuracao>
            {
                new PerformanceIndicadorDadoApuracao
                {
                    ApuracaoGuid = performanceIndicadorApuracaoImportado.Guid,
                    PerformanceIndicadorId = seguroPrestamistaPerformanceIndicadorApurado.PerformanceIndicadorId,
                    PerformanceIndicadorDadoId = performanceIndicadorDadoPrimaryKeys.SeguroPrestamistaContratosElegiveisId,
                    Valor = seguroPrestamistaPerformanceIndicadorApurado.ContratosElegiveis.ToString()
                },
                new PerformanceIndicadorDadoApuracao
                {
                    ApuracaoGuid = performanceIndicadorApuracaoImportado.Guid,
                    PerformanceIndicadorId = seguroPrestamistaPerformanceIndicadorApurado.PerformanceIndicadorId,
                    PerformanceIndicadorDadoId = performanceIndicadorDadoPrimaryKeys.SeguroPrestamistaContratosConvertidosId,
                    Valor = seguroPrestamistaPerformanceIndicadorApurado.ContratosConvertidos.ToString()
                }
            };

            return performanceIndicadorDadoApuracoes;
        }

        private static IList<RentabilidadeIndicadorApuracao> GetRentabilidadeIndicadorApuracoes(RentabilidadeApuracaoImportada rentabilidadeApuracaoImportada)
        {
            var rentabilidadeIndicadorApuracaoApuracoes = new List<RentabilidadeIndicadorApuracao>();

            foreach (var rentabilidadeIndicadorApurado in rentabilidadeApuracaoImportada.RentabilidadeIndicadorApurados)
            {
                if (rentabilidadeIndicadorApurado is RentabilidadeIndicadorApuradoPlus)
                {
                    var plusRentabilidadeIndicadorApurado = rentabilidadeIndicadorApurado as RentabilidadeIndicadorApuradoPlus;

                    rentabilidadeIndicadorApuracaoApuracoes.Add(new RentabilidadeIndicadorApuracao
                    {
                        Guid = rentabilidadeApuracaoImportada.Guid,
                        RentabilidadeIndicadorId = plusRentabilidadeIndicadorApurado.RentabilidadeIndicadorId,
                        Maximo = plusRentabilidadeIndicadorApurado.Maximo.ToString(),
                        Realizado = plusRentabilidadeIndicadorApurado.Realizado.ToString()
                    });
                }

                if (rentabilidadeIndicadorApurado is RentabilidadeIndicadorApuradoRetorno)
                {
                    var retornoRentabilidadeIndicadorApurado = rentabilidadeIndicadorApurado as RentabilidadeIndicadorApuradoRetorno;

                    rentabilidadeIndicadorApuracaoApuracoes.Add(new RentabilidadeIndicadorApuracao
                    {
                        Guid = rentabilidadeApuracaoImportada.Guid,
                        RentabilidadeIndicadorId = retornoRentabilidadeIndicadorApurado.RentabilidadeIndicadorId,
                        Maximo = retornoRentabilidadeIndicadorApurado.Maximo.ToString(),
                        Realizado = retornoRentabilidadeIndicadorApurado.Realizado.ToString()
                    });
                }

                if (rentabilidadeIndicadorApurado is RentabilidadeIndicadorApuradoAuto)
                {
                    var autoRentabilidadeIndicadorApurado = rentabilidadeIndicadorApurado as RentabilidadeIndicadorApuradoAuto;

                    rentabilidadeIndicadorApuracaoApuracoes.Add(new RentabilidadeIndicadorApuracao
                    {
                        Guid = rentabilidadeApuracaoImportada.Guid,
                        RentabilidadeIndicadorId = autoRentabilidadeIndicadorApurado.RentabilidadeIndicadorId,
                        Maximo = autoRentabilidadeIndicadorApurado.Maximo.ToString(),
                        Realizado = autoRentabilidadeIndicadorApurado.Realizado.ToString()
                    });
                }

                if (rentabilidadeIndicadorApurado is RentabilidadeIndicadorApuradoPrestamista)
                {
                    var prestamistaRentabilidadeIndicadorApurado = rentabilidadeIndicadorApurado as RentabilidadeIndicadorApuradoPrestamista;

                    rentabilidadeIndicadorApuracaoApuracoes.Add(new RentabilidadeIndicadorApuracao
                    {
                        Guid = rentabilidadeApuracaoImportada.Guid,
                        RentabilidadeIndicadorId = prestamistaRentabilidadeIndicadorApurado.RentabilidadeIndicadorId,
                        Maximo = prestamistaRentabilidadeIndicadorApurado.Maximo.ToString(),
                        Realizado = prestamistaRentabilidadeIndicadorApurado.Realizado.ToString()
                    });
                }

                if (rentabilidadeIndicadorApurado is RentabilidadeIndicadorApuradoMarketing)
                {
                    var marketingRentabilidadeIndicadorApurado = rentabilidadeIndicadorApurado as RentabilidadeIndicadorApuradoMarketing;

                    rentabilidadeIndicadorApuracaoApuracoes.Add(new RentabilidadeIndicadorApuracao
                    {
                        Guid = rentabilidadeApuracaoImportada.Guid,
                        RentabilidadeIndicadorId = marketingRentabilidadeIndicadorApurado.RentabilidadeIndicadorId,
                        Maximo = $"{1}",
                        Realizado = marketingRentabilidadeIndicadorApurado.Ativo ? $"{1}" : $"{0}"
                    });
                }

                if (rentabilidadeIndicadorApurado is RentabilidadeIndicadorApuradoConsolidado)
                {
                    var consolidadoRentabilidadeIndicadorApurado = rentabilidadeIndicadorApurado as RentabilidadeIndicadorApuradoConsolidado;

                    rentabilidadeIndicadorApuracaoApuracoes.Add(new RentabilidadeIndicadorApuracao
                    {
                        Guid = rentabilidadeApuracaoImportada.Guid,
                        RentabilidadeIndicadorId = consolidadoRentabilidadeIndicadorApurado.RentabilidadeIndicadorId,
                        Maximo = consolidadoRentabilidadeIndicadorApurado.Maximo.ToString(),
                        Realizado = consolidadoRentabilidadeIndicadorApurado.Realizado.ToString()
                    });
                }
            }

            return rentabilidadeIndicadorApuracaoApuracoes;
        }

        private static IList<RentabilidadeBeneficioApuracao> GetRentabilidadeBeneficioApuracoes(RentabilidadeApuracaoImportada rentabilidadeApuracao)
        {
            var rentabilidadeBeneficioApuracoes = new List<RentabilidadeBeneficioApuracao>();

            foreach (var rentabilidadeBeneficioApurado in rentabilidadeApuracao.RentabilidadeBeneficioApurados)
            {
                if (rentabilidadeBeneficioApurado is RentabilidadeBeneficioApuradoGravame)
                {
                    var gravameRentabilidadeBeneficioApurado = rentabilidadeBeneficioApurado as RentabilidadeBeneficioApuradoGravame;

                    rentabilidadeBeneficioApuracoes.Add(new RentabilidadeBeneficioApuracao
                    {
                        Guid = rentabilidadeApuracao.Guid,
                        RentabilidadeBeneficioId = gravameRentabilidadeBeneficioApurado.RentabilidadeBeneficioId,
                        Valor = gravameRentabilidadeBeneficioApurado.QuantidadeGravame.ToString()
                    });
                }

                if (rentabilidadeBeneficioApurado is RentabilidadeBeneficioApuradoCRV)
                {
                    var crvRentabilidadeBeneficioApurado = rentabilidadeBeneficioApurado as RentabilidadeBeneficioApuradoCRV;

                    rentabilidadeBeneficioApuracoes.Add(new RentabilidadeBeneficioApuracao
                    {
                        Guid = rentabilidadeApuracao.Guid,
                        RentabilidadeBeneficioId = crvRentabilidadeBeneficioApurado.RentabilidadeBeneficioId,
                        Valor = crvRentabilidadeBeneficioApurado.PossuiCRV ? $"{1}" : $"{0}"
                    });
                }

                if (rentabilidadeBeneficioApurado is RentabilidadeBeneficioApuradoMesaCredito)
                {
                    var mesaCreditoRentabilidadeBeneficioApurado = rentabilidadeBeneficioApurado as RentabilidadeBeneficioApuradoMesaCredito;

                    rentabilidadeBeneficioApuracoes.Add(new RentabilidadeBeneficioApuracao
                    {
                        Guid = rentabilidadeApuracao.Guid,
                        RentabilidadeBeneficioId = mesaCreditoRentabilidadeBeneficioApurado.RentabilidadeBeneficioId,
                        Valor = mesaCreditoRentabilidadeBeneficioApurado.PossuiMesaCredito ? $"{1}" : $"{0}"
                    });
                }
            }

            return rentabilidadeBeneficioApuracoes;
        }

        private static void AtribuirPerformanceIndicadorApuracoes(ArquivoImportado arquivoImportado, PublishedData publishedData)
        {
            var performanceIndicadorApuracaoImportados = performanceIndicadorMongoDBRepository.BuscarPerformanceIndicadorApuracoes(arquivoImportado.Id);

            var performanceIndicadorDadoPrimaryKeys = new PerformanceIndicadorDadoPrimaryKeys();

            var performanceIndicadorApuracoes = new List<PerformanceIndicadorApuracao>();
            var performanceIndicadorDadoApuracoes = new List<PerformanceIndicadorDadoApuracao>();

            foreach (var performanceIndicadorApuracaoImportado in performanceIndicadorApuracaoImportados)
            {
                foreach (var performanceIndicadorApurado in performanceIndicadorApuracaoImportado.PerformanceIndicadorApurados)
                {
                    if (performanceIndicadorApurado is AmbosPerformanceIndicadorApurado)
                    {
                        var ambosPerformanceIndicadorApurado = performanceIndicadorApurado as AmbosPerformanceIndicadorApurado;

                        performanceIndicadorApuracoes.Add(new PerformanceIndicadorApuracao
                        {
                            ParentGuid = performanceIndicadorApuracaoImportado.Guid,
                            PerformanceIndicadorId = ambosPerformanceIndicadorApurado.PerformanceIndicadorId,
                            Valor = null
                        });

                        performanceIndicadorDadoApuracoes.AddRange(GetAmbosPerformanceIndicadorDadoApuracoes(performanceIndicadorApuracaoImportado,
                            ambosPerformanceIndicadorApurado, performanceIndicadorDadoPrimaryKeys));
                    }

                    if (performanceIndicadorApurado is SeguroAutoPerformanceIndicadorApurado)
                    {
                        var seguroAutoPerformanceIndicadorApurado = performanceIndicadorApurado as SeguroAutoPerformanceIndicadorApurado;

                        performanceIndicadorApuracoes.Add(new PerformanceIndicadorApuracao
                        {
                            ParentGuid = performanceIndicadorApuracaoImportado.Guid,
                            PerformanceIndicadorId = seguroAutoPerformanceIndicadorApurado.PerformanceIndicadorId,
                            Valor = seguroAutoPerformanceIndicadorApurado.Porcentagem.ToString()
                        });

                        performanceIndicadorDadoApuracoes.AddRange(GetSeguroAutoPerformanceIndicadorDadoApuracoes(performanceIndicadorApuracaoImportado,
                            seguroAutoPerformanceIndicadorApurado, performanceIndicadorDadoPrimaryKeys));
                    }

                    if (performanceIndicadorApurado is SeguroPrestamistaPerformanceIndicadorApurado)
                    {
                        var seguroPrestamistaPerformanceIndicadorApurado = performanceIndicadorApurado as SeguroPrestamistaPerformanceIndicadorApurado;

                        performanceIndicadorApuracoes.Add(new PerformanceIndicadorApuracao
                        {
                            ParentGuid = performanceIndicadorApuracaoImportado.Guid,
                            PerformanceIndicadorId = seguroPrestamistaPerformanceIndicadorApurado.PerformanceIndicadorId,
                            Valor = seguroPrestamistaPerformanceIndicadorApurado.Porcentagem.ToString()
                        });

                        performanceIndicadorDadoApuracoes.AddRange(GetSeguroPrestamistaPerformanceIndicadorDadoApuracoes(performanceIndicadorApuracaoImportado,
                            seguroPrestamistaPerformanceIndicadorApurado, performanceIndicadorDadoPrimaryKeys));
                    }
                }
            }

            publishedData.PerformanceIndicadorApuracoes = performanceIndicadorApuracoes;
            publishedData.PerformanceIndicadorDadoApuracoes = performanceIndicadorDadoApuracoes;
        }

        private static void AtribuirRentabilidadeApuracoes(ArquivoImportado arquivoImportado, PublishedData publishedData)
        {
            var rentabilidadeApuracoes = rentabilidadeIndicadorMongoDBRepository.BuscarRentabilidadeApuracoes(arquivoImportado.Id);

            var rentabilidadeIndicadorApuracoes = new List<RentabilidadeIndicadorApuracao>();
            var rentabilidadeBeneficioApuracoes = new List<RentabilidadeBeneficioApuracao>();

            foreach (var rentabilidadeApuracao in rentabilidadeApuracoes)
            {
                rentabilidadeIndicadorApuracoes.AddRange(GetRentabilidadeIndicadorApuracoes(rentabilidadeApuracao));
                rentabilidadeBeneficioApuracoes.AddRange(GetRentabilidadeBeneficioApuracoes(rentabilidadeApuracao));
            }

            publishedData.RentabilidadeIndicadorApuracoes = rentabilidadeIndicadorApuracoes;
            publishedData.RentabilidadeBeneficioApuracoes = rentabilidadeBeneficioApuracoes;
        }

        #endregion
    }
}
