﻿using maisfidelidade.cnc.imp.core.Common.DependencyInjection;
using System;

namespace maisfidelidade.cnc.imp.core.Infrastructure.Publication
{
	public class Publisher
    {
        public static void Publish(object arquivoId)
        {
            var publishedData = PublicationService.GetPublishedData(arquivoId);

            ProcessPublishedData(publishedData);
        }

        private static void ProcessPublishedData(PublishedData publishedData)
        {
            if (publishedData != null)
            {
                var repository = DependencyResolver.GetService<IPublishedDataRepository>();

                repository.Salvar(publishedData);

                MoverArquivoFisicoParaPastaPublicado(publishedData.Arquivo.Nome);
            }
        }

        private static void MoverArquivoFisicoParaPastaPublicado(string arquivoNome)
        {
            try
            {

            }
            catch (Exception exception)
            {

            }
        }
    }
}
