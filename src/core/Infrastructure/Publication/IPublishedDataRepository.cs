﻿namespace maisfidelidade.cnc.imp.core.Infrastructure.Publication
{
	public interface IPublishedDataRepository
    {
        void Salvar(PublishedData publishedData);
    }
}
