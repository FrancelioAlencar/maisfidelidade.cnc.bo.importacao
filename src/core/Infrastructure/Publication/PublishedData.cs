﻿using maisfidelidade.cnc.imp.core.Domain.Model.Apuracoes;
using maisfidelidade.cnc.imp.core.Domain.Model.Arquivos;
using maisfidelidade.cnc.imp.core.Domain.Model.Banners;
using maisfidelidade.cnc.imp.core.Domain.Model.ClassificacaoIndicadores;
using maisfidelidade.cnc.imp.core.Domain.Model.Empresas;
using maisfidelidade.cnc.imp.core.Domain.Model.Gerentes;
using maisfidelidade.cnc.imp.core.Domain.Model.PerformanceIndicadores;
using maisfidelidade.cnc.imp.core.Domain.Model.RentabilidadeIndicadores;
using System.Collections.Generic;

namespace maisfidelidade.cnc.imp.core.Infrastructure.Publication
{
    public class PublishedData
    {
        public ArquivoPublicado Arquivo { get; set; }
        public IList<Empresa> Empresas { get; set; }
        public IList<Gerente> Gerentes { get; set; }
        public IList<Apuracao> Apuracoes { get; set; }
        public IList<BannerApuracao> BannerApuracoes { get; set; }
        public IList<ClassificacaoIndicadorApuracao> ClassificacaoIndicadorApuracoes { get; set; }
        public IList<PerformanceIndicadorApuracao> PerformanceIndicadorApuracoes { get; set; }
        public IList<PerformanceIndicadorDadoApuracao> PerformanceIndicadorDadoApuracoes { get; set; }
        public IList<RentabilidadeIndicadorApuracao> RentabilidadeIndicadorApuracoes { get; set; }
        public IList<RentabilidadeBeneficioApuracao> RentabilidadeBeneficioApuracoes { get; set; }
    }
}
