﻿namespace maisfidelidade.cnc.imp.core.Infrastructure.DomainBase
{
    /// <summary>
    /// 
    /// </summary>
    public interface IEntity
    {
        object Id { get; set; }
    }
}
