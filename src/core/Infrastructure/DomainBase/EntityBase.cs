﻿namespace maisfidelidade.cnc.imp.core.Infrastructure.DomainBase
{
    public abstract class EntityBase : IEntity
    {
        protected EntityBase()
        {

        }

        public object Id { get; set; }
    }
}
