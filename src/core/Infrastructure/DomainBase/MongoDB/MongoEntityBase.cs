﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace maisfidelidade.cnc.imp.core.Infrastructure.DomainBase.MongoDB
{
    public abstract class MongoEntityBase : IEntity
    {
        protected MongoEntityBase()
        {
            if (Id == null)
            {
                Id = ObjectId.GenerateNewId();
            }
        }

        [BsonId]
        public object Id { get; set; }
    }
}
