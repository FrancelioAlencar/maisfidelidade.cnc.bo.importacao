﻿using maisfidelidade.cnc.imp.core.Infrastructure.DomainBase;

namespace maisfidelidade.cnc.imp.core.Infrastructure.RepositoryFramework
{
    /// <summary>
    ///
    /// </summary>
    /// <typeparam name="TAggregateRoot"></typeparam>
    public interface IRepository<TAggregateRoot>
        where TAggregateRoot : IAggregateRoot
    {

    }
}
