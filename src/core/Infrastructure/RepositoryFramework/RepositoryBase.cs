﻿using maisfidelidade.cnc.imp.core.Infrastructure.DomainBase;

namespace maisfidelidade.cnc.imp.core.Infrastructure.RepositoryFramework
{
    public abstract class RepositoryBase<TAggregateRoot> : IRepository<TAggregateRoot> 
        where TAggregateRoot : IAggregateRoot
    {
        protected RepositoryBase()
        {

        }

        protected virtual void PersistNewItem(IEntity item)
        {
            this.PersistNewItem((TAggregateRoot)item);
        }

        protected virtual void PersistUpdatedItem(IEntity item)
        {
            this.PersistUpdatedItem((TAggregateRoot)item);
        }

        protected virtual void PersistDeletedItem(IEntity item)
        {
            this.PersistDeletedItem((TAggregateRoot)item);
        }

        protected abstract void PersistNewItem(TAggregateRoot item);
        protected abstract void PersistUpdatedItem(TAggregateRoot item);
        protected abstract void PersistDeletedItem(TAggregateRoot item);
    }
}
