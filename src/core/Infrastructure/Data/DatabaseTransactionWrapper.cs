﻿using System;
using System.Data.Common;

namespace maisfidelidade.cnc.imp.core.Infrastructure.Data
{
	public class DatabaseTransactionWrapper : IDisposable
    {
        public DatabaseTransactionWrapper(DbTransaction transaction)
        {

        }


        public void Dispose()
        {
            this.Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                GC.SuppressFinalize(this);
            }
        }
    }
}
