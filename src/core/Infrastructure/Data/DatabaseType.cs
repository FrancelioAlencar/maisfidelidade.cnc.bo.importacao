﻿namespace maisfidelidade.cnc.imp.core.Infrastructure.Data
{
    public enum DatabaseType
    {
        MongoDB,
        MySql,
        SqlServer
    }
}
