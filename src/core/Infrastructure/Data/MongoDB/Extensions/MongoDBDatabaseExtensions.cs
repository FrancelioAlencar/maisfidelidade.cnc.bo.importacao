﻿using maisfidelidade.cnc.imp.core.Infrastructure.DomainBase;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace maisfidelidade.cnc.imp.core.Infrastructure.Data.MongoDB.Extensions
{
    public static class MongoDBDatabaseExtensions
    {
        public static IList<T> Read<T>(this Database database, string collection, Filter filter = null, Projection projection = null)
        {
            var settings = MongoClientSettings.FromUrl(new MongoUrl(database.ConnectionString));

            var client = new MongoClient(settings);

            var wrapper = client.GetDatabase(database.DatabaseName);

            IList<T> result = new List<T>();

            if (filter == null)
            {
                result = wrapper.GetCollection<T>(collection).Find(new BsonDocument()).ToList();
            }
            else
            {
                var filterDefinitions = new List<FilterDefinition<T>>();

                foreach (FilterParameter parameter in filter)
                {
                    if (parameter.Value == null)
                        filterDefinitions.Add(Builders<T>.Filter.Eq(parameter.Field, Builders<BsonDocument>.Filter.Empty));
                    else
                        filterDefinitions.Add(Builders<T>.Filter.Eq(parameter.Field, parameter.Value));
                }

                var collectionFilter = Builders<T>.Filter.And(filterDefinitions);

                if (projection != null && projection.Count > 0)
                {
                    var projectionDefinitions = new List<ProjectionDefinition<T>>();

                    foreach (ProjectionParameter parameter in projection)
                    {
                        switch (parameter.Type)
                        {
                            case ProjectionParameterType.Include:

                                projectionDefinitions.Add(Builders<T>.Projection.Include(parameter.Field));

                                break;
                            case ProjectionParameterType.Exclude:

                                projectionDefinitions.Add(Builders<T>.Projection.Exclude(parameter.Field));

                                break;
                            default:
                                throw new Exception("Unexpected Case");
                        }
                    }

                    var collectionProjection = Builders<T>.Projection.Combine(projectionDefinitions);

                    result = wrapper.GetCollection<T>(collection).Find(collectionFilter).Project<T>(collectionProjection).ToList();
                }
                else
                {
                    result = wrapper.GetCollection<T>(collection).Find(collectionFilter).ToList();
                }
            }

            return result;
        }


        public static void Create<TDocument>(this Database database, TDocument document, string collectionName)
        {
            var settings = MongoClientSettings.FromUrl(new MongoUrl(database.ConnectionString));

            var client = new MongoClient(settings);

            var wrapper = client.GetDatabase(database.DatabaseName);

            wrapper.GetCollection<TDocument>(collectionName).InsertOne(document);
        }

        public static void Update<TDocument>(this Database database, TDocument document, string collectionName)
            where TDocument : IEntity
        {
            var settings = MongoClientSettings.FromUrl(new MongoUrl(database.ConnectionString));

            var client = new MongoClient(settings);

            var wrapper = client.GetDatabase(database.DatabaseName);

            wrapper.GetCollection<TDocument>(collectionName).ReplaceOne(d => d.Id == document.Id, document);
        }

        public static IMongoQueryable<TDocument> GetQueryableCollection<TDocument>(this Database database, string collectionName)
        {
            var settings = MongoClientSettings.FromUrl(new MongoUrl(database.ConnectionString));

            var client = new MongoClient(settings);

            var wrapper = client.GetDatabase(database.DatabaseName);

            IMongoQueryable<TDocument> result;

            result = wrapper.GetCollection<TDocument>(collectionName).AsQueryable();

            return result;
        }
    }
}
