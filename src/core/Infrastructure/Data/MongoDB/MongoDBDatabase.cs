﻿using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;

namespace maisfidelidade.cnc.imp.core.Infrastructure.Data.MongoDB
{
	public class MongoDBDatabase : Database
    {
        public MongoDBDatabase(string connectionString)
            : base(connectionString, DatabaseType.MongoDB)
        {

        }

        #region Métodos Públicos



        //public override void Create<T>(T entity, string query)
        //{
        //    var wrapper = GetOpenConnection();

        //    wrapper.GetCollection<T>(query).InsertOne(entity);
        //}

        public override IList<TDocument> Read<TDocument>(string collection, Filter filter = null)
        {
            var wrapper = GetOpenConnectionMongo();

            IList<TDocument> result;

            if (filter == null)
            {
                result = wrapper.GetCollection<TDocument>(collection).Find(new BsonDocument()).ToList();
            }
            else
            {
                var filterDefinitions = new List<FilterDefinition<TDocument>>();

                foreach (FilterParameter parameter in filter)
                {
                    if (parameter.Value == null)
                        filterDefinitions.Add(Builders<TDocument>.Filter.Eq(parameter.Field, Builders<BsonDocument>.Filter.Empty));
                    else
                        filterDefinitions.Add(Builders<TDocument>.Filter.Eq(parameter.Field, parameter.Value));
                }

                var collectionFilter = Builders<TDocument>.Filter.And(filterDefinitions);

                result = wrapper.GetCollection<TDocument>(collection).Find(collectionFilter).ToList();
            }

            return result;
        }

        //public override void Update<T>(T entity, string query, Filter filter = null)
        //{
        //    var wrapper = GetOpenConnection();

        //    wrapper.GetCollection<T>(query).ReplaceOne(a => a.Id == entity.Id, entity);
        //}

        //public override void Delete<T>(T entity, string query, Filter filter = null)
        //{
        //    throw new NotImplementedException();
        //}

 

 

        #endregion

        #region Métodos Protegidos

        protected virtual IMongoDatabase CreateConnection()
        {
            var settings = MongoClientSettings.FromUrl(new MongoUrl(ConnectionString));

            var client = new MongoClient(settings);

            return client.GetDatabase(DatabaseName);
        }

        protected IMongoDatabase GetOpenConnectionMongo()
        {
            return CreateConnection();
        }

        protected override string GetDatabaseName()
        {
            return new MongoUrl(ConnectionString).DatabaseName;
        }

        #endregion


    }
}
