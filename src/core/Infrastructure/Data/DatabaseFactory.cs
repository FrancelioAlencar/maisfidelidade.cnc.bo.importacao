﻿using maisfidelidade.cnc.imp.core.Common.Utility;
using maisfidelidade.cnc.imp.core.Infrastructure.Data.MongoDB;
using maisfidelidade.cnc.imp.core.Infrastructure.Data.Sql;
using System;

namespace maisfidelidade.cnc.imp.core.Infrastructure.Data
{
    public static class DatabaseFactory
    {
        private static volatile Func<string, DatabaseType, SqlDatabase> createNamedSqlDatabase;
        private static volatile Func<string, DatabaseType, MongoDBDatabase> createNamedMongoDBDatabase;

        public static SqlDatabase CreateSqlDatabase(string name, DatabaseType databaseType)
        {
            return GetCreateSqlDatabase()(name, databaseType);
        }

        public static MongoDBDatabase CreateMongoDBDatabase(string name, DatabaseType databaseType)
        {
            return GetCreateMongoDbDatabase()(name, databaseType);
        }

        public static void SetSqlDatabase(Func<string, DatabaseType, SqlDatabase> createNamedSqlDatabase, bool throwIfSet = true)
        {
            Guard.ArgumentNotNull(createNamedSqlDatabase, nameof(createNamedSqlDatabase));

            var funcCreateNamedSqlDatabase = DatabaseFactory.createNamedSqlDatabase;

            if (funcCreateNamedSqlDatabase != null && throwIfSet)
            {
                throw new InvalidOperationException("Resources.ExceptionDatabaseProviderFactoryAlreadySet");
            }

            DatabaseFactory.createNamedSqlDatabase = createNamedSqlDatabase;
        }

        public static void SetMongoDBDatabase(Func<string, DatabaseType, MongoDBDatabase> createNamedMongoDBDatabase, bool throwIfSet = true)
        {
            Guard.ArgumentNotNull(createNamedMongoDBDatabase, nameof(createNamedMongoDBDatabase));

            var funcCreateNamedMongoDBDatabase = DatabaseFactory.createNamedMongoDBDatabase;

            if (funcCreateNamedMongoDBDatabase != null && throwIfSet)
            {
                throw new InvalidOperationException("Resources.ExceptionDatabaseProviderFactoryAlreadySet");
            }

            DatabaseFactory.createNamedMongoDBDatabase = createNamedMongoDBDatabase;
        }

        private static Func<string, DatabaseType, SqlDatabase> GetCreateSqlDatabase()
        {
            var createNamedSqlDatabase = DatabaseFactory.createNamedSqlDatabase;

            if (createNamedSqlDatabase == null)
            {
                throw new InvalidOperationException("Resources.ExceptionDatabaseProviderFactoryNotSet");
            }

            return createNamedSqlDatabase;
        }

        private static Func<string, DatabaseType, MongoDBDatabase> GetCreateMongoDbDatabase()
        {
            var createNamedMongoDBDatabase = DatabaseFactory.createNamedMongoDBDatabase;

            if (createNamedMongoDBDatabase == null)
            {
                throw new InvalidOperationException("Resources.ExceptionDatabaseProviderFactoryNotSet");
            }

            return createNamedMongoDBDatabase;
        }
    }
}
