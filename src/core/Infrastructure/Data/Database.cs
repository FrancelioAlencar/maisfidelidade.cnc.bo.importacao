﻿using System;
using System.Collections.Generic;

namespace maisfidelidade.cnc.imp.core.Infrastructure.Data
{
	public abstract class Database
    {
        protected Database(string connectionString, DatabaseType databaseType)
        {
            if (string.IsNullOrEmpty(connectionString))
            {
                throw new ArgumentException("ExceptionNullOrEmptyString", nameof(connectionString));
            }

            ConnectionString = connectionString;
            DatabaseType = databaseType;

            DatabaseName = GetDatabaseName();
        }

        #region Propriedades

        public string ConnectionString { get; }
        public DatabaseType DatabaseType { get; }
        public string DatabaseName { get; }

        #endregion




        #region Métodos Protegidos

        protected abstract string GetDatabaseName();




        #endregion

        

        //{
        //    return (TransactionScopeConnections.GetConnection(this) ?? this.GetWrappedConnection());
        //}

        #region Métodos das Operações CRUD

        //public abstract void Create<T>(T entity, string query) where T : IEntity;
        public abstract IList<T> Read<T>(string query, Filter filter = null); //where T : IEntity;
        //public abstract void Update<T>(T entity, string query, Filter filter = null) where T : IEntity;
        //public abstract void Delete<T>(T entity, string query, Filter filter = null) where T : IEntity;

        #endregion
    }
}
