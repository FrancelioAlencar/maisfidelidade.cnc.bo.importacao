﻿using maisfidelidade.cnc.imp.core.Infrastructure.Data.Sql;
using MySql.Data.MySqlClient;

namespace maisfidelidade.cnc.imp.core.Infrastructure.Data.MySql
{
    public class MySqlDatabase : SqlDatabase
    {
        public MySqlDatabase(string connectionString)
            : base(connectionString, MySqlClientFactory.Instance, DatabaseType.MySql)
        {

        }

        protected override string GetDatabaseName()
        {
            return new MySqlConnectionStringBuilder(this.ConnectionString).Database;
        }
    }
}
