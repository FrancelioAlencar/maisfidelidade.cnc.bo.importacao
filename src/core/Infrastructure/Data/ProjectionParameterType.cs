﻿namespace maisfidelidade.cnc.imp.core.Infrastructure.Data
{
    public enum ProjectionParameterType
    {
        Include = 1,
        Exclude = 2
    }
}
