﻿using Dapper;
using System;
using System.Data;

namespace maisfidelidade.cnc.imp.core.Infrastructure.Data
{
    public class MySqlGuidTypeHandler : SqlMapper.TypeHandler<Guid>
    {
        public override Guid Parse(object value)
        {
            if (value is Guid)
                return (Guid)value;

            return new Guid((string)value);
        }

        public override void SetValue(IDbDataParameter parameter, Guid value)
        {
            parameter.Value = value.ToString();
        }
    }
}
