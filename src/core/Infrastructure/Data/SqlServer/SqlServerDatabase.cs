﻿using maisfidelidade.cnc.imp.core.Infrastructure.Data.Sql;
using System.Data.SqlClient;

namespace maisfidelidade.cnc.imp.core.Infrastructure.Data.SqlServer
{
    public class SqlServerDatabase : SqlDatabase
    {
        public SqlServerDatabase(string connectionString)
            : base(connectionString, SqlClientFactory.Instance, DatabaseType.SqlServer)
        {

        }

        protected override string GetDatabaseName()
        {
            throw new System.NotImplementedException();
        }
    }
}
