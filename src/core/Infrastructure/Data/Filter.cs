﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace maisfidelidade.cnc.imp.core.Infrastructure.Data
{
	public class Filter : IEnumerable
    {
        private readonly IList<FilterParameter> parameters;

        public Filter()
        {
            parameters = new List<FilterParameter>();
        }

        public void Add(string field, object value)
        {
            if (parameters.Any(p => p.Field.ToUpper() == field.ToUpper()))
            {
                throw new ArgumentException("O campo já existe na lista de parâmetros do filtro");
            }

            parameters.Add(new FilterParameter(field, value));
        }

        public IEnumerator GetEnumerator()
        {
            foreach (var parameter in parameters)
            {
                yield return parameter;
            }
        }
    }
}
