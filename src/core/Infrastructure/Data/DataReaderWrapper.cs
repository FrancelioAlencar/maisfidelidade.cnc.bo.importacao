﻿using System;
using System.Data;

namespace maisfidelidade.cnc.imp.core.Persistence.Repositories.Data
{
	public abstract class DataReaderWrapper : MarshalByRefObject, IDataReader
    {
        private readonly IDataReader innerReader;

        protected DataReaderWrapper(IDataReader innerReader)
        {
            this.innerReader = innerReader;
        }

        #region Properties

        public IDataReader InnerReader
        {
            get { return this.innerReader; }
        }

        public int FieldCount
        {
            get
            {
                return this.innerReader.FieldCount;
            }
        }

        public int Depth
        {
            get
            {
                return this.innerReader.Depth;
            }
        }

        public bool IsClosed
        {
            get
            {
                return innerReader.IsClosed;
            }
        }

        public int RecordsAffected
        {
            get
            {
                return this.innerReader.RecordsAffected;
            }
        }

        public object this[int i]
        {
            get
            {
                return innerReader[i];
            }
        }

        public object this[string name]
        {
            get
            {
                return innerReader[name];
            }
        }

        #endregion

        public virtual void Close()
        {
            if (!this.innerReader.IsClosed)
            {
                this.innerReader.Close();
            }
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }


        public bool GetBoolean(int i)
        {
            return this.innerReader.GetBoolean(i);
        }

        public byte GetByte(int i)
        {
            return this.innerReader.GetByte(i);
        }

        public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
        {
            return this.innerReader.GetBytes(i, fieldOffset, buffer, bufferoffset, length);
        }

        public char GetChar(int i)
        {
            return this.innerReader.GetChar(i);
        }

        public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
        {
            return this.innerReader.GetChars(i, fieldoffset, buffer, bufferoffset, length);
        }

        public IDataReader GetData(int i)
        {
            return this.innerReader.GetData(i);
        }

        public string GetDataTypeName(int i)
        {
            return this.innerReader.GetDataTypeName(i);
        }

        public DateTime GetDateTime(int i)
        {
            return this.innerReader.GetDateTime(i);
        }

        public decimal GetDecimal(int i)
        {
            return this.innerReader.GetDecimal(i);
        }

        public double GetDouble(int i)
        {
            return this.innerReader.GetDouble(i);
        }

        public Type GetFieldType(int i)
        {
            return this.innerReader.GetFieldType(i);
        }

        public float GetFloat(int i)
        {
            return this.innerReader.GetFloat(i);
        }

        public Guid GetGuid(int i)
        {
            return this.innerReader.GetGuid(i);
        }

        public short GetInt16(int i)
        {
            return this.innerReader.GetInt16(i);
        }

        public int GetInt32(int i)
        {
            return this.innerReader.GetInt32(i);
        }

        public long GetInt64(int i)
        {
            return this.innerReader.GetInt64(i);
        }

        public string GetName(int i)
        {
            return this.innerReader.GetName(i);
        }

        public int GetOrdinal(string name)
        {
            return this.innerReader.GetOrdinal(name);
        }

        public DataTable GetSchemaTable()
        {
            return this.innerReader.GetSchemaTable();
        }

        public string GetString(int i)
        {
            return this.innerReader.GetString(i);
        }

        public object GetValue(int i)
        {
            return this.innerReader.GetValue(i);
        }

        public int GetValues(object[] values)
        {
            return this.innerReader.GetValues(values);
        }

        public bool IsDBNull(int i)
        {
            return this.innerReader.IsDBNull(i);
        }

        public bool NextResult()
        {
            return this.innerReader.NextResult();
        }

        public bool Read()
        {
            return this.innerReader.Read();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing && !this.innerReader.IsClosed)
            {
                this.innerReader.Dispose();
            }
        }
    }
}
