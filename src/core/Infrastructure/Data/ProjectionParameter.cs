﻿namespace maisfidelidade.cnc.imp.core.Infrastructure.Data
{
	public class ProjectionParameter
    {
        public ProjectionParameter(string field, ProjectionParameterType type)
        {
            Field = field;
            Type = type;
        }

        public string Field { get; }
        public ProjectionParameterType Type { get; }
    }
}
