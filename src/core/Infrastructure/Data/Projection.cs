﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace maisfidelidade.cnc.imp.core.Infrastructure.Data
{
	public class Projection : IEnumerable
    {
        private readonly IList<ProjectionParameter> parameters;

        public Projection()
        {
            parameters = new List<ProjectionParameter>();
        }

        public void Add(string field, ProjectionParameterType type)
        {
            if (parameters.Any(p => p.Field.ToUpper() == field.ToUpper()))
            {
                throw new ArgumentException("O campo já existe na lista de parâmetros do filtro");
            }

            parameters.Add(new ProjectionParameter(field, type));
        }

        public IEnumerator GetEnumerator()
        {
            foreach (var parameter in parameters)
            {
                yield return parameter;
            }
        }

        public int Count
        {
            get { return parameters.Count; }
        }
    }
}
