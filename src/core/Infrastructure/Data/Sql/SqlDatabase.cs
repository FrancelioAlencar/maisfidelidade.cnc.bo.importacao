﻿using Dapper;
using maisfidelidade.cnc.imp.core.Persistence.Repositories.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;

namespace maisfidelidade.cnc.imp.core.Infrastructure.Data.Sql
{
    public abstract class SqlDatabase : Database
    {
        private readonly DbProviderFactory dbProviderFactory;

        protected SqlDatabase(string connectionString, DbProviderFactory dbProviderFactory, DatabaseType databaseType)
            : base(connectionString, databaseType)
        {
            this.dbProviderFactory = dbProviderFactory ?? throw new ArgumentNullException(nameof(dbProviderFactory));
        }

        public virtual DbConnection CreateConnection()
        {
            DbConnection connection = this.dbProviderFactory.CreateConnection();
            connection.ConnectionString = this.ConnectionString;
            return connection;
        }

        public DbTransaction BeginTransaction()
        {
            var connection = this.CreateConnection();
            
            connection.Open();
            
            return connection.BeginTransaction();
        }

        internal DbConnection GetNewOpenConnection()
        {
            DbConnection connection = null;
            try
            {
                connection = this.CreateConnection();
                connection.Open();
            }
            catch
            {
                if (connection != null)
                {
                    connection.Close();
                }
                throw;
            }
            return connection;
        }

        protected virtual DatabaseConnectionWrapper GetWrappedConnection()
        {
            return new DatabaseConnectionWrapper(this.GetNewOpenConnection());
        }

        public DatabaseConnectionWrapper GetOpenConnection()
        {
            return (TransactionScopeConnections.ObterConexao(this) ?? this.GetWrappedConnection());
        }

        //public override DatabaseConnectionWrapper GetOpenConnection()
        //{
        //    return (TransactionScopeConnections.ObterConexao(this) ?? this.GetWrappedConnection());
        //}


        //public virtual IList<T> Execute(DbCommand command)
        //{
        //    using (DatabaseConnectionWrapper wrapper = this.GetOpenConnection())
        //    {
        //        var result = command.Connection.Query<T>()

        //        return result;

        //        //PrepareCommand(command, wrapper.Connection);
        //        //IDataReader innerReader = this.DoExecuteReader(command, CommandBehavior.Default);
        //        //return this.CreateWrappedReader(wrapper, innerReader);
        //    }
        //}


        //public override void Create<T>(T entity, string query)
        //{
        //    throw new NotImplementedException();
        //}

        public override IList<T> Read<T>(string query, Filter filter = null)
        {
            using (DatabaseConnectionWrapper wrapper = this.GetOpenConnection())
            {
                var result = wrapper.Connection.Query<T>(query, commandType: CommandType.Text).ToList();

                return result;
            }
        }

        //public override void Update<T>(T entity, string query, Filter filter = null)
        //{
        //    throw new NotImplementedException();
        //}

        //public override void Delete<T>(T entity, string query, Filter filter = null)
        //{
        //    throw new NotImplementedException();
        //}

        public IList<T> ExecuteQuery<T>(CommandType commandType, string query)
        {
            using (DatabaseConnectionWrapper wrapper = this.GetOpenConnection())
            {
                var result = wrapper.Connection.Query<T>(query, commandType: commandType).ToList();

                return result;
            }
        }
    }
}
