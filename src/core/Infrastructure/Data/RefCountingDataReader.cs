﻿using System;
using System.Data;

namespace maisfidelidade.cnc.imp.core.Persistence.Repositories.Data
{
	public class RefCountingDataReader : DataReaderWrapper
    {
        private readonly DatabaseConnectionWrapper connectionWrapper;

        public RefCountingDataReader(DatabaseConnectionWrapper connection, IDataReader innerReader) : base(innerReader)
        {
            if (connection == null)
            {
                throw new ArgumentNullException("connection");
            }

            if (innerReader == null)
            {
                throw new ArgumentNullException("innerReader");
            }

            this.connectionWrapper = connection;

            this.connectionWrapper.AddRef();
        }

        public override void Close()
        {
            if (!this.IsClosed)
            {
                base.Close();
                this.connectionWrapper.Dispose();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && !this.IsClosed)
            {
                base.Dispose(true);
                this.connectionWrapper.Dispose();
            }
        }


    }
}
