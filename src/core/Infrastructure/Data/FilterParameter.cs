﻿namespace maisfidelidade.cnc.imp.core.Infrastructure.Data
{
	public class FilterParameter
    {
        public FilterParameter(string field, object value)
        {
            Field = field;
            Value = value;
        }

        public string Field { get; }
        public object Value { get; }
    }
}
