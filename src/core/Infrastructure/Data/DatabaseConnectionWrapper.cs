﻿using System;
using System.Data.Common;
using System.Threading;

namespace maisfidelidade.cnc.imp.core.Persistence.Repositories.Data
{
    /// <summary>
    /// Esta é uma pequena classe auxiliar usada para gerenciar o fechamento de uma conexão na presença de pool de transações. 
    /// Na verdade, não podemos fechar a conexão até que todos os usem estejam concluídos; portanto, precisamos da contagem de referências.
    /// </summary>
    public class DatabaseConnectionWrapper : IDisposable
    {
        private int refCount;

        public DatabaseConnectionWrapper(DbConnection connection)
        {
            Connection = connection;
            this.refCount = 1;
        }

        public DbConnection Connection { get; private set; }
        public bool IsDisposed { get { return (this.refCount == 0); } }

        public DatabaseConnectionWrapper AddRef()
        {
            Interlocked.Increment(ref this.refCount);
            return this;
        }

        public void Dispose()
        {
            this.Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing && (Interlocked.Decrement(ref refCount) == 0))
            {
                this.Connection.Dispose();
                this.Connection = null;
                GC.SuppressFinalize(this);
            }
        }
    }
}
