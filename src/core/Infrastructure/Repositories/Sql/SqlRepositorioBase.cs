﻿using maisfidelidade.cnc.imp.core.Infrastructure.Data;
using maisfidelidade.cnc.imp.core.Infrastructure.DomainBase;
using maisfidelidade.cnc.imp.core.Infrastructure.RepositoryFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.ComponentModel.DataAnnotations.Schema;
using maisfidelidade.cnc.imp.core.Infrastructure.Data.Sql;

namespace maisfidelidade.cnc.imp.core.Infrastructure.Repositories.Sql
{
	public abstract class SqlRepositorioBase<TAggregateRoot> : RepositoryBase<TAggregateRoot>
        where TAggregateRoot : IAggregateRoot
    {
        public delegate void AppendChildData(IList<TAggregateRoot> entitiesAggregate, object childEntityKeyValue);

        private Dictionary<string, AppendChildData> childCallbacks;

        protected SqlRepositorioBase(DatabaseType databaseType)
        {
            this.Database = DatabaseFactory.CreateSqlDatabase("mysql", databaseType);
            this.childCallbacks = new Dictionary<string, AppendChildData>();
            this.BuildChildCallbacks();
        }

        protected SqlDatabase Database { get; }

        #region Métodos Abstratos


        protected abstract void BuildChildCallbacks();

        #endregion

        #region Métodos Públicos

        //public override IList<T> Obter()
        //{
        //    var queryBuilder = string.Empty;
        //    //queryBuilder.Append(";");

        //    return this.ExecuteQuery(queryBuilder.ToString());
        //}

        #endregion

        #region Métodos Protegidos

        protected IList<TAggregateRoot> ExecuteQuery(string query)
        {
            IList<TAggregateRoot> entidades = new List<TAggregateRoot>();

            entidades = this.Database.Read<TAggregateRoot>(query);

            return entidades;
        }

        protected IList<TEntity> ExecuteQuery<TEntity>(string query)
            //where TEntity : IEntity
        {
            IList<TEntity> entidades = new List<TEntity>();

            entidades = this.Database.Read<TEntity>(query);

            return entidades;
        }

        protected TEntity ExecuteQueryFirst<TEntity>(string query)
        {
            IList<TEntity> entidades = new List<TEntity>();

            entidades = this.Database.Read<TEntity>(query);

            return entidades.FirstOrDefault();
        }

        protected virtual void BuildChildEntities(IList<TAggregateRoot> parentEntities)
        {
            if (this.childCallbacks != null && this.childCallbacks.Count > 0)
            {
                foreach (string childKeyName in this.childCallbacks.Keys)
                {
                    this.childCallbacks[childKeyName](parentEntities, null);
                }
            }
        }

        protected virtual IList<TAggregateRoot> BuildEntitiesFromSql(string sql)
        {
            var entities = new List<TAggregateRoot>();

            entities.AddRange(this.ExecuteQuery(sql));

            if (entities.Count > 0)
                BuildChildEntities(entities);

            return entities;
        }

        protected IList<string> GetSqlInBatches<T>(string table, IList<T> entities)
        {
            var columnAttributes = typeof(T).GetProperties().Where(p => p.GetCustomAttribute(typeof(ColumnAttribute)) != null).Select(p => p.GetCustomAttribute(typeof(ColumnAttribute)) as ColumnAttribute);
            
            var insertSql = $"INSERT INTO {table} ({string.Join(", ", columnAttributes.Select(c => c.Name))}) VALUES ";
            var valuesSql = $"({string.Join(", ", columnAttributes.Select(c => $"@{c.Name}"))})";
            var batchSize = 1000;

            var sqlsToExecute = new List<string>();

            var numberOfBatches = (int)Math.Ceiling((double)entities.Count / batchSize);

            for (int i = 0; i < numberOfBatches; i++)
            {
                var entitiesToInsert = entities.Skip(i * batchSize).Take(batchSize);

                var valuesToInsertList = new List<string>();

                foreach (var entity in entitiesToInsert)
                {
                    var valuesToInsert = valuesSql;

                    foreach (var propertyInfo in entity.GetType().GetProperties())
                    {
                        var attribute = propertyInfo.GetCustomAttribute(typeof(ColumnAttribute));

                        if (attribute != null)
                        {
                            var columnAttribute = attribute as ColumnAttribute;

                            switch (Type.GetTypeCode(propertyInfo.PropertyType))
                            {
                                case TypeCode.String:
                                    valuesToInsert = valuesToInsert.Replace($"@{columnAttribute.Name}", $"'{propertyInfo.GetValue(entity)}'");
                                    break;
                                case TypeCode.Int16:
                                case TypeCode.Int32:
                                case TypeCode.Int64:
                                    valuesToInsert = valuesToInsert.Replace($"@{columnAttribute.Name}", $"{propertyInfo.GetValue(entity)}");
                                    break;
                                case TypeCode.Single:
                                case TypeCode.Double:
                                case TypeCode.Decimal:
                                    valuesToInsert = valuesToInsert.Replace($"@{columnAttribute.Name}", $"{propertyInfo.GetValue(entity)}");
                                    break;
                                case TypeCode.DateTime:
                                    valuesToInsert = valuesToInsert.Replace($"@{columnAttribute.Name}", $"{Convert.ToDateTime(propertyInfo.GetValue(entity)).ToString("yyyy-MM-dd hh:mm:ss")}");
                                    break;
                                default:
                                    throw new Exception("Unexpected Case");
                            }
                        }
                    }

                    valuesToInsertList.Add(valuesToInsert);
                }

                sqlsToExecute.Add(insertSql + string.Join(", ", valuesToInsertList));
            }

            //for (int i = 0; i < numberOfBatches; i++)
            //{
            //    var userToInsert = entities.Skip(i * batchSize).Take(batchSize);
            //    var valuesToInsert = userToInsert.Select(u => string.Format(valuesSql, u));
            //    sqlsToExecute.Add(insertSql + string.Join(',', valuesToInsert));
            //}

            return sqlsToExecute;
        }

        #endregion

        protected override void PersistNewItem(TAggregateRoot item)
        {
            throw new System.NotImplementedException();
        }

        protected override void PersistUpdatedItem(TAggregateRoot item)
        {
            throw new System.NotImplementedException();
        }

        protected override void PersistDeletedItem(TAggregateRoot item)
        {
            throw new System.NotImplementedException();
        }

    }
}
