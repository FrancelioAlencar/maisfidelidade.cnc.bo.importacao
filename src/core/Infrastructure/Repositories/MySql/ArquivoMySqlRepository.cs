﻿using maisfidelidade.cnc.imp.core.Domain.Model.Arquivos;
using maisfidelidade.cnc.imp.core.Infrastructure.Repositories.MySql.Base;
using System.Text;

namespace maisfidelidade.cnc.imp.core.Infrastructure.Repositories.MySql
{
    public class ArquivoMySqlRepository : MySqlRepositoryBase<ArquivoPublicado>, IArquivoMySqlRepository
    {
        public ArquivoMySqlRepository()
        {

        }

        protected override void BuildChildCallbacks()
        {

        }

        #region IArquivoMySqlRepository Membros

        public bool ExisteArquivo(string arquivoNome)
        {
            var queryBuilder = new StringBuilder();

            queryBuilder.Append($"SELECT EXISTS(SELECT * FROM tb_arquivo WHERE nome = '{arquivoNome}') AS Existe");

            var existe = this.ExecuteQueryFirst<bool>(queryBuilder.ToString());

            return existe;
        }

        #endregion
    }
}
