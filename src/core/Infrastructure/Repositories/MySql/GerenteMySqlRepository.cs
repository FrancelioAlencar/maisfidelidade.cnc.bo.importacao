﻿using maisfidelidade.cnc.imp.core.Domain.Model.Gerentes;
using maisfidelidade.cnc.imp.core.Infrastructure.Repositories.MySql.Base;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.imp.core.Infrastructure.Repositories.MySql
{
	public class GerenteMySqlRepository : MySqlRepositoryBase<Gerente>, IGerenteMySqlRepository
    {
        protected override void BuildChildCallbacks()
        {

        }

        #region IGerenteMySqlRepository Membros

        public IList<Gerente> ObterTodosGerentes()
        {
            var queryBuilder = new StringBuilder();

            queryBuilder.Append("SELECT id_gerente AS Id, matricula AS Matricula, nome AS Nome, email AS Email, telefone AS Telefone, id_tipo_gerente AS TipoGerenteId");
            queryBuilder.Append(" FROM tb_gerente");

            return BuildEntitiesFromSql(queryBuilder.ToString());
        }

        #endregion
    }
}
