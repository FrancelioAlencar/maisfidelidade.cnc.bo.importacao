﻿using maisfidelidade.cnc.imp.core.Domain.Model.PerformanceIndicadores;
using maisfidelidade.cnc.imp.core.Infrastructure.Repositories.MySql.Base;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.imp.core.Infrastructure.Repositories.MySql
{
	public class PerformanceIndicadorMySqlRepository : MySqlRepositoryBase<PerformanceIndicador>, IPerformanceIndicadorMySqlRepository
    {
        protected override void BuildChildCallbacks()
        {

        }

        #region IPerformanceIndicadorMySqlRepository Membros

        public IList<PerformanceIndicador> ObterPerformanceIndicadores()
        {
            var queryBuilder = new StringBuilder();

            queryBuilder.Append("SELECT id_indicador_performance AS Id, id_entidade AS EntidadeId, id_tipo_indicador_performance AS TipoPerformanceIndicadorId, nome AS Nome, descricao AS Descricao, tooltip AS Tooltip");
            queryBuilder.Append(" FROM tb_indicador_performance");
            queryBuilder.Append(" WHERE ativo = 1");

            return BuildEntitiesFromSql(queryBuilder.ToString());
        }

        public IList<PerformanceIndicadorDado> ObterPerformanceIndicadorDados()
        {
            var queryBuilder = new StringBuilder();

            queryBuilder.Append("SELECT id_indicador_performance_dado AS Id, id_entidade AS EntidadeId, id_indicador_performance AS PerformanceIndicadorId, nome AS Nome, descricao AS Descricao, tooltip AS Tooltip");
            queryBuilder.Append(" FROM tb_indicador_performance_dado");
            queryBuilder.Append(" WHERE ativo = 1");

            return ExecuteQuery<PerformanceIndicadorDado>(queryBuilder.ToString());
        }

        #endregion

    }
}
