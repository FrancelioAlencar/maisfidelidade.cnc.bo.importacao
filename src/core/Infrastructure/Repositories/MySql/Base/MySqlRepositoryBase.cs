﻿using maisfidelidade.cnc.imp.core.Infrastructure.Data;
using maisfidelidade.cnc.imp.core.Infrastructure.DomainBase;
using maisfidelidade.cnc.imp.core.Infrastructure.Repositories.Sql;
using System;

namespace maisfidelidade.cnc.imp.core.Infrastructure.Repositories.MySql.Base
{
    public abstract class MySqlRepositoryBase<TAggregateRoot> : SqlRepositorioBase<TAggregateRoot> 
        where TAggregateRoot : IAggregateRoot
    {
        protected MySqlRepositoryBase()
            : base(DatabaseType.MySql)
        {
            if (Database.DatabaseType != DatabaseType.MySql)
            {
                throw new InvalidOperationException("O objeto atribuído a propriedade Database não é do tipo MySqlDatabase");
            }
        }
    }
}
