﻿using maisfidelidade.cnc.imp.core.Domain.Model.Empresas;
using maisfidelidade.cnc.imp.core.Infrastructure.Repositories.MySql.Base;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.imp.core.Infrastructure.Repositories.MySql
{
	public class EmpresaMySqlRepository : MySqlRepositoryBase<Empresa>, IEmpresaMySqlRepository
    {
        protected override void BuildChildCallbacks()
        {

        }

        #region IEmpresaMySqlRepository Membros

        public IList<Empresa> ObterEmpresas()
        {
            var queryBuilder = new StringBuilder();

            queryBuilder.Append("SELECT id_empresa AS Id, cnpj AS Cnpj, nome_fantasia AS NomeFantasia, nome_grupo AS NomeGrupo, email AS Email, id_segmento AS SegmentoId, matricula_gr AS MatriculaGR, matricula_gc AS MatriculaGC");
            queryBuilder.Append(" FROM tb_empresa");

            return BuildEntitiesFromSql(queryBuilder.ToString());
        }

        #endregion
    }
}
