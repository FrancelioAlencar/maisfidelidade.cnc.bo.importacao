﻿using maisfidelidade.cnc.imp.core.Domain.Model.RentabilidadeIndicadores;
using maisfidelidade.cnc.imp.core.Domain.Model.RentabilidadeIndicadores;
using maisfidelidade.cnc.imp.core.Infrastructure.Repositories.MySql.Base;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.imp.core.Infrastructure.Repositories.MySql
{
	public class RentabilidadeIndicadorMySqlRepository : MySqlRepositoryBase<RentabilidadeIndicador>, IRentabilidadeIndicadorMySqlRepository
	{
		protected override void BuildChildCallbacks()
		{

		}

		#region IRentabilidadeIndicadorMySqlRepository Membros

		public IList<RentabilidadeIndicador> ObterRentabilidadeIndicadores()
		{
			var queryBuilder = new StringBuilder();

			queryBuilder.Append("SELECT id_indicador_rentabilidade AS Id, id_entidade AS EntidadeId, nome AS Nome");
			queryBuilder.Append(" FROM tb_indicador_rentabilidade WHERE ativo = 1");

			return BuildEntitiesFromSql(queryBuilder.ToString());
		}
		public IList<RentabilidadeBeneficio> ObterRentabilidadeBeneficios()
		{
			var queryBuilder = new StringBuilder();

			queryBuilder.Append("SELECT id_beneficio_rentabilidade AS Id, id_entidade AS EntidadeId, nome AS Nome");
			queryBuilder.Append(" FROM tb_beneficio_rentabilidade WHERE ativo = 1");

			return ExecuteQuery<RentabilidadeBeneficio>(queryBuilder.ToString());
		}
		#endregion
	}
}
