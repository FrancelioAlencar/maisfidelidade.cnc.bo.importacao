﻿using maisfidelidade.cnc.imp.core.Domain.Model.Banners;
using maisfidelidade.cnc.imp.core.Infrastructure.Repositories.MySql.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.imp.core.Infrastructure.Repositories.MySql
{
	public class BannerMySqlRepository : MySqlRepositoryBase<Banner>, IBannerMySqlRepository
	{
		protected override void BuildChildCallbacks()
		{

		}

		#region IRentabilidadeIndicadorMySqlRepository Membros

		public IList<Banner> ObterBanners(DateTime? dataReferencia = default)
		{
			var queryBuilder = new StringBuilder();

			queryBuilder.Append("SELECT id_banner AS Id, id_entidade AS EntidadeId, nome AS Nome,prefixo_mongo AS Prefixo, id_tipo AS TipoBanner ");
			queryBuilder.Append(" FROM tb_banner WHERE ativo = 1");
			queryBuilder.Append($" and '{dataReferencia.Value.ToString("yyyy-MM-dd HH:mm:ss")}' BETWEEN dt_inicio AND dt_fim ");

			return BuildEntitiesFromSql(queryBuilder.ToString());
		}
		#endregion
	}
}
