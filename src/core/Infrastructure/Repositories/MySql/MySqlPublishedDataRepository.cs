﻿using Dapper;
using maisfidelidade.cnc.imp.core.Domain.Model.Apuracoes;
using maisfidelidade.cnc.imp.core.Domain.Model.Arquivos;
using maisfidelidade.cnc.imp.core.Domain.Model.ClassificacaoIndicadores;
using maisfidelidade.cnc.imp.core.Domain.Model.Empresas;
using maisfidelidade.cnc.imp.core.Domain.Model.Gerentes;
using maisfidelidade.cnc.imp.core.Domain.Model.PerformanceIndicadores;
using maisfidelidade.cnc.imp.core.Domain.Model.RentabilidadeIndicadores;
using maisfidelidade.cnc.imp.core.Infrastructure.Data;
using maisfidelidade.cnc.imp.core.Infrastructure.Data.Sql;
using maisfidelidade.cnc.imp.core.Infrastructure.Publication;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Transactions;

namespace maisfidelidade.cnc.imp.core.Infrastructure.Repositories.MySql
{
    public class MySqlPublishedDataRepository : IPublishedDataRepository
    {
        protected SqlDatabase Database { get; }

        public MySqlPublishedDataRepository()
        {
            this.Database = DatabaseFactory.CreateSqlDatabase("mysql", DatabaseType.MySql);

            SqlMapper.AddTypeHandler(new MySqlGuidTypeHandler());
        }

        #region IPublishedDataRepository Membros

        public void Salvar(PublishedData publishedData)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    using (DbConnection connection = this.Database.CreateConnection())
                    {
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();

                        var arquivoPublicadoId = SalvarArquivo(publishedData.Arquivo, connection);

                        SalvarGerentes(publishedData.Gerentes, connection);
                        SalvarEmpresas(publishedData.Empresas, connection);

                        AtribuirForeignKeysParaApuracoes(arquivoPublicadoId, publishedData, connection);

                        SalvarApuracoes(publishedData.Apuracoes, connection);

                        AtribuirForeignKeysParaApuracoesComplementares(arquivoPublicadoId, publishedData, connection);

                        SalvarClassificacaoIndicadorApuracoes(publishedData.ClassificacaoIndicadorApuracoes, connection);
                        SalvarPerformanceIndicadorApuracoes(publishedData.PerformanceIndicadorApuracoes, connection);
                        SalvarRentabilidadeIndicadorApuracoes(publishedData.RentabilidadeIndicadorApuracoes, connection);
                        SalvarRentabilidadeBeneficioApuracoes(publishedData.RentabilidadeBeneficioApuracoes, connection);

                        AtribuirForeignKeysParaPerformanceIndicadorApuracaoDados(arquivoPublicadoId, publishedData, connection);

                        SalvarPerformanceIndicadorDadoApuracoes(publishedData.PerformanceIndicadorDadoApuracoes, connection);

                        scope.Complete();
                    }
                }
            }
            catch (Exception exception)
            {
                throw new Exception("some reason to rethrow", exception);
            }
        }



        #endregion


        #region Métodos Privados

        private static IList<string> GetSqlInBatches<T>(string tableName, IList<T> entities)
        {
            var columnAttributes = typeof(T).GetProperties().Where(p => p.GetCustomAttribute(typeof(ColumnAttribute)) != null).Select(p => p.GetCustomAttribute(typeof(ColumnAttribute)) as ColumnAttribute);

            var insertSql = $"INSERT INTO {tableName} ({string.Join(", ", columnAttributes.Select(c => c.Name))}) VALUES ";
            var valuesSql = $"({string.Join(", ", columnAttributes.Select(c => $"@{c.Name}"))})";
            var batchSize = 1000;

            var sqlsToExecute = new List<string>();

            var numberOfBatches = (int)Math.Ceiling((double)entities.Count / batchSize);

            for (int i = 0; i < numberOfBatches; i++)
            {
                var entitiesToInsert = entities.Skip(i * batchSize).Take(batchSize);

                var valuesToInsertList = new List<string>();

                foreach (var entity in entitiesToInsert)
                {
                    var valuesToInsert = valuesSql;

                    foreach (var propertyInfo in entity.GetType().GetProperties())
                    {
                        var attribute = propertyInfo.GetCustomAttribute(typeof(ColumnAttribute));

                        if (attribute != null)
                        {
                            var columnAttribute = attribute as ColumnAttribute;

                            switch (Type.GetTypeCode(propertyInfo.PropertyType))
                            {
                                case TypeCode.String:
                                    valuesToInsert = valuesToInsert.Replace($"@{columnAttribute.Name}", $"'{propertyInfo.GetValue(entity)}'");
                                    break;
                                case TypeCode.Int16:
                                case TypeCode.Int32:
                                case TypeCode.Int64:
                                    valuesToInsert = valuesToInsert.Replace($"@{columnAttribute.Name}", $"{propertyInfo.GetValue(entity)}");
                                    break;
                                case TypeCode.Single:
                                case TypeCode.Double:
                                case TypeCode.Decimal:
                                    valuesToInsert = valuesToInsert.Replace($"@{columnAttribute.Name}", $"{propertyInfo.GetValue(entity).ToString().Replace(",", ".")}");
                                    break;
                                case TypeCode.DateTime:
                                    valuesToInsert = valuesToInsert.Replace($"@{columnAttribute.Name}", $"{Convert.ToDateTime(propertyInfo.GetValue(entity)).ToString("yyyy-MM-dd hh:mm:ss")}");
                                    break;
                                case TypeCode.Boolean:
                                    valuesToInsert = valuesToInsert.Replace($"@{columnAttribute.Name}", $"{Convert.ToBoolean(propertyInfo.GetValue(entity))}");
                                    break;
                                case TypeCode.Object:
                                    var value = propertyInfo.GetValue(entity);

                                    if (value is Guid)
                                    {
                                        valuesToInsert = valuesToInsert.Replace($"@{columnAttribute.Name}", $"'{propertyInfo.GetValue(entity).ToString()}'");
                                    }
                                    break;
                                default:
                                    throw new Exception("Unexpected Case");
                            }
                        }
                    }

                    valuesToInsertList.Add(valuesToInsert);
                }

                sqlsToExecute.Add(insertSql + string.Join(", ", valuesToInsertList));
            }

            return sqlsToExecute;
        }

        private int SalvarArquivo(ArquivoPublicado arquivoPublicado, DbConnection connection)
        {
            try
            {
                var queryBuilder = new StringBuilder();

                queryBuilder.Append("UPDATE tb_arquivo SET ultimo_processado = 0 WHERE ultimo_processado = 1;");
                queryBuilder.Append(" INSERT INTO tb_arquivo (id_arquivo_stage, nome, id_tipo_arquivo, dt_processamento, dt_referencia, dt_previsao_atualizacao, dt_importacao, dt_publicacao, total_linhas, ultimo_processado)");
                queryBuilder.Append($" VALUES('{arquivoPublicado.StageArquivoId}', '{arquivoPublicado.Nome}', {arquivoPublicado.TipoArquivoId}, '{arquivoPublicado.DataProcessamento.ToString("yyyy-MM-dd")}', '{arquivoPublicado.DataReferencia.ToString("yyyy-MM-dd")}',");
                queryBuilder.Append($" '{arquivoPublicado.DataPrevisaoAtualizacao.ToString("yyyy-MM-dd")}', '{arquivoPublicado.DataImportacao.ToString("yyyy-MM-dd")}', '{arquivoPublicado.DataPublicacao.ToString("yyyy-MM-dd")}', {arquivoPublicado.TotalLinhas}, 1);");
                queryBuilder.Append(" SELECT LAST_INSERT_ID();");

                return connection.ExecuteScalar<int>(queryBuilder.ToString(), commandType: CommandType.Text);
            }
            catch (Exception exception)
            {
                throw new Exception("some reason to rethrow", exception);
            }
        }

        private static void SalvarEmpresas(IList<Empresa> empresas, DbConnection connection)
        {
            try
            {
                var sqlBatches = GetSqlInBatches<Empresa>("tb_empresa", empresas);

                foreach (var sql in sqlBatches)
                {
                    connection.Execute(sql, commandType: CommandType.Text);
                }
            }
            catch (Exception exception)
            {
                throw new Exception("some reason to rethrow", exception);
            }
        }

        private static void SalvarGerentes(IList<Gerente> gerentes, DbConnection connection)
        {
            try
            {
                var sqlBatches = GetSqlInBatches<Gerente>("tb_gerente", gerentes);

                foreach (var sql in sqlBatches)
                {
                    connection.Execute(sql, commandType: CommandType.Text);
                }
            }
            catch (Exception exception)
            {
                throw new Exception("some reason to rethrow", exception);
            }
        }

        private static void SalvarApuracoes(IList<Apuracao> apuracoes, DbConnection connection)
        {
            try
            {
                var sqlBatches = GetSqlInBatches<Apuracao>("tb_apuracao", apuracoes);

                foreach (var sql in sqlBatches)
                {
                    connection.Execute(sql, commandType: CommandType.Text);
                }
            }
            catch (Exception exception)
            {
                throw new Exception("some reason to rethrow", exception);
            }

        }

        private static void SalvarClassificacaoIndicadorApuracoes(IList<ClassificacaoIndicadorApuracao> classificacaoIndicadorApuracoes, DbConnection connection)
        {
            try
            {
                var sqlBatches = GetSqlInBatches<ClassificacaoIndicadorApuracao>("tb_apuracao_indicador_classificacao", classificacaoIndicadorApuracoes);

                foreach (var sql in sqlBatches)
                {
                    connection.Execute(sql, commandType: CommandType.Text);
                }
            }
            catch (Exception exception)
            {
                throw new Exception("some reason to rethrow", exception);
            }
        }

        private static void SalvarPerformanceIndicadorApuracoes(IList<PerformanceIndicadorApuracao> performanceIndicadorApuracoes, DbConnection connection)
        {
            try
            {
                var sqlBatches = GetSqlInBatches<PerformanceIndicadorApuracao>("tb_apuracao_indicador_performance", performanceIndicadorApuracoes);

                foreach (var sql in sqlBatches)
                {
                    connection.Execute(sql, commandType: CommandType.Text);
                }
            }
            catch (Exception exception)
            {
                throw new Exception("some reason to rethrow", exception);
            }
        }

        private static void SalvarPerformanceIndicadorDadoApuracoes(IList<PerformanceIndicadorDadoApuracao> performanceIndicadorDadoApuracoes, DbConnection connection)
        {
            try
            {
                var sqlBatches = GetSqlInBatches<PerformanceIndicadorDadoApuracao>("tb_apuracao_indicador_performance_dado", performanceIndicadorDadoApuracoes);

                foreach (var sql in sqlBatches)
                {
                    connection.Execute(sql, commandType: CommandType.Text);
                }
            }
            catch (Exception exception)
            {
                throw new Exception("some reason to rethrow", exception);
            }
        }

        private static void SalvarRentabilidadeIndicadorApuracoes(IList<RentabilidadeIndicadorApuracao> rentabilidadeIndicadorApuracoes, DbConnection connection)
        {
            try
            {
                var sqlBatches = GetSqlInBatches<RentabilidadeIndicadorApuracao>("tb_apuracao_indicador_rentabilidade", rentabilidadeIndicadorApuracoes);

                foreach (var sql in sqlBatches)
                {
                    connection.Execute(sql, commandType: CommandType.Text);
                }
            }
            catch (Exception exception)
            {
                throw new Exception("some reason to rethrow", exception);
            }
        }

        private static void SalvarRentabilidadeBeneficioApuracoes(IList<RentabilidadeBeneficioApuracao> rentabilidadeBeneficioApuracoes, DbConnection connection)
        {
            try
            {
                var sqlBatches = GetSqlInBatches<RentabilidadeBeneficioApuracao>("tb_apuracao_beneficio_rentabilidade", rentabilidadeBeneficioApuracoes);

                foreach (var sql in sqlBatches)
                {
                    connection.Execute(sql, commandType: CommandType.Text);
                }
            }
            catch (Exception exception)
            {
                throw new Exception("some reason to rethrow", exception);
            }
        }

        private static void AtribuirForeignKeysParaApuracoes(int arquivoPublicadoId, PublishedData publishedData, DbConnection connection)
        {
            var empresaKeys = ObterEmpresaKeys(connection);

            publishedData.Apuracoes = publishedData.Apuracoes.Select(a =>
            {
                a.ArquivoId = arquivoPublicadoId;
                a.EmpresaId = empresaKeys.FirstOrDefault(e => e.Cnpj.ToUpper().Equals(a.Cnpj.ToUpper())).EmpresaId;
                return a;
            }).ToList();
        }

        private static void AtribuirForeignKeysParaApuracoesComplementares(int arquivoPublicadoId, PublishedData publishedData, DbConnection connection)
        {
            var apuracaoKeys = ObterApuracaoKeys(arquivoPublicadoId, connection);

            publishedData.ClassificacaoIndicadorApuracoes = publishedData.ClassificacaoIndicadorApuracoes.Select(a =>
            {
                a.ApuracaoId = apuracaoKeys.FirstOrDefault(e => e.Guid.Equals(a.Guid)).ApuracaoId;
                return a;
            }).ToList();
            publishedData.PerformanceIndicadorApuracoes = publishedData.PerformanceIndicadorApuracoes.Select(a =>
            {
                a.ApuracaoId = apuracaoKeys.FirstOrDefault(e => e.Guid.Equals(a.ParentGuid)).ApuracaoId;
                return a;
            }).ToList();
            publishedData.RentabilidadeIndicadorApuracoes = publishedData.RentabilidadeIndicadorApuracoes.Select(a =>
            {
                a.ApuracaoId = apuracaoKeys.FirstOrDefault(e => e.Guid.Equals(a.Guid)).ApuracaoId;
                return a;
            }).ToList();
            publishedData.RentabilidadeBeneficioApuracoes = publishedData.RentabilidadeBeneficioApuracoes.Select(a =>
            {
                a.ApuracaoId = apuracaoKeys.FirstOrDefault(e => e.Guid.Equals(a.Guid)).ApuracaoId;
                return a;
            }).ToList();
        }

        private static void AtribuirForeignKeysParaPerformanceIndicadorApuracaoDados(int arquivoPublicadoId, PublishedData publishedData, DbConnection connection)
        {
            var performanceIndicadorApuracaoKeys = ObterPerformanceIndicadorApuracaoKeys(arquivoPublicadoId, connection);

            publishedData.PerformanceIndicadorDadoApuracoes = publishedData.PerformanceIndicadorDadoApuracoes.Select(a =>
            {
                a.PerformanceIndicadorApuracaoId = performanceIndicadorApuracaoKeys.FirstOrDefault(e => e.ApuracaoGuid == a.ApuracaoGuid
                    && e.PerformanceIndicadorId == a.PerformanceIndicadorId).PerformanceIndicadorApuracaoId;
                return a;
            }).ToList();
        }

        private static IList<EmpresaKeys> ObterEmpresaKeys(DbConnection connection)
        {
            var queryBuilder = new StringBuilder();

            queryBuilder.Append("SELECT id_empresa AS EmpresaId, cnpj AS Cnpj");
            queryBuilder.Append(" FROM tb_empresa");

            return connection.Query<EmpresaKeys>(queryBuilder.ToString(), commandType: CommandType.Text).ToList();
        }

        private static IList<ApuracaoKeys> ObterApuracaoKeys(int arquivoPublicadoId, DbConnection connection)
        {
            var queryBuilder = new StringBuilder();

            queryBuilder.Append("SELECT id_apuracao AS ApuracaoId, guid AS Guid");
            queryBuilder.Append(" FROM tb_apuracao");
            queryBuilder.Append($" WHERE id_arquivo = {arquivoPublicadoId}");

            return connection.Query<ApuracaoKeys>(queryBuilder.ToString(), commandType: CommandType.Text).ToList();
        }

        private static IList<PerformanceIndicadorApuracaoKeys> ObterPerformanceIndicadorApuracaoKeys(int arquivoPublicadoId, DbConnection connection)
        {
            var queryBuilder = new StringBuilder();

            queryBuilder.Append("SELECT per.id_apuracao_indicador_performance AS PerformanceIndicadorApuracaoId, per.id_indicador_performance AS PerformanceIndicadorId, apu.guid AS ApuracaoGuid");
            queryBuilder.Append(" FROM tb_apuracao_indicador_performance per");
            queryBuilder.Append(" INNER JOIN tb_apuracao apu on apu.id_apuracao = per.id_apuracao");
            queryBuilder.Append($" WHERE apu.id_arquivo = {arquivoPublicadoId};");

            return connection.Query<PerformanceIndicadorApuracaoKeys>(queryBuilder.ToString(), commandType: CommandType.Text).ToList();

        }

        #endregion
    }
}
