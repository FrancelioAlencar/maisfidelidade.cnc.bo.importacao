﻿using maisfidelidade.cnc.imp.core.Domain.Model.Usuarios;
using maisfidelidade.cnc.imp.core.Infrastructure.Repositories.MySql.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.imp.core.Infrastructure.Repositories.MySql
{
    public class UsuarioMySqlRepository : MySqlRepositoryBase<Usuario>, IUsuarioMySqlRepository
    {
        protected override void BuildChildCallbacks()
        {

        }

        #region IUsuarioRepository Membros

        public IList<Usuario> ObterUsuariosGC()
        {
            IList<Usuario> usuarios = new List<Usuario>();

            usuarios = ObterUsuarioPorPerfil(PerfilEnum.GC);

            return usuarios;
        }

        public IList<Usuario> ObterUsuariosGR()
        {
            IList<Usuario> usuarios = new List<Usuario>();

            usuarios = ObterUsuarioPorPerfil(PerfilEnum.GR);

            return usuarios;
        }

        #endregion

        #region Métodos Privados

        private IList<Usuario> ObterUsuarioPorPerfil(PerfilEnum perfil)
        {
            var usuarios = new List<Usuario>();

            StringBuilder queryBuilder = new StringBuilder();

            queryBuilder.Append("SELECT u.id_usuario AS Id, u.nome AS Nome, u.email AS Email ");
            queryBuilder.Append(" FROM tb_usuario u");
            queryBuilder.Append(" INNER JOIN tb_usuario_perfil up ON up.id_usuario == u.id_usuario");
            queryBuilder.Append(" INNER JOIN tb_perfil p ON p.id_perfil == up.id_perfil");
            queryBuilder.Append($" WHERE p.id_perfil = {perfil};");

            return usuarios;
        }

        #endregion
    }
}
