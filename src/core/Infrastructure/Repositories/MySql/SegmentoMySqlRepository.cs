﻿using maisfidelidade.cnc.imp.core.Domain.Model.Segmentos;
using maisfidelidade.cnc.imp.core.Infrastructure.Repositories.MySql.Base;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.imp.core.Infrastructure.Repositories.MySql
{
	public class SegmentoMySqlRepository : MySqlRepositoryBase<Segmento>, ISegmentoMySqlRepository
    {
        protected override void BuildChildCallbacks()
        {

        }

        #region ISegmentoMySqlRepository Métodos

        public IList<Segmento> ObterSegmentos()
        {
            var queryBuilder = new StringBuilder();

            queryBuilder.Append("SELECT id_segmento AS Id, nome AS Nome, codigo_santander AS CodigoSantander");
            queryBuilder.Append(" FROM tb_segmento");
            queryBuilder.Append(" WHERE ativo = 1");

            return BuildEntitiesFromSql(queryBuilder.ToString());
        }

        #endregion
    }
}
