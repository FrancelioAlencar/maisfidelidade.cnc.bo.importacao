﻿using maisfidelidade.cnc.imp.core.Domain.Model.Categorias;
using maisfidelidade.cnc.imp.core.Infrastructure.Repositories.MySql.Base;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.imp.core.Infrastructure.Repositories.MySql
{
    public class CategoriaMySqlRepository : MySqlRepositoryBase<Categoria>, ICategoriaMySqlRepository
    {
        protected override void BuildChildCallbacks()
        {

        }

        #region ICategoriaMySqlRepository Membros

        public IList<Categoria> ObterCategorias()
        {
            var queryBuilder = new StringBuilder();

            queryBuilder.Append("SELECT id_categoria AS Id, nome AS Nome, descricao AS Descricao");
            queryBuilder.Append(" FROM tb_categoria");
            queryBuilder.Append(" WHERE ativo = 1");

            return BuildEntitiesFromSql(queryBuilder.ToString());
        }

        #endregion


    }
}
