﻿using maisfidelidade.cnc.imp.core.Domain.Model.ClassificacaoIndicadores;
using maisfidelidade.cnc.imp.core.Infrastructure.Repositories.MySql.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.imp.core.Infrastructure.Repositories.MySql
{
	public class ClassificacaoIndicadorMySqlRepository : MySqlRepositoryBase<ClassificacaoIndicador>, IClassificacaoIndicadorMySqlRepository
	{
		protected override void BuildChildCallbacks()
		{

		}

		#region IRentabilidadeIndicadorMySqlRepository Membros

		public IList<ClassificacaoIndicador> ObterClassificacaoIndicadores()
		{
			var queryBuilder = new StringBuilder();

			queryBuilder.Append("SELECT id_indicador_classificacao AS Id, id_entidade AS EntidadeId, nome AS Nome");
			queryBuilder.Append(" FROM tb_indicador_classificacao WHERE ativo = 1");

			return BuildEntitiesFromSql(queryBuilder.ToString());
		}
		#endregion
	}
}
