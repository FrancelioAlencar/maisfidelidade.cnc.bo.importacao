﻿using maisfidelidade.cnc.bo.importacao.core.Common.Log;
using maisfidelidade.cnc.bo.importacao.core.Domain.Model;
using maisfidelidade.cnc.bo.importacao.core.Infrastructure.datacontext;
using maisfidelidade.cnc.bo.importacao.core.Infrastructure.Repositories.Factories;
using maisfidelidade.cnc.bo.importacao.core.Infrastructure.RepositoryFramework;
using maisfidelidade.cnc.bo.importacao.Core.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Data;

namespace maisfidelidade.cnc.bo.importacao.core.Infrastructure.Repositories
{
    public class LojaParticipanteRepositorio : RepositorioBase, ILojaParticipanteRepositorio
    {
        public LojaParticipanteRepositorio(ConnectionDBMysql connectionDB)
            : base(connectionDB)
        {
        }

        public IList<Parceiro> ObterIdsParceiro(IList<string> cnpjs, out IList<LogDetalhe> listaErros)
        {
            try
            {
                listaErros = new List<LogDetalhe>();

                IList<Parceiro> retorno = new List<Parceiro>();

                var query = $"select ID, CD_NUM_Cliente, cnpj from vmotor_BDV1.PARTNERS where cnpj in ('{String.Join("','", cnpjs)}');";

                using (IDataReader reader = this.ExecutarReader(query))
                {
                    while (reader.Read())
                    {
                        retorno.Add(ClienteMaisFidelidadeFactory.ConstruirParceiro(reader, out LogDetalhe erro));

                        if (erro != null)
                            listaErros.Add(erro);
                    }
                }

                return retorno;
            }
            catch (Exception exception)
            {
                throw new Exception("LojaParticipanteRepositorio > ObterIdsParceiro", exception);
            }
        }

        public IEnumerable<Segmento> ObterSubSegmentosAtivos()
        {
            try
            {
                var query = @"SELECT id_segmento AS IdSegmento, nome AS Nome, ativo AS Ativo FROM autos_tb_segmento WHERE ativo = 1 AND id_segmentoPai IS NOT NULL";

                return this.Database.Obter<Segmento>(query);
            }
            catch (Exception exception)
            {
                throw new Exception("LojaParticipanteRepositorio > ObterSubSegmentosAtivos", exception);
            }
        }

        public IEnumerable<Filial> ObterFiliaisAtivas()
        {
            try
            {
                var query = @"SELECT id_filial AS IdFilial, nome AS Nome, ativo AS Ativo FROM autos_tb_filial WHERE ativo = 1";

                return this.Database.Obter<Filial>(query);
            }
            catch (Exception exception)
            {
                throw new Exception("LojaParticipanteRepositorio > ObterFiliaisAtivas", exception);
            }
        }
    }
}
