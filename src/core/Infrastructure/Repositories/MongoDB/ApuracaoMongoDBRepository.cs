﻿using maisfidelidade.cnc.imp.core.Domain.Model.Apuracoes;
using maisfidelidade.cnc.imp.core.Infrastructure.Repositories.MongoDB.Base;
using System.Collections.Generic;
using System.Linq;

namespace maisfidelidade.cnc.imp.core.Infrastructure.Repositories.MongoDB
{
    public class ApuracaoMongoDBRepository : MongoDBRepositoryBase<ApuracaoRegistro>, IApuracaoMongoDBRepository
    {
        public ApuracaoMongoDBRepository()
            : base ("apuracoes")
        {

        }

        #region IApuracaoMongoDBRepository Membros

        public IList<ApuracaoImportada> BuscarApuracoes(object arquivoImportadoId)
        {
            var empresaApuracoes = new List<ApuracaoImportada>();

            empresaApuracoes = this.GetQueryableCollection()
                .Where(r => r.ArquivoImportadoId == arquivoImportadoId).SelectMany(a => a.Apuracoes)
                .ToList();

            return empresaApuracoes;
        }

        public void Salvar(ApuracaoRegistro apuracaoRegistro)
        {
            PersistNewItem(apuracaoRegistro);
        } 

        #endregion
    }
}
