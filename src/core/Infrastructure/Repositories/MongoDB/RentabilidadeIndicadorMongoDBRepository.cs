﻿using maisfidelidade.cnc.imp.core.Domain.Model.ClassificacaoIndicadores;
using maisfidelidade.cnc.imp.core.Domain.Model.RentabilidadeIndicadores;
using maisfidelidade.cnc.imp.core.Infrastructure.Repositories.MongoDB.Base;
using System.Collections.Generic;
using System.Linq;

namespace maisfidelidade.cnc.imp.core.Infrastructure.Repositories.MongoDB
{
	public class RentabilidadeIndicadorMongoDBRepository : MongoDBRepositoryBase<RentabilidadeIndicadorRegistro>, IRentabilidadeIndicadorMongoDBRepository
	{
		public RentabilidadeIndicadorMongoDBRepository()
			: base("indicadores.rentabilidade")
		{

		}



		#region IRentabilidadeIndicadorMongoDBRepository Membros

		public IList<RentabilidadeApuracaoImportada> BuscarRentabilidadeApuracoes(object arquivoImportadoId)
		{
			var rentabilidadeIndicadorApuracoes = new List<RentabilidadeApuracaoImportada>();

			rentabilidadeIndicadorApuracoes = this.GetQueryableCollection()
				.Where(r => r.ArquivoImportadoId == arquivoImportadoId).SelectMany(r => r.RentabilidadeApuracoes)
				.ToList();

			return rentabilidadeIndicadorApuracoes;
		}

		public void Salvar(RentabilidadeIndicadorRegistro performanceIndicadorRegistro)
		{
			PersistNewItem(performanceIndicadorRegistro);
		}

		#endregion

	}
}
