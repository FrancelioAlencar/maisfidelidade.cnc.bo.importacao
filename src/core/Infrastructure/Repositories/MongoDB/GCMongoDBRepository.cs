﻿using maisfidelidade.cnc.imp.core.Domain.Model.Gerentes;
using maisfidelidade.cnc.imp.core.Infrastructure.Repositories.MongoDB.Base;
using System.Collections.Generic;
using System.Linq;

namespace maisfidelidade.cnc.imp.core.Infrastructure.Repositories.MongoDB
{
	public class GCMongoDBRepository : MongoDBRepositoryBase<GCRegistro>, IGCMongoDBRepository
    {
        public GCMongoDBRepository()
            : base("gerentes.comercial")
        {

        }

        #region IGRMongoDBRepository Membros

        public IList<GCImportado> BuscarGCImportados(object arquivoId)
        {
            var gcImportados = new List<GCImportado>();

            gcImportados = this.GetQueryableCollection()
                .Where(g => g.ArquivoImportadoId == arquivoId).SelectMany(g => g.GCImportados)
                .ToList();

            return gcImportados;
        }

        public void Salvar(GCRegistro gcRegistro)
        {
            PersistNewItem(gcRegistro);
        }

        #endregion
    }
}
