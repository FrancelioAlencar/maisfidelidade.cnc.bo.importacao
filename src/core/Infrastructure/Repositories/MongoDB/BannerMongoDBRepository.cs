﻿using maisfidelidade.cnc.imp.core.Domain.Model.Banners;
using maisfidelidade.cnc.imp.core.Infrastructure.Repositories.MongoDB.Base;

namespace maisfidelidade.cnc.imp.core.Infrastructure.Repositories.MongoDB
{
	public class BannerMongoDBRepository : MongoDBRepositoryBase<BannerRegistro>, IBannerMongoDBRepository
	{
		public BannerMongoDBRepository()
			: base("banners")
		{

		}

		#region IBannerMongoDBRepository Membros

		public void Salvar(BannerRegistro bannerRegistro)
		{
			PersistNewItem(bannerRegistro);
		}

		#endregion

	}
}

