﻿using maisfidelidade.cnc.imp.core.Domain.Model.Gerentes;
using maisfidelidade.cnc.imp.core.Infrastructure.Repositories.MongoDB.Base;
using System.Collections.Generic;
using System.Linq;

namespace maisfidelidade.cnc.imp.core.Infrastructure.Repositories.MongoDB
{
	public class GRMongoDBRepository : MongoDBRepositoryBase<GRRegistro>, IGRMongoDBRepository
    {
        public GRMongoDBRepository()
            : base("gerentes.relacionamento")
        {

        }

        #region IGRMongoDBRepository Membros

        public IList<GRImportado> BuscarGRImportados(object arquivoId)
        {
            var grImportados = new List<GRImportado>();

            grImportados = this.GetQueryableCollection()
                .Where(g => g.ArquivoImportadoId == arquivoId).SelectMany(g => g.GRImportados)
                .ToList();

            return grImportados;
        }

        public void Salvar(GRRegistro grRegistro)
        {
            PersistNewItem(grRegistro);
        } 

        #endregion
    }
}
