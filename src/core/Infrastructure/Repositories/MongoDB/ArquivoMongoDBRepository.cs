﻿using maisfidelidade.cnc.imp.core.Domain.Model.Arquivos;
using maisfidelidade.cnc.imp.core.Domain.Model.Enums;
using maisfidelidade.cnc.imp.core.Infrastructure.Data;
using maisfidelidade.cnc.imp.core.Infrastructure.Data.MongoDB.Extensions;
using maisfidelidade.cnc.imp.core.Infrastructure.DomainBase;
using maisfidelidade.cnc.imp.core.Infrastructure.Repositories.MongoDB.Base;
using System.Collections.Generic;
using System.Linq;

namespace maisfidelidade.cnc.imp.core.Infrastructure.Repositories.MongoDB
{
    public class ArquivoMongoDBRepository : MongoDBRepositoryBase<ArquivoImportado>, IArquivoMongoDBRepository
    {
        public ArquivoMongoDBRepository()
            : base("arquivos")
        {

        }

        #region IArquivoMongoDBRepositorio Membros

        public ArquivoImportado BuscarPor(object id)
        {
            ArquivoImportado arquivo = null;

            var filter = new Filter();

            filter.Add("_id", id);

            var arquivos = this.Execute(filter);

            return arquivo = arquivos?.SingleOrDefault();
        }

        public ArquivoImportado BuscarPor(string nome)
        {
            ArquivoImportado arquivo = null;

            var filter = new Filter();

            filter.Add("nome", nome);

            var arquivos = this.Execute(filter);

            arquivo = arquivos?.SingleOrDefault();

            return arquivo;
        }

        public ArquivoImportadoConteudo BuscarConteudoPor(object idArquivo)
        {
            ArquivoImportadoConteudo arquivoConteudo = null;

            var filter = new Filter();

            filter.Add("_id_arquivo", idArquivo);

            var conteudos = this.Execute<ArquivoImportadoConteudo>("arquivos.dados", filter);

            arquivoConteudo = conteudos?.SingleOrDefault();

            return arquivoConteudo;
        }

        public IList<ArquivoImportadoLinha> BuscarArquivoImportadoLinhas(object idArquivo, int skip, int quantidadeLinhas)
        {
            var linhas = new List<ArquivoImportadoLinha>();

            linhas = this.GetQueryableCollection<ArquivoImportadoConteudo>("arquivos.dados")
                .Where(c => c.ArquivoImportadoId == idArquivo).SelectMany(c => c.Linhas)
                    .Skip(skip).Take(quantidadeLinhas)
                    .ToList();

            return linhas;
        }

        public void Salvar(ArquivoImportado arquivo)
        {
            if (this.BuscarPor(arquivo.Id) == null)
            {
                PersistNewItem(arquivo);
            }
            else
            {
                PersistUpdatedItem(arquivo);
            }
        }

        public void SalvarConteudo(ArquivoImportadoConteudo arquivoConteudo)
        {
            PersistNewItem(arquivoConteudo);
        }

        public IDictionary<int, ArquivoColuna> ObterColunas()
        {
            IList<ArquivoColuna> colunas = new List<ArquivoColuna>();

            var filter = new Filter();

            filter.Add("ativo", true);

            colunas = this.Execute<ArquivoColuna>("configuracao.arquivo.colunas", filter);

            return colunas.ToDictionary(c => c.ColunaId);
        }

        public IDictionary<int, ArquivoColuna> ObterColunas(GrupoColunaEnum grupoColuna)
        {
            IList<ArquivoColuna> colunas = new List<ArquivoColuna>();

            var filter = new Filter();

            filter.Add("coluna_grupos", grupoColuna);
            filter.Add("ativo", true);

            colunas = this.Execute<ArquivoColuna>("configuracao.arquivo.colunas", filter);

            return colunas.ToDictionary(c => c.ColunaId);
        }

        #endregion

        protected override void PersistNewItem(IEntity item)
        {
            ArquivoImportadoConteudo arquivoConteudo = item as ArquivoImportadoConteudo;

            if (arquivoConteudo is ArquivoImportadoConteudo)
            {
                PersistNewItem(arquivoConteudo);
            }
        }

        protected void PersistNewItem(ArquivoImportadoConteudo conteudo)
        {
            this.Database.Create<ArquivoImportadoConteudo>(conteudo, "arquivos.dados");
        }


    }
}
