﻿using maisfidelidade.cnc.imp.core.Domain.Model.PerformanceIndicadores;
using maisfidelidade.cnc.imp.core.Infrastructure.Repositories.MongoDB.Base;
using System.Collections.Generic;
using System.Linq;

namespace maisfidelidade.cnc.imp.core.Infrastructure.Repositories.MongoDB
{
	public class PerformanceIndicadorMongoDBRepository : MongoDBRepositoryBase<PerformanceIndicadorRegistro>, IPerformanceIndicadorMongoDBRepository
	{
		public PerformanceIndicadorMongoDBRepository()
			: base("indicadores.performance")
		{

		}

		#region IPerformanceIndicadorMongoDBRepository Membros

        public IList<PerformanceIndicadorApuracaoImportado> BuscarPerformanceIndicadorApuracoes(object arquivoImportadoId)
        {
            var performanceIndicadorApuracoes = new List<PerformanceIndicadorApuracaoImportado>();

            performanceIndicadorApuracoes = this.GetQueryableCollection()
                .Where(r => r.ArquivoImportadoId == arquivoImportadoId).SelectMany(r => r.PerformanceIndicadorApuracoes)
                .ToList();

            return performanceIndicadorApuracoes;
        }

        public void Salvar(PerformanceIndicadorRegistro performanceIndicadorRegistro)
        {
            PersistNewItem(performanceIndicadorRegistro);
        }

		#endregion

	}
}
