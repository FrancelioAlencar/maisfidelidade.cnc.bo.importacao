﻿using maisfidelidade.cnc.imp.core.Domain.Model.ClassificacaoIndicadores;
using maisfidelidade.cnc.imp.core.Infrastructure.Repositories.MongoDB.Base;
using System.Collections.Generic;
using System.Linq;

namespace maisfidelidade.cnc.imp.core.Infrastructure.Repositories.MongoDB
{
	public class ClassificacaoIndicadorMongoDBRepository : MongoDBRepositoryBase<ClassificacaoIndicadorRegistro>, IClassificacaoIndicadorMongoDBRepository
	{
		public ClassificacaoIndicadorMongoDBRepository()
			: base("indicadores.classificacao")
		{

		}

		#region IClassificacaoIndicadorMongoDBRepository Membros

		public IList<ClassificacaoIndicadorApuracaoImportado> BuscarClassificacaoIndicadorApuracoes(object arquivoImportadoId)
		{
			var classificacaoIndicadorApuracoes = new List<ClassificacaoIndicadorApuracaoImportado>();

			classificacaoIndicadorApuracoes = this.GetQueryableCollection()
				.Where(r => r.ArquivoImportadoId == arquivoImportadoId).SelectMany(r => r.ClassificacaoIndicadorApuracoes)
				.ToList();

			return classificacaoIndicadorApuracoes;
		}

		public void Salvar(ClassificacaoIndicadorRegistro classificacaoIndicadorRegistro)
		{
			PersistNewItem(classificacaoIndicadorRegistro);
		}

		#endregion

	}
}

