﻿using maisfidelidade.cnc.imp.core.Infrastructure.Data;
using maisfidelidade.cnc.imp.core.Infrastructure.Data.MongoDB;
using maisfidelidade.cnc.imp.core.Infrastructure.Data.MongoDB.Extensions;
using maisfidelidade.cnc.imp.core.Infrastructure.DomainBase;
using maisfidelidade.cnc.imp.core.Infrastructure.RepositoryFramework;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;

namespace maisfidelidade.cnc.imp.core.Infrastructure.Repositories.MongoDB.Base
{
    public abstract class MongoDBRepositoryBase<TAggregateRoot> : RepositoryBase<TAggregateRoot>
        where TAggregateRoot : IAggregateRoot
    {
        protected MongoDBRepositoryBase(string collectionName)
        {
            this.Database = DatabaseFactory.CreateMongoDBDatabase("stage", DatabaseType.MongoDB);
            this.CollectionName = collectionName;

            if (Database.DatabaseType != DatabaseType.MongoDB)
            {
                throw new InvalidOperationException("O objeto atribuído a propriedade Database não é do tipo MongoDBDatabase");
            }
        }

        protected MongoDBDatabase Database { get; }
        protected string CollectionName { get; }
        
        #region Métodos Protegidos

        protected IList<TAggregateRoot> Execute(Filter filter = null)
        {
            IList<TAggregateRoot> entidades = new List<TAggregateRoot>();

            entidades = this.Database.Read<TAggregateRoot>(CollectionName, filter);

            return entidades;
        }

        protected IList<TEntity> Execute<TEntity>(string collection)
        {
            IList<TEntity> entidades = new List<TEntity>();

            entidades = this.Database.Read<TEntity>(collection);

            return entidades;
        }

        protected IList<TEntity> Execute<TEntity>(string collection, Filter filter = null)
        //where TEntity : IEntity
        {
            IList<TEntity> entidades = new List<TEntity>();

            entidades = this.Database.Read<TEntity>(collection, filter);

            return entidades;
        }

        protected IList<TEntity> Execute<TEntity>(string collection, Filter filter = null, Projection projection = null)
        //where TEntity : IEntity
        {
            IList<TEntity> entidades = new List<TEntity>();

            entidades = this.Database.Read<TEntity>(collection, filter, projection);

            return entidades;
        }

        protected IMongoQueryable<TAggregateRoot> GetQueryableCollection()
        {
            return this.Database.GetQueryableCollection<TAggregateRoot>(CollectionName);
        }

        protected IMongoQueryable<TEntity> GetQueryableCollection<TEntity>(string collection)
        {
            return this.Database.GetQueryableCollection<TEntity>(collection);
        }

        protected override void PersistNewItem(TAggregateRoot item)
        {
            this.Database.Create<TAggregateRoot>(item, CollectionName);
        }

        protected override void PersistUpdatedItem(TAggregateRoot item)
        {
            this.Database.Update<TAggregateRoot>(item, CollectionName);
        }

        protected override void PersistDeletedItem(TAggregateRoot item)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
