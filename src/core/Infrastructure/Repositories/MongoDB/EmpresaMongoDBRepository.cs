﻿using maisfidelidade.cnc.imp.core.Domain.Model.Empresas;
using maisfidelidade.cnc.imp.core.Infrastructure.Repositories.MongoDB.Base;
using System.Collections.Generic;
using System.Linq;

namespace maisfidelidade.cnc.imp.core.Infrastructure.Repositories.MongoDB
{
	public class EmpresaMongoDBRepository : MongoDBRepositoryBase<EmpresaRegistro>, IEmpresaMongoDBRepository
    {
        public EmpresaMongoDBRepository()
            : base("empresas")
        {

        }



        #region IEmpresaMongoDBRepository Membros

        public IList<EmpresaImportada> BuscarEmpresasImportadas(object arquivoId)
        {
            var empresasImportadas = new List<EmpresaImportada>();

            empresasImportadas = this.GetQueryableCollection()
                .Where(g => g.ArquivoImportadoId == arquivoId).SelectMany(e => e.EmpresasImportadas)
                .ToList();

            return empresasImportadas;
        }

        public void Salvar(EmpresaRegistro empresaRegistro)
        {
            PersistNewItem(empresaRegistro);
        }

        #endregion
    }
}
