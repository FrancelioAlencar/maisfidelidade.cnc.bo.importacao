﻿using maisfidelidade.cnc.imp.core.Common.Logging;
using maisfidelidade.cnc.imp.core.Infrastructure.Repositories.MongoDB.Base;

namespace maisfidelidade.cnc.imp.core.Infrastructure.Repositories.MongoDB
{
    public class LogMongoDBRepository : MongoDBRepositoryBase<Log>, ILogMongoDBRepository
    {
        public LogMongoDBRepository()
            : base("logs")
        {
        }

        public void Salvar(Log logInfo)
        {
            PersistNewItem(logInfo);
        }
    }
}
