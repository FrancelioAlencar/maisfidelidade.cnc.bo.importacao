﻿using maisfidelidade.cnc.imp.core.Infrastructure.Data;
using maisfidelidade.cnc.imp.core.Infrastructure.DomainBase;
using maisfidelidade.cnc.imp.core.Infrastructure.Repositories.Sql;
using System;

namespace maisfidelidade.cnc.imp.core.Infrastructure.Repositories.SqlServer
{
	public class SqlServerRepositorioBase<T> : SqlRepositorioBase<T> where T : IAggregateRoot
	{
		protected SqlServerRepositorioBase()
			: base(DatabaseType.SqlServer)
		{
			if (Database.DatabaseType != DatabaseType.SqlServer)
			{
				throw new InvalidOperationException("O objeto atribuído a propriedade Database não é do tipo SqlServerDatabase");
			}
		}

		protected override void BuildChildCallbacks()
		{
			throw new NotImplementedException();
		}
	}
}
