﻿using maisfidelidade.cnc.imp.core.Infrastructure.Data;
using maisfidelidade.cnc.imp.core.Infrastructure.DomainBase;
using maisfidelidade.cnc.imp.core.Infrastructure.Repositories.Sql;
using System;

namespace maisfidelidade.cnc.imp.core.Infrastructure.Repositories.SqlServer.Base
{
	public abstract class SqlServerRepositorioBase<TAggregateRoot> : SqlRepositorioBase<TAggregateRoot> where TAggregateRoot : IAggregateRoot
	{
		protected SqlServerRepositorioBase()
			: base(DatabaseType.SqlServer)
		{
			if (Database.DatabaseType != DatabaseType.SqlServer)
			{
				throw new InvalidOperationException("O objeto atribuído a propriedade Database não é do tipo MySqlDatabase");
			}
		}
	}
}
