using maisfidelidade.cnc.imp.core.Common.Logging;
using maisfidelidade.cnc.imp.core.Domain.Model.Apuracoes;
using maisfidelidade.cnc.imp.core.Domain.Model.Arquivos;
using maisfidelidade.cnc.imp.core.Domain.Model.Banners;
using maisfidelidade.cnc.imp.core.Domain.Model.Categorias;
using maisfidelidade.cnc.imp.core.Domain.Model.ClassificacaoIndicadores;
using maisfidelidade.cnc.imp.core.Domain.Model.Empresas;
using maisfidelidade.cnc.imp.core.Domain.Model.Gerentes;
using maisfidelidade.cnc.imp.core.Domain.Model.PerformanceIndicadores;
using maisfidelidade.cnc.imp.core.Domain.Model.RentabilidadeIndicadores;
using maisfidelidade.cnc.imp.core.Domain.Model.Segmentos;
using maisfidelidade.cnc.imp.core.Infrastructure.Publication;
using maisfidelidade.cnc.imp.core.Infrastructure.Repositories.MongoDB;
using maisfidelidade.cnc.imp.core.Infrastructure.Repositories.MySql;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace maisfidelidade.cnc.imp.core.Common.DependencyInjection
{
    public static class DependencyResolver
    {
        private readonly static IServiceProvider serviceProvider;

        static DependencyResolver()
        {
            var serviceCollection = new ServiceCollection();

            AdicionarVinculos(serviceCollection);

            serviceProvider = serviceCollection.BuildServiceProvider();
        }

        public static T GetService<T>()
        {
            try
            {
                return serviceProvider.GetService<T>();
            }
            catch (Exception exception)
            {
                throw new Exception("", exception);
            }
        }

        public static object ObterServico(Type servicoType)
        {
            try
            {
                return serviceProvider.GetService(servicoType);
            }
            catch (Exception exception)
            {
                throw new Exception("", exception);
            }
        }

        private static void AdicionarVinculos(IServiceCollection servicos)
        {
            AdicionarVinculosLogging(servicos);
            AdicionarViculosMySqlRepositories(servicos);
            AdicionarViculosMongoDBRepositories(servicos);
        }

        private static void AdicionarVinculosLogging(IServiceCollection servicos)
        {
            servicos.AddTransient<ILogger, Logger>();
            servicos.AddTransient<ILogMongoDBRepository, LogMongoDBRepository>();
        }

        private static void AdicionarViculosMySqlRepositories(IServiceCollection servicos)
        {
            servicos.AddTransient<IArquivoMySqlRepository, ArquivoMySqlRepository>();
            servicos.AddTransient<IEmpresaMySqlRepository, EmpresaMySqlRepository>();
            servicos.AddTransient<IGerenteMySqlRepository, GerenteMySqlRepository>();
            servicos.AddTransient<IPublishedDataRepository, MySqlPublishedDataRepository>();
            servicos.AddTransient<ISegmentoMySqlRepository, SegmentoMySqlRepository>();
			servicos.AddTransient<IPerformanceIndicadorMySqlRepository, PerformanceIndicadorMySqlRepository>();
			servicos.AddTransient<IRentabilidadeIndicadorMySqlRepository, RentabilidadeIndicadorMySqlRepository>();
			servicos.AddTransient<IClassificacaoIndicadorMySqlRepository, ClassificacaoIndicadorMySqlRepository>();
            servicos.AddTransient<ICategoriaMySqlRepository, CategoriaMySqlRepository>();
            servicos.AddTransient<IBannerMySqlRepository, BannerMySqlRepository>();
        }

        private static void AdicionarViculosMongoDBRepositories(IServiceCollection servicos)
        {
            servicos.AddTransient<IArquivoMongoDBRepository, ArquivoMongoDBRepository>();
            servicos.AddTransient<IEmpresaMongoDBRepository, EmpresaMongoDBRepository>();
            servicos.AddTransient<IApuracaoMongoDBRepository, ApuracaoMongoDBRepository>();
            servicos.AddTransient<IPerformanceIndicadorMongoDBRepository, PerformanceIndicadorMongoDBRepository>();
			servicos.AddTransient<IRentabilidadeIndicadorMongoDBRepository, RentabilidadeIndicadorMongoDBRepository >();
			servicos.AddTransient<IClassificacaoIndicadorMongoDBRepository, ClassificacaoIndicadorMongoDBRepository>();
			servicos.AddTransient<IGRMongoDBRepository, GRMongoDBRepository>();
            servicos.AddTransient<IGCMongoDBRepository, GCMongoDBRepository>();
            servicos.AddTransient<IBannerMongoDBRepository, BannerMongoDBRepository>();
        }
    }
}
