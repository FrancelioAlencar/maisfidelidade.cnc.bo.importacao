﻿using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using maisfidelidade.cnc.imp.core.Common.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net;
using System.Reflection;
using System.Text;

namespace maisfidelidade.cnc.imp.core.Common.Configuration
{
	public static class Extensions
	{
		public static void SetEnvironment(this ILambdaContext context)
		{

            var functionArn = context != null && !string.IsNullOrWhiteSpace(context.InvokedFunctionArn) ? context.InvokedFunctionArn : "hml";

			if (functionArn.EndsWith("hml"))
				Environment.SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT", "hml");
			else if (functionArn.EndsWith("azl"))
				Environment.SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT", "azl");
			else if (functionArn.EndsWith("prd"))
				Environment.SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT", "prd");
			else
				Environment.SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT", "dev");

        }

		public static T To<T>(this object value, T defaultValue = default(T))
		{
			if (value == null) return defaultValue;
			try
			{
				if (typeof(T).IsEnum)
				{
					if (value is char || value is string)
					{
						string vbyte = value.ToString();
						if (value.ToString().Length == 1) vbyte = ((sbyte)char.Parse(value.ToString())).ToString();
						return (T)Enum.Parse(typeof(T), vbyte.ToString());
					}
					else
						return (T)Enum.Parse(typeof(T), value.ToString());
				}
				return (T)Convert.ChangeType(value, typeof(T));
			}
			catch { }
			return defaultValue;
		}

		public static APIGatewayProxyResponse CreateResponse(this object _object, HttpStatusCode _httpStatusCode = HttpStatusCode.OK)
		{
			return new APIGatewayProxyResponse()
			{
				StatusCode = (int)_httpStatusCode,
				Headers = new Dictionary<string, string> { { "Access-Control-Allow-Origin", "*" }, { "Access-Control-Allow-Headers", "*" } },
				Body = JsonConvert.SerializeObject(new { success = _httpStatusCode != HttpStatusCode.OK ? false : true, data = _object }, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore, MissingMemberHandling = MissingMemberHandling.Ignore })
			};
		}

		public static string GetDetailedDescription(this Enum value)
		{
			FieldInfo fieldInfo = value.GetType().GetField(Enum.GetName(value.GetType(), value));
			DescriptionAttribute descriptionAttribute = fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false)[0] as DescriptionAttribute;
			return descriptionAttribute.Description;
		}


		public static string DecodeBase64(this string val)
		{
			return Encoding.UTF8.GetString(Convert.FromBase64String(val));
		}

		public static string FormatStr(this string text, params object[] args)
		{
			return string.Format(text, args);
		}
	}
}
