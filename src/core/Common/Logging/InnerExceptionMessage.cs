﻿using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.imp.core.Common.Logging
{
    public class InnerExceptionMessage
    {
        public string Message { get; }

        public InnerExceptionMessage(Exception exception)
        {
            if (exception.InnerException != null)
            {
                Message = exception.InnerException.Message;

                new InnerExceptionMessage(exception.InnerException);
            }
        }
    }
}
