﻿using System;
using System.Diagnostics;

namespace maisfidelidade.cnc.imp.core.Common.Logging
{
	public class LogWatcher
    {
        private readonly Stopwatch stopwatch;
        private DateTime? inicio;

        public LogWatcher()
        {
            stopwatch = new Stopwatch();
        }

        public DateTime Inicio => inicio ?? throw new Exception("Data início está nula. LogWatcher não foi iniciado.");
        public DateTime? Termino { get; private set; }
        public TimeSpan TempoDecorrido => inicio != null ? stopwatch.Elapsed : throw new Exception("Não há tempo como obter o tempo decorrido. LogWatcher não foi iniciado.");

        public void Iniciar()
        {
            if (!stopwatch.IsRunning)
            {
                stopwatch.Restart();

                inicio = DateTime.Now;

                stopwatch.Start();
            }
        }

        public void Parar()
        {
            if (stopwatch.IsRunning)
            {
                Termino = DateTime.Now;

                stopwatch.Stop();
            }
        }
    }
}
