﻿using maisfidelidade.cnc.imp.core.Infrastructure.DomainBase;
using maisfidelidade.cnc.imp.core.Infrastructure.DomainBase.MongoDB;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace maisfidelidade.cnc.imp.core.Common.Logging
{
    public class Log : MongoEntityBase, IAggregateRoot
    {
        public Log(TipoLog tipoLog, LogWatcher logWatcher)
        {
            if (tipoLog == TipoLog.Error)
                logWatcher.Parar();

            TipoLog = tipoLog;
            TipoLogNome = tipoLog.ToString();
            DataInclusao = DateTime.Now;
            DataInicioExecucao = logWatcher.Inicio;
            DataTerminoExecucao = logWatcher.Termino;
            TempoDecorrido = logWatcher.TempoDecorrido;
        }

        [BsonElement("tipo_log")]
        public TipoLog TipoLog { get; }

        [BsonElement("tipo_log_nome")]
        public string TipoLogNome { get; }

        [BsonElement("log_context")]
        public LogContextEnum LogContext { get; set; }

        [BsonElement("log_context_nome")]
        public string LogContextNome { get; set; }

        [BsonElement("dt_inclusao")]
        public DateTime DataInclusao { get; }

        [BsonElement("data_inicio_execucao")]
        public DateTime DataInicioExecucao { get; }

        [BsonElement("data_termino_execucao")]
        public DateTime? DataTerminoExecucao { get; }

        [BsonElement("tempo_decorrido")]
        public TimeSpan TempoDecorrido { get; }

        [BsonElement("id_arquivo_importado")]
        public object ArquivoImportadoId { get; set; }

        [BsonElement("mensagem")]
        public string Mensagem { get; set; }

        [BsonElement("stack_trace")]
        public string StackTrace { get; set; }

        [BsonElement("inner_exception_message")]
        public InnerExceptionMessage InnerExceptionMessage { get; set; }


        //[BsonElement("id_status_processamento")]
        //public StatusAndamentoProcessoEnum StatusAndamentoProcesso { get; set; }

        //[BsonElement("id_status_log")]
        //public StatusLogEnum StatusLog { get; set; }




        //[BsonElement("detalhes")]
        //public IList<LogDetalhe> Detalhes { get; set; }
    }
}
