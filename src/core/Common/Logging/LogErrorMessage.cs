﻿using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.imp.core.Common.Logging
{
    public class LogErrorMessage
    {
        public const string S3Nulo = "S3 está retornando nulo.";
        public const string ExtensaoArquivoImportadoInvalida = "A extensão do arquivo está inválida. Ele dever ser um arquivo .csv.";
        public const string ExisteArquivoImportadoOuPublicadoComMesmoNome = "Nome de arquivo duplicado. Já existe um arquivo importado ou publicado com o mesmo nome.";
        public const string StatusDiferenteDeAguardandoValidacaoFisica = "O status do arquivo importado está diferente de AguardandoValidacaoFisica.";
    }
}
