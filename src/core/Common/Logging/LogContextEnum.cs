﻿namespace maisfidelidade.cnc.imp.core.Common.Logging
{
    public enum LogContextEnum
    {
        ValidacaoFisica = 1,
        ValidacaoLogica = 2,
        Empresa = 31,
        Gerente = 32,
        OrquestracaoCadastro = 4,
        Apuracao = 5,
        ApuracaoBanner = 61,
        ApuracaoCampanha = 62,
        ApuracaoClassificacaoIndicador = 63,
        ApuracaoPerformanceIndicador = 64,
        ApuracaoRentabilidadeIndicador = 65,
        OrquestracaoApuracao = 7,
        Publicacao = 8
    }
}
