﻿using maisfidelidade.cnc.imp.core.Domain.Model.Enums;
using MongoDB.Bson;
using System;
using System.Collections.Generic;

namespace maisfidelidade.cnc.imp.core.Common.Logging
{
    public interface ILogger
    {
        void Log(Log logInfo);
    }
}
