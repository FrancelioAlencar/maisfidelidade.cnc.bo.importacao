﻿using maisfidelidade.cnc.imp.core.Infrastructure.RepositoryFramework;

namespace maisfidelidade.cnc.imp.core.Common.Logging
{
    public interface ILogMongoDBRepository : IRepository<Log>
    {
        void Salvar(Log logInfo);
    }
}
