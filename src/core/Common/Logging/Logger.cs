﻿namespace maisfidelidade.cnc.imp.core.Common.Logging
{
	public class Logger : ILogger
	{
		private readonly ILogMongoDBRepository logMongoDBRepository;

		public Logger(ILogMongoDBRepository logMongoDBRepositorio)
		{
			this.logMongoDBRepository = logMongoDBRepositorio;
		}

		public void Log(Log logInfo)
		{
			logMongoDBRepository.Salvar(logInfo);
		}
	}
}
