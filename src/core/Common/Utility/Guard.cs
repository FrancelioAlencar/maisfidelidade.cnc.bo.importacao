﻿using System;

namespace maisfidelidade.cnc.imp.core.Common.Utility
{
	public static class Guard
    {
        public static void ArgumentNotNull(object argumentValue, string argumentName)
        {
            if (argumentValue == null)
            {
                throw new ArgumentNullException(argumentName);
            }
        }

    }
}
