﻿using Amazon.Lambda.Core;
using maisfidelidade.cnc.imp.core.Common.Configuration;
using maisfidelidade.cnc.imp.core.Common.DependencyInjection;
using maisfidelidade.cnc.imp.core.Common.Logging;
using maisfidelidade.cnc.imp.core.Infrastructure.Data;
using maisfidelidade.cnc.imp.core.Infrastructure.Data.MongoDB;
using maisfidelidade.cnc.imp.core.Infrastructure.Data.MySql;
using maisfidelidade.cnc.imp.core.Infrastructure.Data.Sql;
using maisfidelidade.cnc.imp.core.Infrastructure.Data.SqlServer;
using System;


namespace maisfidelidade.cnc.imp.core.Common.AWS
{
    public abstract class FunctionBase
    {
        private readonly LogContextEnum logContext;
        private readonly ILogger logger;

        protected FunctionBase(LogContextEnum logContext)
        {
            this.logContext = logContext;

            DatabaseFactory.SetSqlDatabase(GetSqlDatabase, false);
            DatabaseFactory.SetMongoDBDatabase(GetMongoDBDatabase, false);

            logger = DependencyResolver.GetService<ILogger>();

            LogWatcher = new LogWatcher();
        }

        protected LogWatcher LogWatcher { get; }


        #region Log Métodos

        protected void LogError(ILambdaContext lambdaContext, object arquivoImportadoId, string mensagem, string stackTrace = null, Exception exception = null)
        {
            logger.Log(new Log(TipoLog.Error, LogWatcher)
            {
                LogContext = logContext,
                LogContextNome = logContext.ToString(),
                ArquivoImportadoId = arquivoImportadoId,
                Mensagem = mensagem,
                StackTrace = stackTrace,
                InnerExceptionMessage = exception != null ? new InnerExceptionMessage(exception) : null
            });
        }

        protected void LogInfo(ILambdaContext lambdaContext, object arquivoImportadoId, string mensagem)
        {
            logger.Log(new Log(TipoLog.Info, LogWatcher)
            {
                LogContext = logContext,
                LogContextNome = logContext.ToString(),
                ArquivoImportadoId = arquivoImportadoId,
                Mensagem = mensagem,
            });
        }

        protected void BeginLogInfo(ILambdaContext lambdaContext, object arquivoImportadoId, string mensagem)
        {
            LogWatcher.Iniciar();

            logger.Log(new Log(TipoLog.Info, LogWatcher)
            {
                LogContext = logContext,
                LogContextNome = logContext.ToString(),
                ArquivoImportadoId = arquivoImportadoId,
                Mensagem = mensagem,
            });
        }

        protected void EndLogInfo(ILambdaContext lambdaContext, object arquivoImportadoId, string mensagem)
        {
            LogWatcher.Parar();

            logger.Log(new Log(TipoLog.Info, LogWatcher)
            {
                LogContext = logContext,
                LogContextNome = logContext.ToString(),
                ArquivoImportadoId = arquivoImportadoId,
                Mensagem = mensagem,
            });
        }

        #endregion

        #region Métodos Privados

        private static MongoDBDatabase GetMongoDBDatabase(string name, DatabaseType databaseType)
        {
            try
            {
                MongoDBDatabase database = null;

                if (databaseType == DatabaseType.MongoDB)
                {
                    switch (name)
                    {
                        case "stage":
                            database = new MongoDBDatabase(new DatabaseSettings("connection_maisfidelidade_mongodb").ConnectionString);
                            break;
                        default:
                            throw new Exception("Unexpected Case");
                    }
                }

                return database;
            }
            catch (Exception exception)
            {
                throw new Exception("Erro ao instanciar a conexão", exception);
            }
        }

        private static SqlDatabase GetSqlDatabase(string name, DatabaseType databaseType)
        {
            try
            {
                SqlDatabase database = null;

                if (databaseType == DatabaseType.MySql)
                {
                    switch (name)
                    {
                        case "mysql":
                            database = new MySqlDatabase(new DatabaseSettings("connection_maisfidelidade_mysql").ConnectionString);
                            break;
                        case "write":
                            database = new MySqlDatabase(new DatabaseSettings("connection_maisfidelidade_mysql_write").ConnectionString);
                            break;
                        default:
                            throw new Exception("Unexpected Case");
                    }
                }

                if (databaseType == DatabaseType.SqlServer)
                {
                    switch (name)
                    {
                        case "webmotors":
                            database = new SqlServerDatabase(new DatabaseSettings("connection_maisfidelidade_sqlserver").ConnectionString);
                            break;
                        default:
                            throw new Exception("Unexpected Case");
                    }
                }

                return database;
            }
            catch (Exception exception)
            {
                throw new Exception("Erro ao instanciar a conexão", exception);
            }
        }

        #endregion
    }
}
