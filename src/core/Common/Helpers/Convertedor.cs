﻿using ChoETL;
using System.Text;

namespace maisfidelidade.cnc.imp.core.Common.Helpers
{
	public static class Convertedor
    {
        public static string CsvParaJSON(string csv)
        {
            var stringBuilder = new StringBuilder();

            using (var csvReader = ChoCSVReader.LoadText(csv).WithFirstLineHeader())
            {
                using (var jsonWriter = new ChoJSONWriter(stringBuilder))
                {
                    jsonWriter.Write(csvReader);
                }
            }

            return stringBuilder.ToString();
        }
    }
}
