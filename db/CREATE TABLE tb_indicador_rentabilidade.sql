CREATE TABLE tb_indicador_rentabilidade (
	id_indicador_rentabilidade  int unsigned not null auto_increment,
	id_indicador_rentabilidade_pai int unsigned,
	nome varchar(50) NOT NULL,
	descricao varchar(255) NOT NULL,
    tooltip varchar(100) NOT NULL,
    ativo tinyint(4) NOT NULL,
	dt_inclusao timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	dt_alteracao timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (id_indicador_rentabilidade), KEY fk_beneficio_beneficio1_idx (id_indicador_rentabilidade_pai),
	CONSTRAINT fk_beneficio__beneficio1 FOREIGN KEY (id_indicador_rentabilidade_pai) 
	REFERENCES tb_indicador_rentabilidade (id_indicador_rentabilidade) ON DELETE NO ACTION ON UPDATE NO ACTION
);


INSERT INTO tb_indicador_rentabilidade(nome, ativo,dt_inclusao,dt_alteracao)
VALUES
('Marketing', 1,NOW(),now()),
('Financiamentos', 1,NOW(),now()),
('Seguros', 1,NOW(),NOW());


SELECT  id_indicador_rentabilidade INTO @idIndicadorFinanciamentos
 FROM tb_indicador_rentabilidade where nome = 'Financiamentos' limit 1;
 
SELECT  id_indicador_rentabilidade INTO @idIndicadorSeguros
 FROM tb_indicador_rentabilidade where nome = 'Seguros' limit 1;

INSERT INTO tb_indicador_rentabilidade(id_indicador_rentabilidade_pai, nome, ativo,dt_inclusao,dt_alteracao)
VALUES
(@idIndicadorFinanciamentos,'Plus', 1,NOW(),now()),
(@idIndicadorFinanciamentos,'Retorno', 1,NOW(),now()),
(@idIndicadorSeguros, 'Prestamista', 1,NOW(),now()),
(@idIndicadorSeguros, 'Auto', 1,NOW(),NOW());


SELECT  id_indicador_rentabilidade INTO @idIndicadorMarketing
 FROM tb_indicador_rentabilidade where nome = 'Marketing' limit 1;
 
UPDATE tb_indicador_rentabilidade SET id_indicador_rentabilidade_pai=@idIndicadorMarketing where nome='Marketing';


CREATE TABLE tb_apuracao_indicador_rentabilidade (
	id_apuracao_indicador_rentabilidade  int unsigned not null auto_increment,
	id_apuracao int unsigned,
	id_indicador_rentabilidade int unsigned,
	realizado varchar(20) NOT NULL,
	maximo varchar(20) NOT NULL,
	dt_inclusao timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	dt_alteracao timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (id_apuracao_indicador_rentabilidade),
	CONSTRAINT fk_apuracaorentabilidade__apuracao FOREIGN KEY (id_apuracao) REFERENCES tb_apuracao (id_apuracao) ON DELETE NO ACTION ON UPDATE NO ACTION,
	CONSTRAINT fk_apuracaorentabilidade__beneficio foreign key (id_indicador_rentabilidade) references tb_indicador_rentabilidade (id_indicador_rentabilidade)
);



CREATE TABLE tb_beneficio_rentabilidade (
	id_beneficio_rentabilidade  int unsigned not null auto_increment,
	nome varchar(50) NOT NULL,
	descricao varchar(255) NOT NULL,
    tooltip varchar(100) NOT NULL,
    ativo tinyint(4) NOT NULL,
	dt_inclusao timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	dt_alteracao timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (id_beneficio_rentabilidade)
);


INSERT INTO tb_beneficio_rentabilidade ( nome, ativo,dt_inclusao,dt_alteracao)
VALUES
('Gravame', 1,NOW(),NOW()),
('Mesa de Crédito', 1,NOW(),NOW()),
('CRV', 1,NOW(),NOW());


CREATE TABLE tb_apuracao_beneficio_rentabilidade (
	id_apuracao_beneficio_rentabilidade  int unsigned not null auto_increment,
	id_apuracao int unsigned,
	id_beneficio_rentabilidade int unsigned,
	valor varchar(20) NOT NULL,
	dt_inclusao timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	dt_alteracao timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (id_apuracao_beneficio_rentabilidade),
	CONSTRAINT fk_apuracaorentabilidade__apuracao FOREIGN KEY (id_apuracao) REFERENCES tb_apuracao (id_apuracao) ON DELETE NO ACTION ON UPDATE NO ACTION,
	CONSTRAINT fk_apuracaorentabilidade__beneficio foreign key (id_beneficio_rentabilidade) references tb_beneficio_rentabilidade (id_beneficio_rentabilidade)
);


CREATE TABLE tb_matriz_indicador_rentabilidade (
	id_matriz_beneficio int unsigned not null auto_increment,
	id_matriz int unsigned not null,
	id_indicador_rentabilidade int unsigned not null,
	valor varchar(50) NOT NULL,
	dt_inclusao timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	dt_alteracao timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	constraint pk_matriz_beneficio primary key (id_matriz_beneficio),
	constraint fk_matrizbeneficio__matriz foreign key (id_matriz) references tb_matriz (id_matriz),
	constraint fk_matrizbeneficio__beneficio foreign key (id_indicador_rentabilidade) references tb_indicador_rentabilidade (id_indicador_rentabilidade)
);

SELECT  id_indicador_rentabilidade INTO @idIndicadorPlus
 FROM tb_indicador_rentabilidade where nome = 'Plus' limit 1;
 
 
SELECT  id_indicador_rentabilidade INTO @idIndicadorAuto
 FROM tb_indicador_rentabilidade where nome = 'Auto' limit 1;
 
 
SELECT  id_indicador_rentabilidade INTO @idIndicadorPrestamista
 FROM tb_indicador_rentabilidade where nome = 'Prestamista' limit 1;
 

INSERT INTO tb_matriz_beneficio
(id_matriz, id_indicador_rentabilidade, valor)
VALUES
(1, @idIndicadorPlus, '0,017'),
(17, @idIndicadorPlus, '0,017'),
(33, @idIndicadorPlus, '0,017'),
(1, @idIndicadorPrestamista, '0,04'),
(17, @idIndicadorPrestamista, '0,04'),
(33, @idIndicadorPrestamista, '0,04'),
(1, @idIndicadorAuto, '0,04'),
(17, @idIndicadorAuto, '0,04'),
(33, @idIndicadorAuto, '0,04'),
(2, @idIndicadorPlus, '0,017'),
(18, @idIndicadorPlus, '0,017'),
(34, @idIndicadorPlus, '0,017'),
(2, @idIndicadorPrestamista, '0,05'),
(18, @idIndicadorPrestamista, '0,05'),
(34, @idIndicadorPrestamista, '0,05'),
(2, @idIndicadorAuto, '0,05'),
(18, @idIndicadorAuto, '0,05'),
(34, @idIndicadorAuto, '0,05'),
(3, @idIndicadorPlus, '0,017'),
(19, @idIndicadorPlus, '0,017'),
(35, @idIndicadorPlus, '0,017'),
(3, @idIndicadorPrestamista, '0,05'),
(19, @idIndicadorPrestamista, '0,05'),
(35, @idIndicadorPrestamista, '0,05'),
(3, @idIndicadorAuto, '0,05'),
(19, @idIndicadorAuto, '0,05'),
(35, @idIndicadorAuto, '0,05'),
(4, @idIndicadorPlus, '0,017'),
(20, @idIndicadorPlus, '0,017'),
(36, @idIndicadorPlus, '0,017'),
(4, @idIndicadorPrestamista, '0,06'),
(20, @idIndicadorPrestamista, '0,06'),
(36, @idIndicadorPrestamista, '0,06'),
(4, @idIndicadorAuto, '0,06'),
(20, @idIndicadorAuto, '0,06'),
(36, @idIndicadorAuto, '0,06'),
(5, @idIndicadorPlus, '0,019'),
(21, @idIndicadorPlus, '0,019'),
(37, @idIndicadorPlus, '0,019'),
(5, @idIndicadorPrestamista, '0,07'),
(21, @idIndicadorPrestamista, '0,07'),
(37, @idIndicadorPrestamista, '0,07'),
(5, @idIndicadorAuto, '0,07'),
(21, @idIndicadorAuto, '0,07'),
(37, @idIndicadorAuto, '0,07'),
(6, @idIndicadorPlus, '0,019'),
(22, @idIndicadorPlus, '0,019'),
(38, @idIndicadorPlus, '0,019'),
(6, @idIndicadorPrestamista, '0,08'),
(22, @idIndicadorPrestamista, '0,08'),
(38, @idIndicadorPrestamista, '0,08'),
(6, @idIndicadorAuto, '0,07'),
(22, @idIndicadorAuto, '0,07'),
(38, @idIndicadorAuto, '0,07'),
(7, @idIndicadorPlus, '0,020'),
(23, @idIndicadorPlus, '0,020'),
(39, @idIndicadorPlus, '0,020'),
(7, @idIndicadorPrestamista, '0,09'),
(23, @idIndicadorPrestamista, '0,09'),
(39, @idIndicadorPrestamista, '0,09'),
(7, @idIndicadorAuto, '0,08'),
(23, @idIndicadorAuto, '0,08'),
(39, @idIndicadorAuto, '0,08'),
(8, @idIndicadorPlus, '0,020'),
(24, @idIndicadorPlus, '0,020'),
(40, @idIndicadorPlus, '0,020'),
(8, @idIndicadorPrestamista, '0,1'),
(24, @idIndicadorPrestamista, '0,1'),
(40, @idIndicadorPrestamista, '0,1'),
(8, @idIndicadorAuto, '0,08'),
(24, @idIndicadorAuto, '0,08'),
(40, @idIndicadorAuto, '0,08'),
(9, @idIndicadorPlus, '0,021'),
(25, @idIndicadorPlus, '0,021'),
(41, @idIndicadorPlus, '0,021'),
(9, @idIndicadorPrestamista, '0,09'),
(25, @idIndicadorPrestamista, '0,09'),
(41, @idIndicadorPrestamista, '0,09'),
(9, @idIndicadorAuto, '0,08'),
(25, @idIndicadorAuto, '0,08'),
(41, @idIndicadorAuto, '0,08'),
(10, @idIndicadorPlus, '0,024'),
(26, @idIndicadorPlus, '0,024'),
(42, @idIndicadorPlus, '0,024'),
(10, @idIndicadorPrestamista, '0,14'),
(26, @idIndicadorPrestamista, '0,14'),
(42, @idIndicadorPrestamista, '0,14'),
(10, @idIndicadorAuto, '0,12'),
(26, @idIndicadorAuto, '0,12'),
(42, @idIndicadorAuto, '0,12'),
(11, @idIndicadorPlus, '0,024'),
(27, @idIndicadorPlus, '0,024'),
(43, @idIndicadorPlus, '0,024'),
(11, @idIndicadorPrestamista, '0,15'),
(27, @idIndicadorPrestamista, '0,15'),
(43, @idIndicadorPrestamista, '0,15'),
(11, @idIndicadorAuto, '0,13'),
(27, @idIndicadorAuto, '0,13'),
(43, @idIndicadorAuto, '0,13'),
(12, @idIndicadorPlus, '0,024'),
(28, @idIndicadorPlus, '0,024'),
(44, @idIndicadorPlus, '0,024'),
(12, @idIndicadorPrestamista, '0,28'),
(28, @idIndicadorPrestamista, '0,28'),
(44, @idIndicadorPrestamista, '0,28'),
(12, @idIndicadorAuto, '0,19'),
(28, @idIndicadorAuto, '0,19'),
(44, @idIndicadorAuto, '0,19'),
(13, @idIndicadorPlus, '0,020'),
(29, @idIndicadorPlus, '0,020'),
(45, @idIndicadorPlus, '0,020'),
(13, @idIndicadorPrestamista, '0,09'),
(29, @idIndicadorPrestamista, '0,09'),
(45, @idIndicadorPrestamista, '0,09'),
(13, @idIndicadorAuto, '0,09'),
(29, @idIndicadorAuto, '0,09'),
(45, @idIndicadorAuto, '0,09'),
(14, @idIndicadorPlus, '0,024'),
(30, @idIndicadorPlus, '0,024'),
(46, @idIndicadorPlus, '0,024'),
(14, @idIndicadorPrestamista, '0,16'),
(30, @idIndicadorPrestamista, '0,16'),
(46, @idIndicadorPrestamista, '0,16'),
(14, @idIndicadorAuto, '0,13'),
(30, @idIndicadorAuto, '0,13'),
(46, @idIndicadorAuto, '0,13'),
(15, @idIndicadorPlus, '0,024'),
(31, @idIndicadorPlus, '0,024'),
(47, @idIndicadorPlus, '0,024'),
(15, @idIndicadorPrestamista, '0,3'),
(31, @idIndicadorPrestamista, '0,3'),
(47, @idIndicadorPrestamista, '0,3'),
(15, @idIndicadorAuto, '0,19'),
(31, @idIndicadorAuto, '0,19'),
(47, @idIndicadorAuto, '0,19'),
(16, @idIndicadorPlus, '0,024'),
(32, @idIndicadorPlus, '0,024'),
(48, @idIndicadorPlus, '0,024'),
(16, @idIndicadorPrestamista, '0,35'),
(32, @idIndicadorPrestamista, '0,35'),
(48, @idIndicadorPrestamista, '0,35'),
(16, @idIndicadorAuto, '0,19'),
(32, @idIndicadorAuto, '0,19'),
(48, @idIndicadorAuto, '0,19');
