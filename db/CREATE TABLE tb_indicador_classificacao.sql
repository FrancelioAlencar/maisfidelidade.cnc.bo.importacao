CREATE TABLE tb_indicador_classificacao (
	id_indicador_classificacao  int unsigned not null auto_increment,
	nome varchar(50) NOT NULL,
	descricao varchar(255) NOT NULL,
   tooltip varchar(100) NOT NULL,
   ativo tinyint(4) NOT NULL,
   id_entidade int unsigned not null,
	dt_inclusao timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	dt_alteracao timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (id_indicador_classificacao)
);


INSERT INTO tb_indicador_classificacao ( nome, ativo,dt_inclusao,dt_alteracao,id_entidade)
VALUES
('Market Share', 1,NOW(),NOW(),1),
('IP Seguros', 1,NOW(),NOW(),2);


CREATE TABLE tb_apuracao_indicador_classificacao (
	id_apuracao_indicador_classificacao  int unsigned not null auto_increment,
	id_apuracao int unsigned,
	id_indicador_classificacao int unsigned,
	valor varchar(20) NOT NULL,
	dt_inclusao timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	dt_alteracao timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (id_apuracao_indicador_classificacao),
	CONSTRAINT fk_apuracaoclassificacao__apuracao FOREIGN KEY (id_apuracao) REFERENCES tb_apuracao (id_apuracao) ON DELETE NO ACTION ON UPDATE NO ACTION,
	CONSTRAINT fk_apuracaoclassificacao__indicador foreign key (id_indicador_classificacao) references tb_indicador_classificacao (id_indicador_classificacao)
);
