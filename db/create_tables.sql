USE db_concessionaria;


INSERT INTO tb_perfil (id_perfil, nome, fl_ativo, dt_alteracao)
VALUES (2, 'GC', 1, now()),
	   (3, 'GR', 1, now());

CREATE TABLE tb_tipo_gerente (
	id_tipo_gerente int unsigned not null auto_increment,
    nome varchar(50) not null,
    sigla varchar(5) not null,
    dt_inclusao timestamp not null default current_timestamp,
    dt_alteracao timestamp not null default current_timestamp on update current_timestamp,
    constraint pk_tipo_gerente primary key (id_tipo_gerente)
);

INSERT INTO tb_tipo_gerente (id_tipo_gerente, nome, sigla)
VALUES (1, 'Gerente de Relacionamento', 'GR'),
	   (2, 'Gerente Comercial', 'GC');

CREATE TABLE tb_gerente (
    id_gerente int unsigned not null auto_increment,
    matricula int unsigned not null,
    id_tipo_gerente int unsigned not null,
    nome varchar(100) not null,
    email varchar(255) not null,
    telefone varchar(11) not null,
    ativo bit not null default 1,
    dt_inclusao timestamp not null default current_timestamp,
    dt_alteracao timestamp not null default current_timestamp on update current_timestamp,
    constraint pk_gerente primary key (id_gerente),
    constraint fk_tipo_gerente__gerente foreign key (id_tipo_gerente) references tb_tipo_gerente (id_tipo_gerente),
    constraint uq_matricula_gerente unique (matricula)
);


CREATE TABLE tb_tipo_banner (
	id_tipo_banner int unsigned not null auto_increment,
	nome varchar(50) not null,
    dt_inclusao timestamp not null default current_timestamp,
    dt_alteracao timestamp not null default current_timestamp on update current_timestamp,
    constraint pk_tipo_banner primary key (id_tipo_banner)
);

INSERT INTO tb_tipo_banner (id_tipo_banner, nome) 
VALUES (1, 'Imagem'),
	   (2, 'Imagem com Link'),
       (3, 'Html');

CREATE TABLE tb_banner (
	id_banner int unsigned not null auto_increment,    
    id_entidade int unsigned not null,
    id_tipo_banner int unsigned not null,
    nome varchar(50) not null,
    descricao varchar(255) not null,
    tooltip varchar(255) not null,
    dt_inicio_vigencia datetime not null,
    dt_fim_vigencia datetime not null,
    ativo bit not null default 1,
    dt_inclusao timestamp not null default current_timestamp,
    dt_alteracao timestamp not null default current_timestamp on update current_timestamp,
    constraint pk_banner primary key (id_indicador_performance),
    constraint fk_banner__tipo_banner foreign key (id_tipo_banner) references tb_tipo_banner (id_tipo_banner),
    constraint uq_id_entidade_banner unique key (id_entidade)
);


CREATE TABLE tb_tipo_indicador_performance (
	id_tipo_indicador_performance int unsigned not null auto_increment,
	nome varchar(50) not null,
    dt_inclusao timestamp not null default current_timestamp,
    dt_alteracao timestamp not null default current_timestamp on update current_timestamp,
    constraint pk_tipo_indicador_performance primary key (id_tipo_indicador_performance)
);

INSERT INTO tb_tipo_indicador_performance (id_tipo_indicador_performance, nome) 
VALUES (1, 'Com Percentual'),
	   (2, 'Sem Percentual');


CREATE TABLE IF NOT EXISTS tb_indicador_performance (
	id_indicador_performance int unsigned not null auto_increment,    
    id_entidade int unsigned not null,
    id_tipo_indicador_performance int unsigned not null,
    nome varchar(50) not null,
    descricao varchar(255) not null,
    tooltip varchar(255) not null,
    ativo bit not null default 1,
    dt_inclusao timestamp not null default current_timestamp,
    dt_alteracao timestamp not null default current_timestamp on update current_timestamp,
    constraint pk_indicador_performance primary key (id_indicador_performance),
    constraint fk_tipo_indicador_performance foreign key (id_tipo_indicador_performance) references tb_tipo_indicador_performance (id_tipo_indicador_performance),
    constraint uq_id_entidade_indicador_performance unique key (id_entidade)
);
 
INSERT INTO tb_indicador_performance (id_indicador_performance, id_entidade, id_tipo_indicador_performance, nome, descricao, tooltip) 
VALUES (1, 10, 2, 'Ambos', '', ''),
	   (2, 20, 1, 'Seguro Auto', '', ''),
       (3, 30, 1, 'Seguro Prestamista', '', '');

CREATE TABLE IF NOT EXISTS tb_indicador_performance_dado (
	id_indicador_performance_dado int unsigned not null auto_increment,
	id_entidade int unsigned not null,
    id_indicador_performance int unsigned not null,
    nome varchar(50) not null,
    descricao varchar(255) not null,
    tooltip varchar(255) not null,
    ativo bit not null default 1,    
    dt_inclusao timestamp not null default current_timestamp,
    dt_alteracao timestamp not null default current_timestamp on update current_timestamp,
    constraint pk_indicador_performance_dado primary key (id_indicador_performance_dado),
	constraint fk_indicador_performance foreign key (id_indicador_performance) references tb_indicador_performance (id_indicador_performance)
);

INSERT INTO tb_indicador_performance_dado (id_entidade, id_indicador_performance, nome, descricao, tooltip) 
VALUES (11, 1, 'Contratos', '', ''),
	   (21, 2, 'Contratos elegíveis', '', ''),
	   (22, 2, 'Contratos convertidos', '', ''),
	   (31, 3, 'Contratos elegíveis', '', ''),
	   (32, 3, 'Contratos converttidos', '', '');


CREATE TABLE IF NOT EXISTS tb_apuracao_indicador_performance (
	id_apuracao_indicador_performance int unsigned not null auto_increment,
	id_apuracao int unsigned not null,
	id_indicador_performance int unsigned not null,
    valor varchar(100) not null,
    dt_inclusao timestamp not null default current_timestamp,
    dt_alteracao timestamp not null default current_timestamp on update current_timestamp,
	constraint pk_apuracao_indicador_performance primary key (id_apuracao_indicador_performance),
    constraint fk_apuracao__apuracao_indicador_performance foreign key (id_apuracao) references tb_apuracao (id_apuracao),
	constraint fk_indicador_performance__apuracao_indicador_performance foreign key (id_indicador_performance) references tb_indicador_performance (id_indicador_performance)
);

CREATE TABLE IF NOT EXISTS tb_apuracao_indicador_performance_dado (
	id_apuracao_indicador_performance_dado int unsigned not null auto_increment,
	id_apuracao_indicador_performance int unsigned not null,
	id_indicador_performance_dado int unsigned not null,
    valor varchar(100) not null,
    dt_inclusao timestamp not null default current_timestamp,
    dt_alteracao timestamp not null default current_timestamp on update current_timestamp,
	constraint pk_apuracao_indicador_performance_dado primary key (id_apuracao_indicador_performance_dado),
    constraint fk_apuracao_indicador_performance foreign key (id_apuracao_indicador_performance) references tb_apuracao_indicador_performance (id_apuracao_indicador_performance),
	constraint fk_indicador_performance_dado foreign key (id_indicador_performance_dado) references tb_indicador_performance_dado (id_indicador_performance_dado)
);





CREATE TABLE tb_categoria (
	id_categoria int unsigned not null auto_increment,
    nome varchar(50) not null,
    descricao varchar(255) not null,
    ativo bit not null default 1,
    dt_inclusao timestamp not null default current_timestamp,
    dt_alteracao timestamp not null default current_timestamp on update current_timestamp,
    constraint pk_categoria primary key (id_categoria)
);

INSERT INTO db_concessionaria.tb_categoria (id_categoria, nome, descricao)
VALUES (1, "Bronze", ""),
	   (2, "Silver", ""),
       (3, "Gold", ""),
       (4, "Platinum", "");

/* TODO: Verificar pra não pode inserir id_segmento_pai igual a próprio id_segmento */
CREATE TABLE tb_segmento (
	id_segmento int unsigned not null auto_increment,
    id_segmento_pai int unsigned null,
    nome varchar(50) not null,
    codigo_santander int unsigned null,
    ativo bit not null default 1,
    dt_inclusao timestamp not null default current_timestamp,
    dt_alteracao timestamp not null default current_timestamp on update current_timestamp,
    constraint pk_segmento primary key (id_segmento),
    constraint fk_segmento__segmento foreign key (id_segmento_pai) references tb_segmento (id_segmento)
);

INSERT INTO db_concessionaria.tb_segmento (id_segmento, nome)
VALUES (1, 'REVENDAS');

INSERT INTO db_concessionaria.tb_segmento (id_segmento, id_segmento_pai, nome, codigo_santander)
VALUES (2, 1, 'BIG DEALERS', 149),
	   (3, 1, 'INTERMEDIATE DEALERS', 141),
	   (4, 1, 'REVENDAS VAREJO', 140);

CREATE TABLE tb_empresa (
	id_empresa int unsigned not null auto_increment,
    cnpj char(14) not null,
    nome_fantasia varchar(255) not null,
    nome_grupo varchar(255) not null,
    email varchar(255) not null,
    id_segmento int unsigned not null, 
	matricula_gr int unsigned not null,
    matricula_gc int unsigned not null,
    ativo bit not null default 1,
    guid char(38) not null,
    dt_inclusao timestamp not null default current_timestamp,
    dt_alteracao timestamp not null default current_timestamp on update current_timestamp,
    constraint pk_empresa primary key (id_empresa),
    constraint fk_segmento__empresa foreign key (id_segmento) references tb_segmento (id_segmento),
    constraint fk_gerente__empresa___matricula_gr foreign key (matricula_gr) references tb_gerente (matricula),
    constraint fk_gerente__empresa___matricula_gc foreign key (matricula_gc) references tb_gerente (matricula),
    constraint uq_cnpj_empresa unique key (cnpj)
);


/*
insert into db_concessionaria.tb_empresa (cnpj, nome_fantasia, nome_grupo, email, id_segmento, matricula_gr, matricula_gc)
values ('91359513000149', 'COMAUTO', 'AUTO MONTENEGRINA', 'wellbatera77@gmail.com', 2, 701971, 729367)
*/

CREATE TABLE IF NOT EXISTS tb_tipo_arquivo (
	id_tipo_arquivo int unsigned not null auto_increment,
    nome varchar(50) not null,
    dt_inclusao timestamp not null default current_timestamp,
    dt_alteracao timestamp not null default current_timestamp on update current_timestamp,
    constraint pk_tipo_arquivo primary key (id_tipo_arquivo)
);

INSERT INTO tb_tipo_arquivo (id_tipo_arquivo, nome)
VALUES (1, 'Semanal'),
	   (2, 'Mensal');

CREATE TABLE IF NOT EXISTS tb_arquivo (
	id_arquivo int unsigned not null auto_increment,
    id_arquivo_stage char(24) not null,
    nome varchar(100) not null,
    id_tipo_arquivo int unsigned not null,
    dt_processamento datetime not null,
    dt_referencia datetime not null,
    dt_previsao_atualizacao datetime not null,
    dt_importacao datetime not null,
    dt_publicacao datetime not null,
    total_linhas int unsigned not null default 0,
	ultimo_processado bit not null default 0,
    ativo bit not null default 1,
    dt_inclusao timestamp not null default current_timestamp,
    dt_alteracao timestamp not null default current_timestamp on update current_timestamp,
    constraint pk_arquivo primary key (id_arquivo),
	constraint fk_tipo_arquivo__arquivo foreign key (id_tipo_arquivo) references tb_tipo_arquivo (id_tipo_arquivo),
    constraint uq_id_arquivo_stage_arquivo unique key (id_arquivo_stage),
    constraint uq_nome_arquivo unique key (nome)
);

CREATE TABLE IF NOT EXISTS tb_apuracao (
	id_apuracao int unsigned not null auto_increment,
    id_arquivo int unsigned not null,
    id_empresa int unsigned not null,
    id_categoria int unsigned not null,
    financiado_santander decimal(10,2) not null,
	financiado_loja decimal(10,2) not null,
    qtd_contratos int unsigned not null,
    qtd_prestamista int unsigned not null,
    qtd_auto int unsigned not null,
    qtd_ambos int unsigned not null,    
    plus_mes_anterior decimal(10,2) not null,     
    plus_mes_vigente decimal(10,2) not null,
    plus_proximo_mes decimal(10,2) not null,
    auto_mes_anterior decimal(10,2) not null,    
    auto_mes_vigente decimal(10,2) not null,
    auto_proximo_mes decimal(10,2) not null,
    prestamista_mes_anterior decimal(10,2) not null,    
    prestamista_mes_vigente decimal(10,2) not null,
    prestamista_proximo_mes decimal(10,2) not null,
    guid char(38) not null,
    dt_inclusao timestamp not null default current_timestamp,
    dt_alteracao timestamp not null default current_timestamp on update current_timestamp,
    constraint pk_apuracao primary key (id_apuracao),
    constraint fk_arquivo__apuracao foreign key (id_arquivo) references tb_arquivo (id_arquivo),
    constraint fk_empresa__apuracao foreign key (id_empresa) references tb_empresa (id_empresa),
    constraint fk_categoria__apuracao foreign key (id_categoria) references tb_categoria (id_categoria),
    constraint uq_guid_apuracao unique key (guid)
);






CREATE TABLE tb_beneficio (
	id_beneficio int unsigned not null auto_increment,
    id_entidade int unsigned not null,
    nome varchar(50) not null,
    descricao varchar(255) not null,
    tooltip varchar(100) not null,
    ativo bit not null default 1,
    dt_inclusao timestamp not null default current_timestamp,
    dt_alteracao timestamp not null default current_timestamp on update current_timestamp,
    constraint pk_beneficio primary key (id_beneficio),
    constraint uq_id_entidade_beneficio unique (id_entidade)
);

INSERT INTO tb_beneficio (id_entidade, nome, descricao, tooltip)
VALUES (1, "Campanha de Marketing", "", ""),
	   (2, "Comissão Plus", "", ""),
       (3, "Retorno", "", ""),
       (4, "Comissão Prestamista", "", ""),
       (5, "Comissão Seguro Auto", "", ""),
       (6, "Pagamento Sem Gravame", "", ""),
       (7, "Pagamento Sem Documento", "", ""),
       (8, "Floorplan Usados", "", ""),
       (9, "Mesa de Crédito (Buyer)", "", "");

CREATE TABLE tb_criterio (
	id_criterio int unsigned not null auto_increment,
    id_entidade int unsigned not null,
    nome varchar(50) not null,
    descricao varchar(255) not null,
    tooltip varchar(100) not null,
    ativo bit not null default 1,
    dt_inclusao timestamp not null default current_timestamp,
    dt_alteracao timestamp not null default current_timestamp on update current_timestamp,
    constraint pk_criterio primary key (id_criterio),
    constraint uq_criterio unique (id_entidade)
);

INSERT INTO tb_criterio (id_entidade, nome, descricao, tooltip)
VALUES (1, "Market Share", "", ""),
	   (2, "IP Seguros", "", "");










CREATE TABLE tb_indicador_performance (
	id_indicador_performance int unsigned auto_increment,
    id_entidade int unsigned,
    nome varchar(50) not null,
    descricao varchar(255) not null,
    tooltip varchar(100) not null,
    ativo bit not null default 1,
    dt_inclusao timestamp not null default current_timestamp,
    dt_alteracao timestamp not null default current_timestamp on update current_timestamp,
    constraint pk_indicador_performance primary key (id_indicador_performance),
    constraint uq_indicador_performance unique (id_entidade)
);

INSERT INTO tb_indicador_performance (id_entidade, nome, descricao, tooltip)
VALUES (1, "Financiamento", "", ""),
	   (2, "Seguro Auto", "", ""),
       (3, "Seguro Prestamista", "", "");
       





/***********

select * from tb_arquivo;
select * from tb_apuracao;

select * from tb_categoria;
select * from tb_criterio;
select * from tb_beneficio;
select * from tb_indicador_performance;

drop table tb_indicador_performance;
drop table tb_gerente

1. drop table tb_empresa;
2. drop table tb_segmento;


drop table tb_apuracao;

drop table tb_arquivo;
drop table tb_categoria;
drop table tb_criterio;
drop table tb_beneficio;

update tb_criterio
set nome = "IP Seguros"
where id_entidade = 2

**********/	
