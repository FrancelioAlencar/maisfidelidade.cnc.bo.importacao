SET FOREIGN_KEY_CHECKS = 0;
SET FOREIGN_KEY_CHECKS = 1;
SET SQL_SAFE_UPDATES = 0;
SET SQL_SAFE_UPDATES = 1;

SET @@session.time_zone = 'America/Sao_Paulo';

select * from mysql.time_zone_name;

/* Apuração */
select * from tb_arquivo;
select * from tb_apuracao where id_arquivo = 32;

select * from tb_apuracao_indicador_classificacao;
select * from tb_apuracao_indicador_performance;
select * from tb_apuracao_indicador_performance_dado;
select * from tb_apuracao_indicador_rentabilidade;
select * from tb_apuracao_beneficio_rentabilidade;


select * from tb_tipo_indicador_performance;
select * from tb_indicador_performance;
select * from tb_indicador_performance_dado;



delete from tb_apuracao_indicador_classificacao;
delete from tb_apuracao_indicador_rentabilidade;

drop table tb_tipo_indicador_performance;
drop table tb_indicador_performance;
drop table tb_indicador_performance_dado;

drop table tb_apuracao_indicador_performance;
drop table tb_apuracao_indicador_performance_dado;

update tb_arquivo
set id_arquivo_stage = uuid()
where id_arquivo = 17;