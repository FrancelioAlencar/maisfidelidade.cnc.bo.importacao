use db_concessionaria

if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"NO_CNPJ"},{"_id_coluna": 1}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaCNPJ",
        _id_coluna: NumberInt(1),
        _id_coluna_tipo: NumberInt(6),
        coluna_grupos: [ NumberInt(1), NumberInt(10), NumberInt(11), NumberInt(12), NumberInt(13), NumberInt(14), NumberInt(15), NumberInt(16) ],
        nome: "NO_CNPJ",
        preenchimento_obrigatorio: true,
        ativo: true,		
        opcoes: []
    })
}

if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"NO_TAB"},{"_id_coluna": 2}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaNumeroInteiro",
        _id_coluna: NumberInt(2),
        _id_coluna_tipo: NumberInt(2),
        coluna_grupos: [ NumberInt(1), NumberInt(10) ],
        nome: "NO_TAB",
        preenchimento_obrigatorio: true,
        ativo: true,		
        opcoes: []
    })
}

if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"NM_FANTASIA"},{"_id_coluna": 3}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaTexto",
        _id_coluna: NumberInt(3),
        _id_coluna_tipo: NumberInt(1),
        coluna_grupos: [ NumberInt(1), NumberInt(10) ],
        nome: "NM_FANTASIA",
        preenchimento_obrigatorio: true,
        ativo: true,		
        opcoes: []
    })
}

if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"NM_GRUPO"},{"_id_coluna": 4}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaTexto",
        _id_coluna: NumberInt(4),
        _id_coluna_tipo: NumberInt(1),
        coluna_grupos: [ NumberInt(1), NumberInt(10) ],
        nome: "NM_GRUPO",
        preenchimento_obrigatorio: true,
        ativo: true,		
        opcoes: []
    })
}

if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"NM_SEGMENTO"},{"_id_coluna": 5}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaTexto",
        _id_coluna: NumberInt(5),
        _id_coluna_tipo: NumberInt(1),
        coluna_grupos: [ NumberInt(1), NumberInt(10) ],
        nome: "NM_SEGMENTO",
        preenchimento_obrigatorio: true,
        ativo: true,		
        opcoes: ["BIG DEALERS", "INTERMEDIATE DEALERS", "REVENDAS VAREJO"]
    })
}

if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"DS_EMAIL"},{"_id_coluna": 6}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaEmail",
        _id_coluna: NumberInt(6),
        _id_coluna_tipo: NumberInt(7),
        coluna_grupos: [ NumberInt(1), NumberInt(10) ],
        nome: "DS_EMAIL",
        preenchimento_obrigatorio: true,
        ativo: true,		
        opcoes: []
    })
}

if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"DT_PROCESSAMENTO"},{"_id_coluna": 7}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaData",
        _id_coluna: NumberInt(7),
        _id_coluna_tipo: NumberInt(5),
        coluna_grupos: [ NumberInt(1) ],
        nome: "DT_PROCESSAMENTO",
        preenchimento_obrigatorio: true,
        ativo: true,		
        opcoes: []
    })
}

if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"DT_REFERENCIA"},{"_id_coluna": 8}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaData",
        _id_coluna: NumberInt(8),
        _id_coluna_tipo: NumberInt(5),
        coluna_grupos: [ NumberInt(1) ],
        nome: "DT_REFERENCIA",
        preenchimento_obrigatorio: true,
        ativo: true,		
        opcoes: []
    })
}

if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"REF_ARQUIVO"},{"_id_coluna": 9}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaTexto",
        _id_coluna: NumberInt(9),
        _id_coluna_tipo: NumberInt(1),
        coluna_grupos: [ NumberInt(1) ],
        nome: "REF_ARQUIVO",
        preenchimento_obrigatorio: true,
        ativo: true,		
        opcoes: ["S", "M"]
    })
}

if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"DT_PREVISAO_ATUALIZACAO"},{"_id_coluna": 10}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaData",
        _id_coluna: NumberInt(10),
        _id_coluna_tipo: NumberInt(5),
        coluna_grupos: [ NumberInt(1) ],
        nome: "DT_PREVISAO_ATUALIZACAO",
        preenchimento_obrigatorio: true,
        ativo: true,		
        opcoes: []
    })
}

if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"FINANCIADO_SANTANDER"},{"_id_coluna": 11}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaNumeroDecimal",
        _id_coluna: NumberInt(11),
        _id_coluna_tipo: NumberInt(3),
        coluna_grupos: [ NumberInt(12) ],
        nome: "FINANCIADO_SANTANDER",
        preenchimento_obrigatorio: true,
        ativo: true,		
        opcoes: []
    })
}

if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"FINANCIADO_LOJA"},{"_id_coluna": 12}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaNumeroDecimal",
        _id_coluna: NumberInt(12),
        _id_coluna_tipo: NumberInt(3),
        coluna_grupos: [ NumberInt(12) ],
        nome: "FINANCIADO_LOJA",
        preenchimento_obrigatorio: true,
        ativo: true,		
        opcoes: []
    })
}

if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"PREMIO_PRESTAMISTA"},{"_id_coluna": 13}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaNumeroInteiro",
        _id_coluna: NumberInt(13),
        _id_coluna_tipo: NumberInt(2),
        coluna_grupos: [ NumberInt(1) ],
        nome: "PREMIO_PRESTAMISTA",
        preenchimento_obrigatorio: true,
        ativo: true,		
        opcoes: []
    })
}

if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"PREMIO_AUTO"},{"_id_coluna": 14}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaNumeroInteiro",
        _id_coluna: NumberInt(14),
        _id_coluna_tipo: NumberInt(2),
        coluna_grupos: [ NumberInt(1) ],
        nome: "PREMIO_AUTO",
        preenchimento_obrigatorio: true,
        ativo: true,		
        opcoes: []
    })
}

if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"PRODUCAO_REAL"},{"_id_coluna": 15}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaNumeroInteiro",
        _id_coluna: NumberInt(15),
        _id_coluna_tipo: NumberInt(2),
        coluna_grupos: [ NumberInt(1) ],
        nome: "PRODUCAO_REAL",
        preenchimento_obrigatorio: true,
        ativo: true,		
        opcoes: []
    })
}

if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"QTD_CONTRATOS"},{"_id_coluna": 16}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaNumeroInteiro",
        _id_coluna: NumberInt(16),
        _id_coluna_tipo: NumberInt(2),
        coluna_grupos: [ NumberInt(12) ],
        nome: "QTD_CONTRATOS",
        preenchimento_obrigatorio: true,
        ativo: true,		
        opcoes: []
    })
}

if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"QTD_PRESTAMISTAS"},{"_id_coluna": 17}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaNumeroInteiro",
        _id_coluna: NumberInt(17),
        _id_coluna_tipo: NumberInt(2),
        coluna_grupos: [ NumberInt(12) ],
        nome: "QTD_PRESTAMISTAS",
        preenchimento_obrigatorio: true,
        ativo: true,		
        opcoes: []
    })
}

if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"QTD_AUTO"},{"_id_coluna": 18}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaNumeroInteiro",
        _id_coluna: NumberInt(18),
        _id_coluna_tipo: NumberInt(2),
        coluna_grupos: [ NumberInt(12) ],
        nome: "QTD_AUTO",
        preenchimento_obrigatorio: true,
        ativo: true,		
        opcoes: []
    })
}

if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"QTD_AMBOS"},{"_id_coluna": 19}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaNumeroInteiro",
        _id_coluna: NumberInt(19),
        _id_coluna_tipo: NumberInt(2),
        coluna_grupos: [ NumberInt(12) ],
        nome: "QTD_AMBOS",
        preenchimento_obrigatorio: true,
        ativo: true,		
        opcoes: []
    })
}

if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"MARKET_SHARE_PC"},{"_id_coluna": 20}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaNumeroInteiro",
        _id_coluna: NumberInt(20),
        _id_coluna_tipo: NumberInt(2),
        coluna_grupos: [ NumberInt(1) ],
        nome: "MARKET_SHARE_PC",
        preenchimento_obrigatorio: true,
        ativo: true,		
        opcoes: []
    })
}

if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"IPS_PC"},{"_id_coluna": 21}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaNumeroInteiro",
        _id_coluna: NumberInt(21),
        _id_coluna_tipo: NumberInt(2),
        coluna_grupos: [ NumberInt(1) ],
        nome: "IPS_PC",
        preenchimento_obrigatorio: true,
        ativo: true,		
        opcoes: []
    })
}

if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"TIPO_GRUPO"},{"_id_coluna": 22}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaTexto",
        _id_coluna: NumberInt(22),
        _id_coluna_tipo: NumberInt(1),
        coluna_grupos: [ NumberInt(1) ],
        nome: "TIPO_GRUPO",
        preenchimento_obrigatorio: true,
        ativo: true,		
        opcoes: []
    })
}

if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"MATRICULA_GR"},{"_id_coluna": 23}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaNumeroInteiro",
        _id_coluna: NumberInt(23),
        _id_coluna_tipo: NumberInt(2),
        coluna_grupos: [ NumberInt(10), NumberInt(11) ],
        nome: "MATRICULA_GR",
        preenchimento_obrigatorio: true,
        ativo: true,		
        opcoes: []
    })
}

if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"NOME_GR"},{"_id_coluna": 24}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaTexto",
        _id_coluna: NumberInt(24),
        _id_coluna_tipo: NumberInt(1),
        coluna_grupos: [ NumberInt(11) ],
        nome: "NOME_GR",
        preenchimento_obrigatorio: true,
        ativo: true,		
        opcoes: []
    })
}

if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"EMAIL_GR"},{"_id_coluna": 25}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaEmail",
        _id_coluna: NumberInt(25),
        _id_coluna_tipo: NumberInt(7),
        coluna_grupos: [ NumberInt(11) ],
        nome: "EMAIL_GR",
        preenchimento_obrigatorio: true,
        ativo: true,		
        opcoes: []
    })
}

if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"TELEFONE_GR"},{"_id_coluna": 26}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaTelefone",
        _id_coluna: NumberInt(26),
        _id_coluna_tipo: NumberInt(8),
        coluna_grupos: [ NumberInt(11) ],
        nome: "TELEFONE_GR",
        preenchimento_obrigatorio: true,
        ativo: true,		
        opcoes: []
    })
}

if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"MATRICULA_GC"},{"_id_coluna": 27}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaNumeroInteiro",
        _id_coluna: NumberInt(27),
        _id_coluna_tipo: NumberInt(2),
        coluna_grupos: [ NumberInt(10), NumberInt(11) ],
        nome: "MATRICULA_GC",
        preenchimento_obrigatorio: true,
        ativo: true,		
        opcoes: []
    })
}

if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"NOME_GC"},{"_id_coluna": 28}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaTexto",
        _id_coluna: NumberInt(28),
        _id_coluna_tipo: NumberInt(1),
        coluna_grupos: [ NumberInt(11) ],
        nome: "NOME_GC",
        preenchimento_obrigatorio: true,
        ativo: true,		
        opcoes: []
    })
}

if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"EMAIL_GC"},{"_id_coluna": 29}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaEmail",
        _id_coluna: NumberInt(29),
        _id_coluna_tipo: NumberInt(7),
        coluna_grupos: [ NumberInt(11) ],
        nome: "EMAIL_GC",
        preenchimento_obrigatorio: true,
        ativo: true,		
        opcoes: []
    })
}

if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"TELEFONE_GC"},{"_id_coluna": 30}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaTelefone",
        _id_coluna: NumberInt(30),
        _id_coluna_tipo: NumberInt(8),
        coluna_grupos: [ NumberInt(11) ],
        nome: "TELEFONE_GC",
        preenchimento_obrigatorio: true,
        ativo: true,		
        opcoes: []
    })
}

if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"FINANCIAMENTO_SANTANDER"},{"_id_coluna": 31}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaNumeroDecimal",
        _id_coluna: NumberInt(31),
        _id_coluna_tipo: NumberInt(3),
        coluna_grupos: [ NumberInt(13) ],
        nome: "FINANCIAMENTO_SANTANDER",
        preenchimento_obrigatorio: true,
        ativo: false,
        opcoes: []
    })
}

if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"FINANCIAMENTO_CONCESSIONARIA"},{"_id_coluna": 32}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaNumeroDecimal",
        _id_coluna: NumberInt(32),
        _id_coluna_tipo: NumberInt(3),
        coluna_grupos: [ NumberInt(13) ],
        nome: "FINANCIAMENTO_CONCESSIONARIA",
        preenchimento_obrigatorio: true,
        ativo: false,		
        opcoes: []
    })
}

if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"SEGURO_AUTO_PC"},{"_id_coluna": 33}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaNumeroDecimal",
        _id_coluna: NumberInt(33),
        _id_coluna_tipo: NumberInt(3),
        coluna_grupos: [ NumberInt(13) ],
        nome: "SEGURO_AUTO_PC",
        preenchimento_obrigatorio: true,
        ativo: true,		
        opcoes: []
    })
}

if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"SEGURO_AUTO_CONTRATOS_ELEGIVEIS"},{"_id_coluna": 34}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaNumeroInteiro",
        _id_coluna: NumberInt(34),
        _id_coluna_tipo: NumberInt(2),
        coluna_grupos: [ NumberInt(13) ],
        nome: "SEGURO_AUTO_CONTRATOS_ELEGIVEIS",
        preenchimento_obrigatorio: true,
        ativo: true,		
        opcoes: []
    })
}

if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"SEGURO_AUTO_CONTRATOS_CONVERTIDOS"},{"_id_coluna": 35}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaNumeroInteiro",
        _id_coluna: NumberInt(35),
        _id_coluna_tipo: NumberInt(2),
        coluna_grupos: [ NumberInt(13) ],
        nome: "SEGURO_AUTO_CONTRATOS_CONVERTIDOS",
        preenchimento_obrigatorio: true,
        ativo: true,		
        opcoes: []
    })
}

if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"SEGURO_PRESTAMISTA_PC"},{"_id_coluna": 36}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaNumeroDecimal",
        _id_coluna: NumberInt(36),
        _id_coluna_tipo: NumberInt(3),
        coluna_grupos: [ NumberInt(13) ],
        nome: "SEGURO_PRESTAMISTA_PC",
        preenchimento_obrigatorio: true,
        ativo: true,		
        opcoes: []
    })
}

if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"SEGURO_PRESTAMISTA_CONTRATOS_ELEGIVEIS"},{"_id_coluna": 37}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaNumeroInteiro",
        _id_coluna: NumberInt(37),
        _id_coluna_tipo: NumberInt(2),
        coluna_grupos: [ NumberInt(13) ],
        nome: "SEGURO_PRESTAMISTA_CONTRATOS_ELEGIVEIS",
        preenchimento_obrigatorio: true,
        ativo: true,		
        opcoes: []
    })
}

if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"SEGURO_PRESTAMISTA_CONTRATOS_CONVERTIDOS"},{"_id_coluna": 38}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaNumeroInteiro",
        _id_coluna: NumberInt(38),
        _id_coluna_tipo: NumberInt(2),
        coluna_grupos: [ NumberInt(13) ],
        nome: "SEGURO_PRESTAMISTA_CONTRATOS_CONVERTIDOS",
        preenchimento_obrigatorio: true,
        ativo: true,		
        opcoes: []
    })
}

if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"PLUS_MES_VIGENTE"},{"_id_coluna": 39}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaNumeroDecimal",
        _id_coluna: NumberInt(39),
        _id_coluna_tipo: NumberInt(3),
        coluna_grupos: [ NumberInt(12) ],
        nome: "PLUS_MES_VIGENTE",
        preenchimento_obrigatorio: true,
        ativo: true,        
        opcoes: []
    })
}
 
if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"PLUS_MES_ANTERIOR"},{"_id_coluna": 40}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaNumeroDecimal",
        _id_coluna: NumberInt(40),
        _id_coluna_tipo: NumberInt(3),
        coluna_grupos: [ NumberInt(12) ],
        nome: "PLUS_MES_ANTERIOR",
        preenchimento_obrigatorio: true,
        ativo: true,        
        opcoes: []
    })
}
 
if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"PLUS_PROXIMO_MES"},{"_id_coluna": 41}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaNumeroDecimal",
        _id_coluna: NumberInt(41),
        _id_coluna_tipo: NumberInt(3),
        coluna_grupos: [ NumberInt(12) ],
        nome: "PLUS_PROXIMO_MES",
        preenchimento_obrigatorio: true,
        ativo: true,        
        opcoes: []
    })
}

if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"AUTO_MES_VIGENTE"},{"_id_coluna": 42}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaNumeroDecimal",
        _id_coluna: NumberInt(42),
        _id_coluna_tipo: NumberInt(3),
        coluna_grupos: [ NumberInt(12) ],
        nome: "AUTO_MES_VIGENTE",
        preenchimento_obrigatorio: true,
        ativo: true,        
        opcoes: []
    })
}
 
if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"AUTO_MES_ANTERIOR"},{"_id_coluna": 43}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaNumeroDecimal",
        _id_coluna: NumberInt(43),
        _id_coluna_tipo: NumberInt(3),
        coluna_grupos: [ NumberInt(12) ],
        nome: "AUTO_MES_ANTERIOR",
        preenchimento_obrigatorio: true,
        ativo: true,        
        opcoes: []
    })
}
 
if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"AUTO_PROXIMO_MES"},{"_id_coluna": 44}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaNumeroDecimal",
        _id_coluna: NumberInt(44),
        _id_coluna_tipo: NumberInt(3),
        coluna_grupos: [ NumberInt(12) ],
        nome: "AUTO_PROXIMO_MES",
        preenchimento_obrigatorio: true,
        ativo: true,        
        opcoes: []
    })
}

if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"PRESTAMISTA_MES_VIGENTE"},{"_id_coluna": 45}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaNumeroDecimal",
        _id_coluna: NumberInt(45),
        _id_coluna_tipo: NumberInt(3),
        coluna_grupos: [ NumberInt(12) ],
        nome: "PRESTAMISTA_MES_VIGENTE",
        preenchimento_obrigatorio: true,
        ativo: true,        
        opcoes: []
    })
}
 
if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"PRESTAMISTA_MES_ANTERIOR"},{"_id_coluna": 46}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaNumeroDecimal",
        _id_coluna: NumberInt(46),
        _id_coluna_tipo: NumberInt(3),
        coluna_grupos: [ NumberInt(12) ],
        nome: "PRESTAMISTA_MES_ANTERIOR",
        preenchimento_obrigatorio: true,
        ativo: true,        
        opcoes: []
    })
}
 
if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"PRESTAMISTA_PROXIMO_MES"},{"_id_coluna": 47}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaNumeroDecimal",
        _id_coluna: NumberInt(47),
        _id_coluna_tipo: NumberInt(3),
        coluna_grupos: [ NumberInt(12) ],
        nome: "PRESTAMISTA_PROXIMO_MES",
        preenchimento_obrigatorio: true,
        ativo: true,        
        opcoes: []
    })
}

if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"CATEGORIA_ATUAL"},{"_id_coluna": 48}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaTexto",
        _id_coluna: NumberInt(48),
        _id_coluna_tipo: NumberInt(1),
        coluna_grupos: [ NumberInt(12) ],
        nome: "CATEGORIA_ATUAL",
        preenchimento_obrigatorio: true,
        ativo: true,		
        opcoes: ["BRONZE", "SILVER", "GOLD", "PLATINUM"]
    })
}
 
if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"FINANCIAMENTOS_REALIZADO"},{"_id_coluna":49}]}).count() == 0) {

    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaNumeroDecimal",
        _id_coluna: NumberInt(49),
        _id_coluna_tipo: NumberInt(3),
        coluna_grupos: [ NumberInt(14) ],
        nome: "FINANCIAMENTOS_REALIZADO",
        preenchimento_obrigatorio: true,
        ativo: true,        
        opcoes: []
    })
}

if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"FINANCIAMENTOS_MAXIMA"},{"_id_coluna":50}]}).count() == 0) {

    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaNumeroDecimal",
        _id_coluna: NumberInt(50),
        _id_coluna_tipo: NumberInt(3),
        coluna_grupos: [ NumberInt(14) ],
        nome: "FINANCIAMENTOS_MAXIMA",
        preenchimento_obrigatorio: true,
        ativo: true,        
        opcoes: []
    })
}
 
if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"SEGUROS_REALIZADO"},{"_id_coluna":51}]}).count() == 0) {

    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaNumeroDecimal",
        _id_coluna: NumberInt(51),
        _id_coluna_tipo: NumberInt(3),
        coluna_grupos: [ NumberInt(14) ],
        nome: "SEGUROS_REALIZADO",
        preenchimento_obrigatorio: true,
        ativo: true,        
        opcoes: []
    })
}

if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"SEGUROS_MAXIMA"},{"_id_coluna":52}]}).count() == 0) {

    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaNumeroDecimal",
        _id_coluna: NumberInt(52),
        _id_coluna_tipo:NumberInt(3),
        coluna_grupos: [ NumberInt(14) ],
        nome:"SEGUROS_MAXIMA",
        preenchimento_obrigatorio:true,
        ativo:true,        
        opcoes: []
    })
}
 
if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"PLUS_REALIZADO"},{"_id_coluna":53}]}).count() == 0) {

    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaNumeroDecimal",
        _id_coluna: NumberInt(53),
        _id_coluna_tipo: NumberInt(3),
        coluna_grupos: [ NumberInt(14) ],
        nome: "PLUS_REALIZADO",
        preenchimento_obrigatorio: true,
        ativo: true,        
        opcoes: []
    })
}
 
if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"PLUS_MAXIMA"},{"_id_coluna":54}]}).count() == 0) {

    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaNumeroDecimal",
        _id_coluna: NumberInt(54),
        _id_coluna_tipo: NumberInt(3),
        coluna_grupos: [ NumberInt(14) ],
        nome: "PLUS_MAXIMA",
        preenchimento_obrigatorio: true,
        ativo: true,        
        opcoes: []
    })
}

    

 
if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"RETORNO_REALIZADO"},{"_id_coluna":55}]}).count() == 0) {

    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaNumeroDecimal",
        _id_coluna: NumberInt(55),
        _id_coluna_tipo: NumberInt(3),
        coluna_grupos: [ NumberInt(14) ],
        nome: "RETORNO_REALIZADO",
        preenchimento_obrigatorio: true,
        ativo: true,        
        opcoes: []
    })
}
 
if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"RETORNO_MAXIMA"},{"_id_coluna":56}]}).count() == 0) {

    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaNumeroDecimal",
        _id_coluna: NumberInt(56),
        _id_coluna_tipo: NumberInt(3),
        coluna_grupos: [ NumberInt(14) ],
        nome: "RETORNO_MAXIMA",
        preenchimento_obrigatorio: true,
        ativo: true,        
        opcoes: []
    })
}
 
if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"PRESTAMISTA_REALIZADO"},{"_id_coluna":57}]}).count() == 0) {

    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaNumeroDecimal",
        _id_coluna: NumberInt(57),
        _id_coluna_tipo: NumberInt(3),
        coluna_grupos: [ NumberInt(14) ],
        nome: "PRESTAMISTA_REALIZADO",
        preenchimento_obrigatorio: true,
        ativo: true,        
        opcoes: []
    })
}
 
if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"PRESTAMISTA_MAXIMA"},{"_id_coluna":58}]}).count() == 0) {

    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaNumeroDecimal",
        _id_coluna: NumberInt(58),
        _id_coluna_tipo: NumberInt(3),
        coluna_grupos: [ NumberInt(14) ],
        nome: "PRESTAMISTA_MAXIMA",
        preenchimento_obrigatorio: true,
        ativo: true,        
        opcoes: []
    })
}

if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"AUTO_REALIZADO"},{"_id_coluna":59}]}).count() == 0) {

    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaNumeroDecimal",
        _id_coluna: NumberInt(59),
        _id_coluna_tipo: NumberInt(3),
        coluna_grupos: [ NumberInt(14) ],
        nome: "AUTO_REALIZADO",
        preenchimento_obrigatorio: true,
        ativo: true,        
        opcoes: []
    })
}
 
if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"AUTO_MAXIMA"},{"_id_coluna":60}]}).count() == 0) {

    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaNumeroDecimal",
        _id_coluna: NumberInt(60),
        _id_coluna_tipo: NumberInt(3),
        coluna_grupos: [ NumberInt(14) ],
        nome: "AUTO_MAXIMA",
        preenchimento_obrigatorio: true,
        ativo: true,        
        opcoes: []
    })
}
 
if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"POSSUI_MARKETING"},{"_id_coluna":61}]}).count() == 0) {

    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaBooleano",
        _id_coluna: NumberInt(61),
        _id_coluna_tipo: NumberInt(4),
        coluna_grupos: [ NumberInt(14) ],
        nome: "POSSUI_MARKETING",
        preenchimento_obrigatorio: true,
        ativo: true,        
        opcoes: []
    })
}

if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"POSSUI_CRV"},{"_id_coluna": 62}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaBooleano",
        _id_coluna: NumberInt(62),
        _id_coluna_tipo: NumberInt(4),
        coluna_grupos: [ NumberInt(14) ],
        nome: "POSSUI_CRV",
        preenchimento_obrigatorio: true,
        ativo: true,        
        opcoes: []
    })
}
 
if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"POSSUI_MESA_CREDITO"},{"_id_coluna": 63}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaBooleano",
        _id_coluna: NumberInt(63),
        _id_coluna_tipo: NumberInt(4),
        coluna_grupos: [ NumberInt(14) ],
        nome: "POSSUI_MESA_CREDITO",
        preenchimento_obrigatorio: true,
        ativo: true,        
        opcoes: []
    })
}
 
if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"QTD_GRAVAME"},{"_id_coluna": 64}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaNumeroInteiro",
        _id_coluna: NumberInt(64),
        _id_coluna_tipo: NumberInt(2),
        coluna_grupos: [ NumberInt(14) ],
        nome: "QTD_GRAVAME",
        preenchimento_obrigatorio: true,
        ativo: true,        
        opcoes: []
    })
}

if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"AMBOS_CONTRATOS"},{"_id_coluna": 65}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaNumeroInteiro",
        _id_coluna: NumberInt(65),
        _id_coluna_tipo: NumberInt(2),
        coluna_grupos: [ NumberInt(13) ],
        nome: "AMBOS_CONTRATOS",
        preenchimento_obrigatorio: true,
        ativo: true,        
        opcoes: []
    })
}

if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"CONSOLIDADO_REALIZADO"},{"_id_coluna":66}]}).count() == 0) {

    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaNumeroDecimal",
        _id_coluna: NumberInt(66),
        _id_coluna_tipo: NumberInt(3),
        coluna_grupos: [ NumberInt(14) ],
        nome: "CONSOLIDADO_REALIZADO",
        preenchimento_obrigatorio: true,
        ativo: true,
        opcoes: []
    })
}

if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"CONSOLIDADO_MAXIMA"},{"_id_coluna":67}]}).count() == 0) {

    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaNumeroDecimal",
        _id_coluna: NumberInt(67),
        _id_coluna_tipo: NumberInt(3),
        coluna_grupos: [ NumberInt(14) ],
        nome: "CONSOLIDADO_MAXIMA",
        preenchimento_obrigatorio: true,
        ativo: true,
        opcoes: []
    })
}

    
if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"BANNER_VAI_ATINGIU"},{"_id_coluna": 68}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaBooleano",
        _id_coluna: NumberInt(68),
        _id_coluna_tipo: NumberInt(4),
        coluna_grupos: [ NumberInt(16) ],
        nome: "BANNER_VAI_ATINGIU",
        preenchimento_obrigatorio: false,
        ativo: true,        
        opcoes: []
    })
}


if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"BANNER_CINEMA_ATINGIU"},{"_id_coluna": 69}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaBooleano",
        _id_coluna: NumberInt(69),
        _id_coluna_tipo: NumberInt(4),
        coluna_grupos: [ NumberInt(16) ],
        nome: "BANNER_CINEMA_ATINGIU",if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"BANNER_VAI_ATINGIU"},{"_id_coluna": 68}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaBooleano",
        _id_coluna: NumberInt(68),
        _id_coluna_tipo: NumberInt(4),
        coluna_grupos: [ NumberInt(16) ],
        nome: "BANNER_VAI_ATINGIU",
        preenchimento_obrigatorio: true,
        ativo: true,        
        opcoes: []
    })
}
if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"BANNER_CINEMA_ATINGIU"},{"_id_coluna": 69}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaBooleano",
        _id_coluna: NumberInt(69),
        _id_coluna_tipo: NumberInt(4),
        coluna_grupos: [ NumberInt(16) ],
        nome: "BANNER_CINEMA_ATINGIU",
        preenchimento_obrigatorio: true,
        ativo: true,        
        opcoes: []
    })
}
if (db.configuracao.arquivo.colunas.find({$and:[{"nome":"BANNER_CINEMA_LINK"},{"_id_coluna": 70}]}).count() == 0) {
    
    db.configuracao.arquivo.colunas.insert({
        _t: "ArquivoColunaTexto",
        _id_coluna: NumberInt(70),
        _id_coluna_tipo: NumberInt(1),
        coluna_grupos: [ NumberInt(16) ],
        nome: "BANNER_CINEMA_LINK",
        preenchimento_obrigatorio: true,
        ativo: true,		
        opcoes: []
    })
}
>>>>>>> 38db13c51bbb5f0f81840ea8323e2d5b263842ad
>>>>>>> master
