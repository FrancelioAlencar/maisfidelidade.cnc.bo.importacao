use db_concessionaria

if (db.configuracao.coluna.tipos.find({"_id":1}).count() == 0) {
    
    db.configuracao.coluna.tipos.insert({
        _id: NumberInt(1),
        nome: "Texto"
    })
}

if (db.configuracao.coluna.tipos.find({"_id":2}).count() == 0) {
    
    db.configuracao.coluna.tipos.insert({
        _id: NumberInt(2),
        nome: "N�mero Inteiro"
    })
}

if (db.configuracao.coluna.tipos.find({"_id":3}).count() == 0) {
    
    db.configuracao.coluna.tipos.insert({
        _id: NumberInt(3),
        nome: "N�mero Decimal"
    })
}

if (db.configuracao.coluna.tipos.find({"_id":4}).count() == 0) {
    
    db.configuracao.coluna.tipos.insert({
        _id: NumberInt(4),
        nome: "Booleano"
    })
}

if (db.configuracao.coluna.tipos.find({"_id":5}).count() == 0) {
    
    db.configuracao.coluna.tipos.insert({
        _id: NumberInt(5),
        nome: "Data"
    })
}

if (db.configuracao.coluna.tipos.find({"_id":6}).count() == 0) {
    
    db.configuracao.coluna.tipos.insert({
        _id: NumberInt(6),
        nome: "CNPJ"
    })
}

if (db.configuracao.coluna.tipos.find({"_id":7}).count() == 0) {
    
    db.configuracao.coluna.tipos.insert({
        _id: NumberInt(7),
        nome: "E-mail"
    })
}

if (db.configuracao.coluna.tipos.find({"_id":8}).count() == 0) {
    
    db.configuracao.coluna.tipos.insert({
        _id: NumberInt(8),
        nome: "Telefone"
    })
}

