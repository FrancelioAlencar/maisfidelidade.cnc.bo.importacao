﻿using Amazon.Lambda.SNSEvents;
using Amazon.Lambda.TestUtilities;
using maisfidelidade.cnc.imp.empresa;
using maisfidelidade.cnc.imp.tests.Helpers;
using System;
using System.Collections.Generic;
using Xunit;
using static Amazon.Lambda.SNSEvents.SNSEvent;

namespace maisfidelidade.cnc.imp.tests
{
	public class EmpresaFunctionTest
    {
        [Fact]
        public void EmpresaLambdaFunction()
        {
			// Arrange
			Environment.SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT", "hml");

			var sqsEvent = new SNSEvent
			{
				Records = new List<SNSRecord>
				{
					new SNSRecord
					{
						Sns = new SNSMessage
						{
							Message = Arrange.ArquivoImportadoId
						}
					}
				}
			};

			var lambdaContext = new TestLambdaContext
			{
				Logger = new TestLambdaLogger()
			};

			// Act
			var function = new Function();
			var result = function.FunctionHandler(sqsEvent, lambdaContext);

			// Assert
			Assert.True(result);
		}
    }
}
