﻿using Amazon.Lambda.SNSEvents;
using Amazon.Lambda.TestUtilities;
using maisfidelidade.cnc.imp.tests.Helpers;
using System;
using System.Collections.Generic;
using Xunit;
using static Amazon.Lambda.SNSEvents.SNSEvent;
using maisfidelidade.cnc.imp.apu.banner;

namespace maisfidelidade.cnc.imp.tests
{
	public class BannerFunctionTest
	{
		[Fact]
		public void BannerLambdaFunction()
		{
			// Arrange
			Environment.SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT", "hml");

			var sqsEvent = new SNSEvent
			{
				Records = new List<SNSRecord>
				{
					new SNSRecord
					{
						Sns = new SNSMessage
						{
							Message = Arrange.ArquivoImportadoId
						}
					}
				}
			};

			var lambdaContext = new TestLambdaContext
			{
				Logger = new TestLambdaLogger()
			};

			// Act
			var function = new Function();
			var result = function.FunctionHandler(sqsEvent, lambdaContext);

			// Assert
			Assert.True(result);
		}
	}
}