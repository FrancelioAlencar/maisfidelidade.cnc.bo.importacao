﻿using Amazon.Lambda.SNSEvents;
using Amazon.Lambda.TestUtilities;
using maisfidelidade.cnc.imp.tests.Helpers;
using System;
using System.Collections.Generic;
using Xunit;
using maisfidelidade.cnc.imp.orq.cadastro;
using static Amazon.Lambda.SNSEvents.SNSEvent;
using Newtonsoft.Json;
using maisfidelidade.cnc.imp.core.Domain.Model.OrquestracaoCadastro;

namespace maisfidelidade.cnc.imp.tests
{
	public class OrqCadastroFunctionTest
	{

		[Fact]
		public void EmpresaLambdaFunction()
		{
			// Arrange
			Environment.SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT", "hml");

			var sqsEvent = new SNSEvent
			{
				Records = new List<SNSRecord>
				{
					new SNSRecord
					{
						Sns = new SNSMessage
						{
							Message =JsonConvert.SerializeObject(new SnsOrquestracaoCadastro
								{
									ObjectId = Arrange.ArquivoImportadoId,
									EtapaCadastro = CadastroEnum.Empresas
								})
						}
					}
				}
			};

			var lambdaContext = new TestLambdaContext
			{
				//FunctionName = "lambda-maisfidelidade-importacao-orquestracao",
				MemoryLimitInMB = 512,
				//AwsRequestId = "568b709b-5c5c-43d8-b1a3-fa73fcc8110c",
				//InvokedFunctionArn = "arn:aws:lambda:us-east-1:900266424485:function:lambda-maisfidelidade-importacao-orquestracao:dev",
				Logger = new TestLambdaLogger()
			};

			// Act
			var function = new Function();
			var result = function.FunctionHandler(sqsEvent, lambdaContext);

			// Assert
			Assert.True(result);
		}
	
	[Fact]
	public void GerenteLambdaFunction()
	{

		// Arrange
		Environment.SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT", "hml");

		var sqsEvent = new SNSEvent
		{
			Records = new List<SNSRecord>
				{
					new SNSRecord
					{
						Sns = new SNSMessage
						{
							Message =JsonConvert.SerializeObject(new SnsOrquestracaoCadastro
								{
									ObjectId = Arrange.ArquivoImportadoId,
									EtapaCadastro = CadastroEnum.Gerentes
							})
						}
					}
				}
		};

		var lambdaContext = new TestLambdaContext
		{
			//FunctionName = "lambda-maisfidelidade-importacao-orquestracao",
			MemoryLimitInMB = 512,
			//AwsRequestId = "568b709b-5c5c-43d8-b1a3-fa73fcc8110c",
			//InvokedFunctionArn = "arn:aws:lambda:us-east-1:900266424485:function:lambda-maisfidelidade-importacao-orquestracao:dev",
			Logger = new TestLambdaLogger()
		};

		// Act
		var function = new Function();
		var result = function.FunctionHandler(sqsEvent, lambdaContext);

		// Assert
		Assert.True(result);
	}
}
}
