using Amazon.Lambda.SNSEvents;
using Amazon.Lambda.TestUtilities;
using maisfidelidade.cnc.imp.publicacao;
using maisfidelidade.cnc.imp.tests.Helpers;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;
using static Amazon.Lambda.SNSEvents.SNSEvent;

namespace maisfidelidade.cnc.imp.tests
{
    public class PublicacaoFunctionTest
    {
		[Fact]
        public async Task TestToUpperFunction()
        {
            Environment.SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT", "dev");

            var snsEvent = new SNSEvent
            {
                Records = new List<SNSRecord> {
                    new SNSRecord {
                        Sns = new SNSMessage {
                            Message = Arrange.ArquivoImportadoId
                        }
                    }
                }
            };

            var logger = new TestLambdaLogger();
            var context = new TestLambdaContext
            {
                Logger = logger
            };

            var function = new Function();
            Assert.True(function.FunctionHandler(snsEvent, context));
        }
    }
}