using Amazon.Lambda.SQSEvents;
using Amazon.Lambda.TestUtilities;
using maisfidelidade.cnc.imp.validacao.logica;
using maisfidelidade.cnc.imp.tests.Helpers;
using System;
using System.Collections.Generic;
using Xunit;

namespace maisfidelidade.cnc.imp.tests
{
	public class ValidacaoLogicaFunctionTest
	{
		[Fact]
		public void TestSQSEventLambdaFunction()
		{
			// Arrange
			Environment.SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT", "hml");

			var sqsEvent = new SQSEvent
			{
				Records = new List<SQSEvent.SQSMessage>
				{
					new SQSEvent.SQSMessage
					{
						Body = Arrange.ArquivoImportadoId
					}
				}
			};

			var lambdaContext = new TestLambdaContext
			{
				Logger = new TestLambdaLogger()
			};

			// Act
			var function = new Function();
			var result = function.FunctionHandler(sqsEvent, lambdaContext);

			// Assert
			Assert.True(result);
		}
	}
}