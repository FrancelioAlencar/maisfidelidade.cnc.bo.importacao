﻿using Amazon.Lambda.S3Events;
using Amazon.Lambda.TestUtilities;
using Amazon.S3.Util;
using maisfidelidade.cnc.imp.valid.fisica;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace maisfidelidade.cnc.imp.tests.ContextoValidacaoFisica
{
	public class ExtensaoArquivoInvalidaValidacaoFisicaFunctionTest
	{
		[Fact]
		public async Task ExtensaoArquivoInvalidaLambdaFunction()
		{
			// Arrange
			Environment.SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT", "hml");

			var bucketName = "opah-maisfidelidade-concessionaria";
			//var key = string.Empty;
			var key = "importacao/dev/JUNHO_MENSAL_CNC.v4.txt";

			var s3Event = new S3Event
			{
				Records = new List<S3EventNotification.S3EventNotificationRecord>
				{
					new S3EventNotification.S3EventNotificationRecord
					{
						S3 = new S3EventNotification.S3Entity
						{
							Bucket = new S3EventNotification.S3BucketEntity { Name = bucketName },
							Object = new S3EventNotification.S3ObjectEntity { Key = key }
						}
					}
				}
			};

			var lambdaContext = new TestLambdaContext
			{
				Logger = new TestLambdaLogger()
			};

			// Act
			var function = new Function();
			var result = await function.FunctionHandler(s3Event, lambdaContext);

			// Assert
			Assert.True(result);
		}
	}
}
