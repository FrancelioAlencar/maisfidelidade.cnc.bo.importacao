﻿using Amazon.Lambda.S3Events;
using Amazon.Lambda.TestUtilities;
using Amazon.S3.Util;
using maisfidelidade.cnc.imp.valid.fisica;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace maisfidelidade.cnc.imp.tests.ContextoValidacaoFisica
{
	public class S3NuloValidacaoFisicaFunctionTest
    {
		[Fact]
		public async Task S3NuloLambdaFunction()
		{
			// Arrange
			Environment.SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT", "hml");

			var bucketName = "opah-maisfidelidade-concessionaria";
			var key = "importacao/dev/JUNHO_MENSAL_CNC.v4.csv";

			var s3Event = new S3Event
			{
				Records = new List<S3EventNotification.S3EventNotificationRecord>
				{
					new S3EventNotification.S3EventNotificationRecord
					{
						S3 = null
					}
				}
			};

			var lambdaContext = new TestLambdaContext
			{
				Logger = new TestLambdaLogger()
			};

			// Act
			var function = new Function();
			var result = await function.FunctionHandler(s3Event, lambdaContext);

			// Assert
			Assert.True(result);
		}
	}
}
