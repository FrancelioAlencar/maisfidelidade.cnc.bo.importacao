﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using maisfidelidade.cnc.imp.test;
using Amazon.Lambda.TestUtilities;

namespace maisfidelidade.cnc.imp.tests.ContextoTest
{
    public class TestFunctionTest
    {
        [Fact]
        public void ApuracaoLambdaFunction()
        {
            // Arrange
            var lambdaContext = new TestLambdaContext
            {
                Logger = new TestLambdaLogger()
            };

            // Act
            //var function = new Function();

            var result = Function.FunctionHandler("inputTest", lambdaContext);

            // Assert
            Assert.Equal("", result);
        }
    }
}
